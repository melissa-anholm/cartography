#ifndef INCLUDE_MAPUTILITY
#define INCLUDE_MAPUTILITY 1


// ==================================================================== //
// Code by Melissa Anholm
// 
// ==================================================================== //

#include <vector>
#include <cmath>
#include <sstream> 
#include <iostream>  // cout

using std::cout;
using std::endl;
using std::string;
using std::vector;
using std::stringstream;

#include <TH1.h>
#include <TChain.h>
#include <TFile.h>
#include "TStyle.h"


#include "HistExtras.h" 
#include "ColorExtras.h"


#undef NDEBUG
#include<assert.h>


// --- // --- // --- // --- // --- // --- // --- // --- // --- // --- // --- // --- //
/*
struct chamber_geometry // naive numbers indirectly imported from G4.  we'll just kludge them in here, but remember that you can only use this if the trap is centred.
{
	// distance to the *front* of the dssd is 102.127357.
	G4double vdistance_center_to_dssd = 103.627357*mm; // this is probably a bit too precise.  wev.  it's in mm.
	G4double dssd_width_x = 40.0*mm; // mm.
	G4double dssd_width_y = 40.0*mm; // mm.
	G4double dssd_cut_radius = 15.5*mm; // mm.  probably it's only good for post-processing...  unused.
};
*/

extern double get_costheta(double rhit, double zhit);
extern string make_mapname_from_monoenergy(string namestub, double monoenergy);
extern string make_mapnamekludge_from_monoenergy(string namestub, double monoenergy);
extern string make_mapname_from_monoenergy(double monoenergy);
extern string make_mapnamekludge_from_monoenergy(double monoenergy);
extern string int_to_string(int the_int);


// --- // --- // --- // --- // --- // --- // --- // --- // --- // --- // --- // --- //
class MapSetup
{
public:
	MapSetup() 
		{ filename = "map_out.root"; };
	~MapSetup();
	
	void LoadFromTree(TChain * the_tree, int N_rebin_hists=1);
	void LoadFromTree_CountKludge(TChain * the_tree, int N_rebin_hists=1);
	void LoadFromFile(string filename_);
	void AdjustTheColors();
	void save_to_file(string filename_);
	void save_to_file();
	void CloneToFile(TFile * the_file, int verbose=1, bool leaveopen=false);
	TFile * RecreateAndLeaveOpen();  // it doesn't really update, it recreates.
	
	//
	TH1D* naive_EnergyT_p_hist;
	TH1D* naive_EnergyB_p_hist;
	TH1D* naive_EnergyT_m_hist;
	TH1D* naive_EnergyB_m_hist;
	
	TH1D* measured_EnergyT_p_hist;
	TH1D* measured_EnergyB_p_hist;
	TH1D* measured_EnergyT_m_hist;
	TH1D* measured_EnergyB_m_hist;
	
	TH1D* measured_EnergyT_p_bb1agree;
	TH1D* measured_EnergyB_p_bb1agree;
	TH1D* measured_EnergyT_m_bb1agree;
	TH1D* measured_EnergyB_m_bb1agree;
	
	TH1D* measured_EnergyT_p_bb1_r155;
	TH1D* measured_EnergyB_p_bb1_r155;
	TH1D* measured_EnergyT_m_bb1_r155;
	TH1D* measured_EnergyB_m_bb1_r155;
	
	TH1D* measured_EnergyT_p_bb1_r105;
	TH1D* measured_EnergyB_p_bb1_r105;
	TH1D* measured_EnergyT_m_bb1_r105;
	TH1D* measured_EnergyB_m_bb1_r105;
	
	TH2D* costheta_v_costheta_p;
	TH2D* costheta_v_costheta_m;
	
//	TH1D* BB1energyT_p_bb1agree;
//	TH1D* BB1energyB_p_bb1agree;
//	TH1D* BB1energyT_m_bb1agree;
//	TH1D* BB1energyB_m_bb1agree;
	
private:
	string filename;
};


// --- // --- // --- // --- // --- // --- // --- // --- // --- // --- // --- // --- //
// --- // --- // --- // --- // --- // --- // --- // --- // --- // --- // --- // --- //
// --- // --- // --- // --- // --- // --- // --- // --- // --- // --- // --- // --- //


// --- // --- // --- // --- // --- // --- // --- // --- // --- // --- // --- // --- //
#endif
