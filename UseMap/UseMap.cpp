// ==================================================================== //
// Code by Melissa Anholm
// 
// 
// ==================================================================== //
#include <ncurses.h>

#include "TGraph.h"
#include "TGraphErrors.h"

#include "location.cpp"
#include "MetaChain.cpp"

//#include "scaleplots_fordan.cpp"
//#include "canvasplots.cpp"
//#include "canvasplots.h"  // includes MapUtility.cpp. ..or not.  includes FitUtility.cpp though.  .. or not?
#include "MapUtility.h"  // 

//#include "MapUtility.cpp"
//#include "FitUtility.cpp"
#include "FitUtility.h"
//#include "ColorExtras.cpp"
//#include "GraphExtras.cpp"
#include "AsymmetryCanvasLibs.cpp"


#include "TStyle.h"
#include "TCanvas.h"
#include "TApplication.h"
#include "TLegend.h"
#include "TPaveText.h"

//#include <bits/stdc++.h> 
#include <algorithm>    // std::max
using std::max;
using std::min;

//using std::vector;
using std::pair;
using std::make_pair;

// --- // --- // --- // --- // --- // --- // --- // --- // --- // --- // --- // --- //
#define __SHORT_FORM_OF_FILE__ \
(strrchr(__FILE__,'/') ? strrchr(__FILE__,'/')+1 : __FILE__ )
// --- // --- // --- // --- // --- // --- // --- // --- // --- // --- // --- // --- //

enum detector : int  // don't know if this is Ben's convention or not.
{
	t=0,
	b=1
};
class energy_specifier
{
public:
	energy_specifier(string ts, int ti, double td)
	{
		the_string = ts;
		the_double = td;
		the_int    = ti;
	};
	
	string the_string;
	double the_double;
	int the_int;
};
vector<energy_specifier> load_energies()
{
	vector<energy_specifier> the_energyset;
	the_energyset.push_back(energy_specifier( "500",  500, 0.5));
//	the_energyset.push_back(energy_specifier( "625",  625, 0.625));
	the_energyset.push_back(energy_specifier( "750",  750, 0.75));
	the_energyset.push_back(energy_specifier( "875",  875, 0.875));
	the_energyset.push_back(energy_specifier("1000", 1000, 1.0));
	the_energyset.push_back(energy_specifier("1125", 1125, 1.125));
	the_energyset.push_back(energy_specifier("1250", 1250, 1.25));
	the_energyset.push_back(energy_specifier("1375", 1375, 1.375));
	the_energyset.push_back(energy_specifier("1500", 1500, 1.5));
//	the_energyset.push_back(energy_specifier("1625", 1625, 1.5));
//	the_energyset.push_back(energy_specifier("1750", 1750, 1.5));
//	the_energyset.push_back(energy_specifier("1875", 1875, 1.5));
	the_energyset.push_back(energy_specifier("2000", 2000, 2.0));
	the_energyset.push_back(energy_specifier("2500", 2500, 2.5));
	the_energyset.push_back(energy_specifier("3000", 3000, 3.0));
	the_energyset.push_back(energy_specifier("3500", 3500, 3.5));
	the_energyset.push_back(energy_specifier("4000", 4000, 4.0));
	the_energyset.push_back(energy_specifier("4500", 4500, 4.5));
	the_energyset.push_back(energy_specifier("5000", 5000, 5.0));
	
	return the_energyset;
}
vector<energy_specifier> the_energyset = load_energies();

void SetupLineshapes(TF1 &R_tp, TF1 &R_tm, TF1 &R_bp, TF1 &R_bm, double E_in_keV)
{
	SetParam_E0(R_tp, R_tm, R_bp, R_bm,      (double)E_in_keV);
	SetParam_toeres(R_tp, R_tm, R_bp, R_bm,  (double)E_in_keV);
	SetParam_W(R_tp, R_tm, R_bp, R_bm,       (double)E_in_keV);  
	
	SetParam_alpha(R_tp, R_tm, R_bp, R_bm,   (double)E_in_keV);
	SetParam_beta(R_tp, R_tm, R_bp, R_bm,    (double)E_in_keV);
	SetParam_gamma(R_tp, R_tm, R_bp, R_bm,   (double)E_in_keV);
	SetParam_delta(R_tp, R_tm, R_bp, R_bm,   (double)E_in_keV);

	SetParam_gfrac(R_tp, R_tm, R_bp, R_bm,   (double)E_in_keV);

	SetParam_gres(R_tp, R_tm, R_bp, R_bm,    (double)E_in_keV);
	SetParam_lres(R_tp, R_tm, R_bp, R_bm,    (double)E_in_keV);

//	SetParam_DgE_tmp(R_tp, R_tm, R_bp, R_bm, (double)E_in_keV);
	R_tp . FixParameter(R_tp.GetParNumber("13_DgE"), 0 );
	R_tm . FixParameter(R_tm.GetParNumber("13_DgE"), 0 );
	R_bp . FixParameter(R_bp.GetParNumber("13_DgE"), 0 );
	R_bm . FixParameter(R_bm.GetParNumber("13_DgE"), 0 );
	
	SetParam_dE0(R_tp, R_tm, R_bp, R_bm,     (double)E_in_keV);
	
	SetParam_scale(R_tp, R_tm, R_bp, R_bm,   (double)E_in_keV);
	SetParam_norm(R_tp, R_tm, R_bp, R_bm,    (double)E_in_keV);
	SetParam_k(R_tp, R_tm, R_bp, R_bm,       (double)E_in_keV);
	
	
	// norm, scale.  
}

// --- // --- // --- // --- // --- // --- // --- // --- // --- // --- // --- // --- //

//TF1  * make_superratio(TF1*  f_tp, TF1*  f_tm, TF1*  f_bp, TF1*  f_bm);
TH1D * make_superratio_direct(TH1D* h_tp, TH1D* h_tm, TH1D* h_bp, TH1D* h_bm)
{
	int plotmarkerstyle=7;
	TH1D * h_ratio = make_superratio_histogram(h_tp, h_tm, h_bp, h_bm, string("Superratio"), int(kBlack), plotmarkerstyle);
	h_ratio -> SetOption("E3");
	h_ratio -> SetFillColor(kGray);
	
	return h_ratio;
}

//TF1  * make_supersum(TF1*  f_tp, TF1*  f_tm, TF1*  f_bp, TF1*  f_bm);
TH1D * make_supersum_direct(TH1D* h_tp, TH1D* h_tm, TH1D* h_bp, TH1D* h_bm)
{
	int plotmarkerstyle=7;
	TH1D * h_sum = make_supersum_histogram(h_tp, h_tm, h_bp, h_bm, string("Supersum"), int(kBlack), plotmarkerstyle);
	h_sum -> SetOption("E3");
	h_sum -> SetFillColor(kGray);
	
	return h_sum;
}

//TF1  * make_superratioasymmetry(TF1*  f_tp, TF1*  f_tm, TF1*  f_bp, TF1*  f_bm);
TH1D * make_superratioasymmetry_direct(TH1D* h_tp, TH1D* h_tm, TH1D* h_bp, TH1D* h_bm)  // do this to get an asymmetry from the full spectrum.
{
	int plotmarkerstyle=7;
	TH1D * h_asym = make_asymmetry_histogram(h_tp, h_tm, h_bp, h_bm, string("Superratio Asymmetry"), int(kBlack), plotmarkerstyle);
	h_asym -> SetOption("E3");
	h_asym -> SetFillColor(kGray);
	
	return h_asym;
}


// --- // --- // --- // --- // --- // --- // --- // --- // --- // --- // --- // --- //

int main(int argc, char *argv[]) 
{
	setup_location();
	TApplication* rootapp = 0;
	char ** mychar = NULL;
	rootapp = new TApplication("blarg",0, mychar);
	TFile * f;

	int N_rebin = 60;
	string namestub = "FullSpectrumMap";
	MapSetup * my_new_map = new MapSetup();
	
	TF1 * R = make_response_function();
	TF1 * R_tp = (TF1*)R->Clone("R_tp");
	TF1 * R_tm = (TF1*)R->Clone("R_tm");
	TF1 * R_bp = (TF1*)R->Clone("R_bp");
	TF1 * R_bm = (TF1*)R->Clone("R_bm");

	plotfuncs * the_funcs_tp;
	/*
	// Create and save the histogram 'map' from the full spectrum runs we're comparing everything to:  
	vector<int> the_runlist{10001, 10002};
	TChain * TreeChain = get_chain_from_runlist(the_runlist);
	
	my_new_map -> LoadFromTree(TreeChain);
	my_new_map -> AdjustTheColors();
	my_new_map -> save_to_file( make_mapname_from_monoenergy(namestub, 0) );  // file is closed when it's done.
	*/
	
	//
	my_new_map -> LoadFromFile( make_mapname_from_monoenergy(namestub, 0) );
	
	TH1D* h_tp = (TH1D*)my_new_map->naive_EnergyT_p_hist;//->Rebin(N_rebin);
	TH1D* h_bp = (TH1D*)my_new_map->naive_EnergyB_p_hist;//->Rebin(N_rebin);
	TH1D* h_tm = (TH1D*)my_new_map->naive_EnergyT_m_hist;//->Rebin(N_rebin);
	TH1D* h_bm = (TH1D*)my_new_map->naive_EnergyB_m_hist;//->Rebin(N_rebin);
	h_tp -> SetLineColor(kGreen);
	h_bp -> SetLineColor(kGreen);
	h_tm -> SetLineColor(kGreen);
	h_bm -> SetLineColor(kGreen);
	
	int FasterRebin = 60;
//	h_tp = (TH1D*)my_new_map->measured_EnergyT_p_bb1agree->Rebin(FasterRebin);
//	h_bp = (TH1D*)my_new_map->measured_EnergyB_p_bb1agree->Rebin(FasterRebin);
//	h_tm = (TH1D*)my_new_map->measured_EnergyT_m_bb1agree->Rebin(FasterRebin);
//	h_bm = (TH1D*)my_new_map->measured_EnergyB_m_bb1agree->Rebin(FasterRebin);
	h_tp->Rebin(FasterRebin);
	h_bp->Rebin(FasterRebin);
	h_tm->Rebin(FasterRebin);
	h_bm->Rebin(FasterRebin);
	
	int N_bins_orig = h_tp -> GetNbinsX();
	double E_in_keV;
	
	
	cout << "Reconstructing... (N=" << N_bins_orig << ")" << endl;
	TH1D* reconstructed_tp = CreateHist( string("Reconstructed Upper Energy(+)"), string("Mapping_Ebeta"), int(kRed), FasterRebin);
	TH1D* reconstructed_bp = CreateHist( string("Reconstructed Lower Energy(+)"), string("Mapping_Ebeta"), int(kRed), FasterRebin);
	TH1D* reconstructed_tm = CreateHist( string("Reconstructed Upper Energy(-)"), string("Mapping_Ebeta"), int(kRed), FasterRebin);
	TH1D* reconstructed_bm = CreateHist( string("Reconstructed Lower Energy(-)"), string("Mapping_Ebeta"), int(kRed), FasterRebin);	
	cout << "reconstructed_bm (Red) has " << reconstructed_bm-> GetNbinsX() << " bins." << endl;
	
	for (int i=10; i<N_bins_orig; i++)
	{
		if(i%100 == 0) { cout << "i = " << i << endl; }
		E_in_keV = h_tp->GetBinCenter(i);
		SetupLineshapes(*R_tp, *R_tm, *R_bp, *R_bm, E_in_keV);
		// all the R's are modified in place.
		
		for (int j=10; j<N_bins_orig; j++)
		{
			double other_E = h_tp->GetBinCenter(j);
			
		//	if(i==90)
		//	cout << "i=30;  E_in = " << E_in_keV << ";\tE=" << other_E << "; R_tp->Eval(other_E) = " << R_tp->Eval(other_E) << endl;
			
			if(other_E >5000.0)
			{
				if(R_tp->Eval(other_E) >0.1)
				cout << "E_in_keV = " << E_in_keV << ";\tE=" << other_E << ";\tR_tp->Eval(other_E) = " << R_tp->Eval(other_E) << endl;
			}
			reconstructed_tp -> Fill(other_E, R_tp->Eval(other_E) );
			reconstructed_bp -> Fill(other_E, R_bp->Eval(other_E) );
			reconstructed_tm -> Fill(other_E, R_tm->Eval(other_E) );
			reconstructed_bm -> Fill(other_E, R_bm->Eval(other_E) );
		}
		// ok ... now what?  Are they done?
	}
	/*
	// alternately, load 'em from the file:
	f = new TFile("SuperCanvases.root");
	TH1D* reconstructed_tp = (TH1D*)f->Get("Reconstructed Upper Energy(+)");  // already have nbins=100 ??
	TH1D* reconstructed_bp = (TH1D*)f->Get("Reconstructed Lower Energy(+)");
	TH1D* reconstructed_tm = (TH1D*)f->Get("Reconstructed Upper Energy(-)");
	TH1D* reconstructed_bm = (TH1D*)f->Get("Reconstructed Lower Energy(-)");
	reconstructed_tp -> SetLineColor(kMagenta);
	reconstructed_bp -> SetLineColor(kMagenta);
	reconstructed_tm -> SetLineColor(kMagenta);
	reconstructed_bm -> SetLineColor(kMagenta);
	reconstructed_tp -> SetMarkerColor(kMagenta);
	reconstructed_bp -> SetMarkerColor(kMagenta);
	reconstructed_tm -> SetMarkerColor(kMagenta);
	reconstructed_bm -> SetMarkerColor(kMagenta);
	cout << "magenta bins?  " << reconstructed_bp -> GetNbinsX() << endl;
	
//	reconstructed_tp->Rebin(FasterRebin);
//	reconstructed_bp->Rebin(FasterRebin);
//	reconstructed_tm->Rebin(FasterRebin);
//	reconstructed_bm->Rebin(FasterRebin);

//	f->Close();
	*/
	cout << "Done reconstructing." << endl;

	/*
	if( reconstructed_tp && reconstructed_tm && reconstructed_bp && reconstructed_bm )
	{ cout << "All exist." << endl; }
	
	cout << "Exiting, I guess." << endl;
	return 0;
	*/
	
	TH1D* recounts_tp = CreateHist( string("Reconstructed Counts Upper Energy(+)"), string("Mapping_Ebeta"), int(kBlue), N_rebin);
	TH1D* recounts_bp = CreateHist( string("Reconstructed Counts Lower Energy(+)"), string("Mapping_Ebeta"), int(kBlue), N_rebin);
	TH1D* recounts_tm = CreateHist( string("Reconstructed Counts Upper Energy(-)"), string("Mapping_Ebeta"), int(kBlue), N_rebin);
	TH1D* recounts_bm = CreateHist( string("Reconstructed Counts Lower Energy(-)"), string("Mapping_Ebeta"), int(kBlue), N_rebin);

	cout << "Integrating ..." << endl;
//	for (int i=1; i<N_bins_orig; i++)
	for (int i=10; i<N_bins_orig; i++)
	{
		E_in_keV = h_tp->GetBinCenter(i);
		SetupLineshapes(*R_tp, *R_tm, *R_bp, *R_bm, E_in_keV);
		
		/*
		the_funcs_tp = new plotfuncs();
		the_funcs_tp -> load_parameters(R_tp);
		cout << "i=" << i << ";\tE=" << E_in_keV << endl;
		*/
		
		/*
		double landau_val = the_funcs_tp -> f_landau()->Eval(E_in_keV);
		double c1_val = the_funcs_tp -> f1_clifford()->Eval(E_in_keV);
		double c2_val = the_funcs_tp -> f2_clifford()->Eval(E_in_keV);
		double c3_val = the_funcs_tp -> f3_clifford()->Eval(E_in_keV);
		double c4_val = the_funcs_tp -> f4_clifford()->Eval(E_in_keV);
		double c5_val = the_funcs_tp -> f5_clifford()->Eval(E_in_keV);
		double val_511 = the_funcs_tp -> f_511()->Eval(E_in_keV);
		cout << "\tR_tp = " << R_tp->Eval(E_in_keV) << ";\tIntegral=" << R_tp->Integral(200,5500) << endl;
		cout << "\t\tlandau_val=" << c1_val << endl;
		cout << "\t\tc1_val=" << c1_val << endl;
		cout << "\t\tc2_val=" << c2_val << endl;
		cout << "\t\tc3_val=" << c3_val << endl;
		cout << "\t\tc4_val=" << c4_val << endl;
		cout << "\t\tc5_val=" << c5_val << endl;
		cout << "\t\tval_511=" << val_511 << endl;
		*/
		
		/*
		TCanvas * c = new TCanvas();
		the_funcs_tp -> f1_clifford() -> Draw();
		the_funcs_tp -> f2_clifford() -> Draw("same");
		the_funcs_tp -> f3_clifford() -> Draw("same");
		the_funcs_tp -> f4_clifford() -> Draw("same");
		the_funcs_tp -> f5_clifford() -> Draw("same");
		the_funcs_tp -> f_511() -> Draw("same");
		R_tp -> Draw("same");
		rootapp->Run();
		*/
		/*
		for (int j=1; j<N_bins_orig; j++)
		{
			E_in_keV = h_tp->GetBinCenter(j);
			cout << "E = " << E_in_keV << ";\t R_tp = " << R_tp->Eval(E_in_keV) << endl;
			cout << "\tf1 = " << the_funcs_tp->f1_clifford()->Eval(E_in_keV) << endl;
			cout << "\tf2 = " << the_funcs_tp->f2_clifford()->Eval(E_in_keV) << endl;
			cout << "\tf3 = " << the_funcs_tp->f3_clifford()->Eval(E_in_keV) << endl;
			cout << "\tf4 = " << the_funcs_tp->f4_clifford()->Eval(E_in_keV) << endl;
			cout << "\tf5 = " << the_funcs_tp->f5_clifford()->Eval(E_in_keV) << endl;
			cout << "\tf511 = " << the_funcs_tp->f_511()->Eval(E_in_keV) << endl;
		}
		*/
		/*
		//cout << 
		E_in_keV = 1229.5;
		cout << "\t\tlandau_val=" << c1_val << endl;
		cout << "\t\tc1_val=" << c1_val << endl;
		cout << "\t\tc2_val=" << c2_val << endl;
		cout << "\t\tc3_val=" << c3_val << endl;
		cout << "\t\tc4_val=" << c4_val << endl;
		cout << "\t\tc5_val=" << c5_val << endl;
		cout << "\t\tval_511=" << val_511 << endl;
		E_in_keV = 1289.5;
		cout << "\t\tlandau_val=" << c1_val << endl;
		cout << "\t\tc1_val=" << c1_val << endl;
		cout << "\t\tc2_val=" << c2_val << endl;
		cout << "\t\tc3_val=" << c3_val << endl;
		cout << "\t\tc4_val=" << c4_val << endl;
		cout << "\t\tc5_val=" << c5_val << endl;
		cout << "\t\tval_511=" << val_511 << endl;
		*/
	//	return 0;
		
		recounts_tp -> Fill(E_in_keV, R_tp->Integral(200,5500)/15.0 );
		
	//	cout << "\tR_bp = " << R_bp->Eval(E_in_keV) << ";\tIntegral=" << R_bp->Integral(200,5500) << endl;
		recounts_bp -> Fill(E_in_keV, R_bp->Integral(200,5500)/15.0 );
		
	//	cout << "\tR_tm = " << R_tm->Eval(E_in_keV) << ";\tIntegral=" << R_tm->Integral(200,5500) << endl;
		recounts_tm -> Fill(E_in_keV, R_tm->Integral(200,5500)/15.0 );
		
	//	cout << "\tR_bm = " << R_bm->Eval(E_in_keV) << ";\tIntegral=" << R_bm->Integral(200,5500) << endl;
		recounts_bm -> Fill(E_in_keV, R_bm->Integral(200,5500)/15.0 );
	}
	cout << "Done integrating." << endl;
	
	/*
	TH1D* reconstructed_tp_coarse = (TH1D*)reconstructed_tp->Clone("Reconstructed Upper Energy(+) - Rebinned");
	TH1D* reconstructed_bp_coarse = (TH1D*)reconstructed_bp->Clone("Reconstructed Lower Energy(+) - Rebinned");
	TH1D* reconstructed_tm_coarse = (TH1D*)reconstructed_tm->Clone("Reconstructed Upper Energy(-) - Rebinned");
	TH1D* reconstructed_bm_coarse = (TH1D*)reconstructed_bm->Clone("Reconstructed Lower Energy(-) - Rebinned");
	cout << "Got here?" << endl;
	
	reconstructed_tp_coarse->Rebin(N_rebin/FasterRebin);
	reconstructed_bp_coarse->Rebin(N_rebin/FasterRebin);
	reconstructed_tm_coarse->Rebin(N_rebin/FasterRebin);
	reconstructed_bm_coarse->Rebin(N_rebin/FasterRebin);
	cout << "got here." << endl;
	
	reconstructed_tp_coarse->Sumw2();
	reconstructed_bp_coarse->Sumw2();
	reconstructed_tm_coarse->Sumw2();
	reconstructed_bm_coarse->Sumw2();
	*/
	
	cout << reconstructed_tp->GetNbinsX() << " = " << reconstructed_tm->GetNbinsX() << " = " << reconstructed_bp->GetNbinsX() << " = " << reconstructed_bm->GetNbinsX() << endl;
	
	//
	TH1D * hre_asym  = make_superratioasymmetry_direct(reconstructed_tp, reconstructed_tm, reconstructed_bp, reconstructed_bm);
	cout << "Asymmetry is done." << endl;
	TH1D * hre_ratio = make_superratio_direct(reconstructed_tp, reconstructed_tm, reconstructed_bp, reconstructed_bm);
	TH1D * hre_sum   = make_supersum_direct(reconstructed_tp, reconstructed_tm, reconstructed_bp, reconstructed_bm);
	hre_asym  -> SetMarkerColor(kRed);
	hre_ratio -> SetMarkerColor(kRed);
	hre_sum   -> SetMarkerColor(kRed);
	hre_asym  -> SetLineColor(kRed);
	hre_ratio -> SetLineColor(kRed);
	hre_sum   -> SetLineColor(kRed);
	hre_asym  -> SetFillColor(kRed-9);
	hre_ratio -> SetFillColor(kRed-9);
	hre_sum   -> SetFillColor(kRed-9);

//	cout << "ok..." << endl;
	
	TGraphErrors* gre_asym  = new TGraphErrors(hre_asym);
	TGraphErrors* gre_ratio = new TGraphErrors(hre_ratio);
	TGraphErrors* gre_sum   = new TGraphErrors(hre_sum);
	
	
	// Full spectrum:  
	// make histograms:  superratio, supersum, superratio asymmetry.
	TH1D* hfull_tp = (TH1D*)my_new_map->measured_EnergyT_p_bb1agree->Rebin(N_rebin);
	TH1D* hfull_bp = (TH1D*)my_new_map->measured_EnergyB_p_bb1agree->Rebin(N_rebin);
	TH1D* hfull_tm = (TH1D*)my_new_map->measured_EnergyT_m_bb1agree->Rebin(N_rebin);
	TH1D* hfull_bm = (TH1D*)my_new_map->measured_EnergyB_m_bb1agree->Rebin(N_rebin);
	
	// Make the SuperHists:
	TH1D * hfull_asym  = make_superratioasymmetry_direct(hfull_tp, hfull_tm, hfull_bp, hfull_bm);
	TH1D * hfull_ratio = make_superratio_direct(hfull_tp, hfull_tm, hfull_bp, hfull_bm);
	TH1D * hfull_sum   = make_supersum_direct(hfull_tp, hfull_tm, hfull_bp, hfull_bm);
//	hfull_asym  -> SetMarkerColor(kRed);
//	hfull_ratio -> SetMarkerColor(kRed);
//	hfull_sum   -> SetMarkerColor(kRed);
//	hfull_asym  -> SetLineColor(kRed);
//	hfull_ratio -> SetLineColor(kRed);
//	hfull_sum   -> SetLineColor(kRed);
//	hfull_asym  -> SetFillColor(kRed-9);
//	hfull_ratio -> SetFillColor(kRed-9);
//	hfull_sum   -> SetFillColor(kRed-9);
	
	TGraphErrors* gfull_asym  = new TGraphErrors(hfull_asym);
	TGraphErrors* gfull_ratio = new TGraphErrors(hfull_ratio);
	TGraphErrors* gfull_sum   = new TGraphErrors(hfull_sum);
	
	// --- //  // --- //  // --- //  // --- // 
	TLegend * myLegend;
	TText *datalabel = new TText();
	datalabel -> SetNDC();
	datalabel -> SetTextColor(1);
	datalabel -> SetTextSize(0.018);
	TText *datalabel2 = new TText();
	datalabel2 -> SetNDC();
	datalabel2 -> SetTextColor(1);
	datalabel2 -> SetTextSize(0.018*2);

	
	// --- //  // --- // 
	TCanvas * c4 = new TCanvas("c4 Canvas", "c4 Canvas", 100, 0, 900, 700);
	c4->Divide(2,2);
	c4->cd(1);  // tp
	reconstructed_tp -> Draw("lhist");  // red.
	h_tp -> Draw("lsame");  // naive counts;  green.
	recounts_tp -> Draw("l hist same");  // blue, measured counts at whatever input energy.  /100.
	
	datalabel2 -> DrawText(0.10, 0.908, __SHORT_FORM_OF_FILE__);
	c4->cd(2);  // tm
	reconstructed_tm -> Draw("lhist");
	h_tm -> Draw("lsame");
	recounts_tm -> Draw("l hist same");

	c4->cd(3);  // bp
	reconstructed_bp -> Draw("lhist");
	h_bp -> Draw("lsame");
	recounts_bp -> Draw("l hist same");

	c4->cd(4);  // bm
	reconstructed_bm -> Draw("lhist");
	h_bm -> Draw("lsame");
	recounts_bm -> Draw("l hist same");

	gPad->Update();

	
	// --- //  // --- // 
	TCanvas * c1 = new TCanvas("SuperAsym Canvas", "SuperAsym Canvas", 100, 0, 900, 700);
	c1->cd();
	hfull_asym -> Draw();
	gfull_asym -> Draw("lpx same");  // probably black?
	gre_asym   -> Draw("lpx same");  // should be red.

	datalabel -> DrawText(0.10, 0.908, __SHORT_FORM_OF_FILE__);
	gPad->Update();
	
	TCanvas * c2 = new TCanvas("Ratio Canvas", "Ratio Canvas", 100, 0, 900, 700);
	c2->cd();
	hfull_ratio -> Draw();
	gfull_ratio -> Draw("lpx same");
	gre_ratio   -> Draw("lpx same");
	datalabel -> DrawText(0.10, 0.908, __SHORT_FORM_OF_FILE__);
	gPad->Update();

	TCanvas * c3 = new TCanvas("SuperSum Canvas", "SuperSum Canvas", 100, 0, 900, 700);
	c3->cd();
	hfull_sum -> Draw();
	gfull_sum -> Draw("lpx same");
	gre_sum   -> Draw("lpx same");
	datalabel -> DrawText(0.10, 0.908, __SHORT_FORM_OF_FILE__);
	gPad->Update();
	
	// --- //  // --- // 
	f = new TFile("SuperCanvases.root", "UPDATE");
	f->cd();
	
	c4->Write("",TObject::kOverwrite);
	reconstructed_tp -> Write("",TObject::kOverwrite);
	reconstructed_tm -> Write("",TObject::kOverwrite);
	reconstructed_bp -> Write("",TObject::kOverwrite);
	reconstructed_bm -> Write("",TObject::kOverwrite);
	h_tp ->Write("",TObject::kOverwrite);
	h_tm ->Write("",TObject::kOverwrite);
	h_bp ->Write("",TObject::kOverwrite);
	h_bm ->Write("",TObject::kOverwrite);
	recounts_tp -> Write("",TObject::kOverwrite);
	recounts_tm -> Write("",TObject::kOverwrite);
	recounts_bp -> Write("",TObject::kOverwrite);
	recounts_bm -> Write("",TObject::kOverwrite);
	
	hfull_tp->Write("",TObject::kOverwrite);
	hfull_bp->Write("",TObject::kOverwrite);
	hfull_tm->Write("",TObject::kOverwrite);
	hfull_bm->Write("",TObject::kOverwrite);
	
	c1->Write("",TObject::kOverwrite);
	hfull_asym->Write("",TObject::kOverwrite);
	gre_asym->Write("",TObject::kOverwrite);
	
	c2->Write("",TObject::kOverwrite);
	hfull_ratio->Write("",TObject::kOverwrite);
	gre_ratio->Write("",TObject::kOverwrite);
	
	c3->Write("",TObject::kOverwrite);
	hfull_sum->Write("",TObject::kOverwrite);
	gre_sum->Write("",TObject::kOverwrite);
	
	
	
	f->Close();

	
	// --- //  // --- // 
	beep();
	
	cout << "Running the rootapp." << endl;
	rootapp->Run();
	
	return 0;
}




























