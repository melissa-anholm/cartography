// make_showing_plots.cpp

#include<iostream>

#include "ColorExtras.h"

class energy_specifier;
extern vector<energy_specifier> the_energyset;


void input_plot()  // from the rootscript input_plot.cpp.
{
	double energy;
	MapSetup * my_new_map;
	
	TCanvas * c = new TCanvas("c", "c", 100, 0, 900, 700);
	c->Divide(2,2);
	
	for(int i=the_energyset.size()-1; i>=0; i--)  // count down instead.  (am I off by one?)
	{
		energy = the_energyset.at(i).the_int;
		
		my_new_map = new MapSetup();
		my_new_map -> LoadFromFile( make_mapname_from_monoenergy(the_energyset.at(i).the_double) );
		
		if(i== the_energyset.size()-1)
		{
			c->cd(1);
			my_new_map -> naive_EnergyT_p_hist->Draw("hist");
		
			c->cd(2);
			my_new_map -> naive_EnergyT_m_hist->Draw("hist");
			
			c->cd(3);
			my_new_map -> naive_EnergyB_p_hist->Draw("hist");
			
			c->cd(4);
			my_new_map -> naive_EnergyB_m_hist->Draw("hist");
		}
		
		c->cd(1);
		my_new_map -> naive_EnergyT_p_hist->Draw("same");
		gPad->Update();
		
		c->cd(2);
		my_new_map -> naive_EnergyT_m_hist->Draw("same");
		gPad->Update();
		
		c->cd(3);
		my_new_map -> naive_EnergyB_p_hist->Draw("same");
		gPad->Update();
		
		c->cd(4);
		my_new_map -> naive_EnergyB_m_hist->Draw("same");
		gPad->Update();
		
		
		cout << "Loaded for i = " << i << endl;
	}
}

TCanvas * plot_inputcompare_p(int N_rebin)
{
//	int N_rebin = 9*5*3;
	MapSetup * my_new_map;
	double energy;
	TH1D* h0;
	TH1D* h;
	TH1D* h2;
	
	TCanvas * cp = new TCanvas("cp", "cp", 100, 0, 900, 700);
	for(int i=the_energyset.size()-1; i>=0; i--)  // count down instead.  (am I off by one?)
	{
		energy = the_energyset.at(i).the_int;
		
		my_new_map = new MapSetup();
		my_new_map -> LoadFromFile( make_mapname_from_monoenergy(the_energyset.at(i).the_double) );
		
		cp -> cd();
		h = (TH1D*)my_new_map->naive_bincenter_EnergyT_p_hist->Clone();
		h0 = (TH1D*)my_new_map->naive_bincenter_EnergyT_p_hist->Clone();
		h0->Rebin(N_rebin);
		h0->SetName("Naive Pol+ T");
		h0->SetTitle("Naive Pol+ T");

		h->Add(my_new_map->naive_bincenter_EnergyB_p_hist);  
		h->SetName("Naive Pol+, T+B");
		h->SetTitle("Naive Pol+");
		h->Rebin(N_rebin);
		h->SetLineColor(kCyan);
		h->SetMarkerColor(kCyan);
		h->SetFillColor(kCyan);
		
		h2 = (TH1D*)my_new_map->naive_bincenter_EnergyB_p_hist->Clone();
		h2->SetName("Naive Pol+ B");
		h2->SetTitle("Naive Pol+ B");
		h2->Rebin(N_rebin);
		h2->SetLineColor(int(mOrange));
		h2->SetMarkerColor(int(mOrange));
		h2->SetFillColor(int(mOrange));
		
		if(i== the_energyset.size()-1)
		{
			h->GetXaxis()->SetRangeUser(0, 5200);
			h->Draw("b");  // figure out how to fill it in.
		}
		h->Draw("b same");
		h2->Draw("b same");
		
		cout << std::fixed;
		cout << std::setprecision(0);
		cout << "E = " << energy << " keV" << endl;
		cout << "N_top (+)    =  " << (double)h0->Integral() << endl;
		cout << "N_bottom (+) = " << (double)h2->Integral() << endl;
		cout << "N_total (+)  = " << h->Integral() << endl;
		cout << std::setprecision(4);
		cout << "Percent B = " << 100.0*(double)h2->Integral() / h->Integral() << endl;
		cout << endl;
	}
	return cp;
}

TCanvas * plot_inputcompare_m(int N_rebin)
{
//	int N_rebin = 9*5*3;
	MapSetup * my_new_map;
	double energy;
	TH1D* h0;
	TH1D* h;
	TH1D* h2;
	
//	TCanvas * cp = new TCanvas("cp", "cp", 100, 0, 900, 700);
	TCanvas * cm = new TCanvas("cm", "cm", 100, 0, 900, 700);

	for(int i=the_energyset.size()-1; i>=0; i--)
	{
		energy = the_energyset.at(i).the_int;
		
		my_new_map = new MapSetup();
		my_new_map -> LoadFromFile( make_mapname_from_monoenergy(the_energyset.at(i).the_double) );
		
		cm -> cd();
		h = (TH1D*)my_new_map->naive_bincenter_EnergyT_m_hist->Clone();
		h0 = (TH1D*)my_new_map->naive_bincenter_EnergyT_m_hist->Clone();
		h0->Rebin(N_rebin);
		h0->SetName("Naive Pol- T");
		h0->SetTitle("Naive Pol- T");

		h->Add(my_new_map->naive_bincenter_EnergyB_m_hist);  
		h->SetName("Naive Pol-, T+B");
		h->SetTitle("Naive Pol-");
		h->Rebin(N_rebin);
		h->SetLineColor(kCyan);
		h->SetMarkerColor(kCyan);
		h->SetFillColor(kCyan);
		
		h2 = (TH1D*)my_new_map->naive_bincenter_EnergyB_m_hist->Clone();
		h2->SetName("Naive Pol- B");
		h2->SetTitle("Naive Pol- B");
		h2->Rebin(N_rebin);
		h2->SetLineColor(int(mOrange));
		h2->SetMarkerColor(int(mOrange));
		h2->SetFillColor(int(mOrange));
		
		if(i== the_energyset.size()-1)
		{
			h->GetXaxis()->SetRangeUser(0, 5200);
			h->Draw("b");  // figure out how to fill it in.
		}
		h->Draw("b same");
		h2->Draw("b same");
		
		cout << std::fixed;
		cout << std::setprecision(0);
		cout << "E = " << energy << " keV" << endl;
		cout << "N_top (-)    = " << (double)h0->Integral() << endl;
		cout << "N_bottom (-) =  " << (double)h2->Integral() << endl;
		cout << "N_total (-)  = " << h->Integral() << endl;
		cout << std::setprecision(4);
		cout << "Percent T = " << 100.0*(double)h0->Integral() / h->Integral() << endl;
		cout << endl;
	}
	
	return cm;
}


TCanvas * plot_lineshapecompare_p(TH1D * h_tp, TH1D* h_bp)
{
	TH1D* h_sum = (TH1D*)h_tp->Clone();
	h_sum -> Add(h_bp);
	h_sum->SetLineColor(kCyan);
	h_sum->SetMarkerColor(kCyan);
	h_sum->SetFillColor(kCyan);
	h_sum->SetTitle("Lineshape Integrals, Pol+");
	
	h_bp->SetLineColor(int(mOrange));
	h_bp->SetMarkerColor(int(mOrange));
	h_bp->SetFillColor(int(mOrange));
	
	// print out the bins:
	cout << std::fixed;
	cout << std::setprecision(0);
	
	for(int i=1; i<h_sum->GetNbinsX(); i++)
	{
		if(h_sum->GetBinContent(i) > 0 )
		{
			cout << std::fixed;
			cout << std::setprecision(0);
			cout << "Energy (+) = " << h_sum->GetBinCenter(i) << endl;
			cout << "N_tp = " << h_tp->GetBinContent(i) << ";\tN_bp = " << h_bp->GetBinContent(i) << endl;
			cout << "N_total = " << h_sum->GetBinContent(i) << endl;
			cout << std::setprecision(4);
			cout << "Percent B = " << 100.0*((double)h_bp->GetBinContent(i))/((double)h_sum->GetBinContent(i)) << endl;
			cout << endl;
		}
	}
	
	
	TCanvas * cp_lineint = new TCanvas("cp_lineint", "cp_lineint", 100, 0, 900, 700);
	h_sum->GetXaxis()->SetRangeUser(0, 5200);
	h_sum -> Draw("b");
	h_bp -> Draw("b same");
	gPad->Update();
	
	return cp_lineint;
}
TCanvas * plot_lineshapecompare_m(TH1D * h_tm, TH1D* h_bm)
{
	TH1D* h_sum = (TH1D*)h_tm->Clone();
	h_sum -> Add(h_bm);
	h_sum->SetLineColor(kCyan);
	h_sum->SetMarkerColor(kCyan);
	h_sum->SetFillColor(kCyan);
	h_sum->SetTitle("Lineshape Integrals, Pol-");
	
	h_bm->SetLineColor(int(mOrange));
	h_bm->SetMarkerColor(int(mOrange));
	h_bm->SetFillColor(int(mOrange));

	// print out the bins:
	cout << std::fixed;
	cout << std::setprecision(0);
	
	for(int i=1; i<h_sum->GetNbinsX(); i++)
	{
		if(h_sum->GetBinContent(i) > 0 )
		{
			cout << std::fixed;
			cout << std::setprecision(0);
			cout << "Energy (-) = " << h_sum->GetBinCenter(i) << endl;
			cout << "N_tm = " << h_tm->GetBinContent(i) << ";\tN_bm = " << h_bm->GetBinContent(i) << endl;
			cout << "N_total = " << h_sum->GetBinContent(i) << endl;
			cout << std::setprecision(4);
			cout << "Percent T = " << 100.0*((double)h_tm->GetBinContent(i))/((double)h_sum->GetBinContent(i)) << endl;
			cout << endl;
		}
	}
	
	TCanvas * cm_lineint = new TCanvas("cm_lineint", "cm_lineint", 100, 0, 900, 700);
	h_sum->GetXaxis()->SetRangeUser(0, 5200);
	h_sum -> Draw("b");
	h_bm -> Draw("b same");
	gPad->Update();
	
	return cm_lineint;
}




TH1D * make_inputhist_tp(int N_rebin)
{
	MapSetup * my_new_map;
	double energy;
	double integral;
	TH1D* h;
	
	TH1D * h_tp = CreateHist( string("Naive CountIntegral (T+)"), string("AmplitudeBinning_Ebeta"), int(kBlack), N_rebin);
	for(int i=the_energyset.size()-1; i>=0; i--)
	{
		energy = the_energyset.at(i).the_int;
		
		my_new_map = new MapSetup();
		my_new_map -> LoadFromFile( make_mapname_from_monoenergy(the_energyset.at(i).the_double) );
		
		h = (TH1D*)my_new_map->naive_bincenter_EnergyT_p_hist->Clone();
		integral = h->Integral();
		
		h_tp -> Fill(energy, integral);
		
		//
	//	cout << std::fixed;
	//	cout << std::setprecision(0);
	//	cout << "E = " << energy << " keV" << endl;
	//	cout << "Integrated N_top (+)    =  " << integral << endl;
	//	cout << endl;
	}
	return h_tp;
}
TH1D * make_inputhist_bp(int N_rebin)
{
	MapSetup * my_new_map;
	double energy;
	double integral;
	TH1D* h;
	
	TH1D * h_bp = CreateHist( string("Naive CountIntegral (B+)"), string("AmplitudeBinning_Ebeta"), int(kBlack), N_rebin);
	for(int i=the_energyset.size()-1; i>=0; i--)
	{
		energy = the_energyset.at(i).the_int;
		
		my_new_map = new MapSetup();
		my_new_map -> LoadFromFile( make_mapname_from_monoenergy(the_energyset.at(i).the_double) );
		
		h = (TH1D*)my_new_map->naive_bincenter_EnergyB_p_hist->Clone();
		integral = h->Integral();
		
		h_bp -> Fill(energy, integral);
		
		//
	//	cout << std::fixed;
	//	cout << std::setprecision(0);
	//	cout << "E = " << energy << " keV" << endl;
	//	cout << "Integrated N_top (+)    =  " << integral << endl;
	//	cout << endl;
	}
	return h_bp;
}
TH1D * make_inputhist_tm(int N_rebin)
{
	MapSetup * my_new_map;
	double energy;
	double integral;
	TH1D* h;
	
	TH1D * h_tm = CreateHist( string("Naive CountIntegral (T-)"), string("AmplitudeBinning_Ebeta"), int(kBlack), N_rebin);
	for(int i=the_energyset.size()-1; i>=0; i--)
	{
		energy = the_energyset.at(i).the_int;
		
		my_new_map = new MapSetup();
		my_new_map -> LoadFromFile( make_mapname_from_monoenergy(the_energyset.at(i).the_double) );
		
		h = (TH1D*)my_new_map->naive_bincenter_EnergyT_m_hist->Clone();
		integral = h->Integral();
		
		h_tm -> Fill(energy, integral);
		
		//
	//	cout << std::fixed;
	//	cout << std::setprecision(0);
	//	cout << "E = " << energy << " keV" << endl;
	//	cout << "Integrated N_top (+)    =  " << integral << endl;
	//	cout << endl;
	}
	return h_tm;
}
TH1D * make_inputhist_bm(int N_rebin)
{
	MapSetup * my_new_map;
	double energy;
	double integral;
	TH1D* h;
	
	TH1D * h_bm = CreateHist( string("Naive CountIntegral (B-)"), string("AmplitudeBinning_Ebeta"), int(kBlack), N_rebin);
	for(int i=the_energyset.size()-1; i>=0; i--)
	{
		energy = the_energyset.at(i).the_int;
		
		my_new_map = new MapSetup();
		my_new_map -> LoadFromFile( make_mapname_from_monoenergy(the_energyset.at(i).the_double) );
		
		h = (TH1D*)my_new_map->naive_bincenter_EnergyB_m_hist->Clone();
		integral = h->Integral();
		
		h_bm -> Fill(energy, integral);
		
		//
	//	cout << std::fixed;
	//	cout << std::setprecision(0);
	//	cout << "E = " << energy << " keV" << endl;
	//	cout << "Integrated N_top (+)    =  " << integral << endl;
	//	cout << endl;
	}
	return h_bm;
}



TH1D * make_integralhist_tp(int N_rebin)
{
	MapSetup * my_new_map;
	double energy;
	double integral;
	TH1D* h;
	
	TH1D * h_tp = CreateHist( string("Lineshape CountIntegral (T+) - BB1 Agreement"), string("AmplitudeBinning_Ebeta"), int(kBlack), N_rebin);
	for(int i=the_energyset.size()-1; i>=0; i--)
	{
		energy = the_energyset.at(i).the_int;
		
		my_new_map = new MapSetup();
		my_new_map -> LoadFromFile( make_mapname_from_monoenergy(the_energyset.at(i).the_double) );
		
		h = (TH1D*)my_new_map->measured_bincenter_EnergyT_p_bb1agree->Clone();
		integral = h->Integral();
		
		h_tp -> Fill(energy, integral);
		
		//
		/*
		cout << std::fixed;
		cout << std::setprecision(0);
		cout << "E = " << energy << " keV" << endl;
		cout << "Integrated N_top (+)    =  " << integral << endl;
		cout << endl;
		*/
	}
	h_tp->Sumw2(false);
	return h_tp;
}
TH1D * make_integralhist_bp(int N_rebin)
{
	MapSetup * my_new_map;
	double energy;
	double integral;
	TH1D* h;
	
	TH1D * h_bp = CreateHist( string("Lineshape CountIntegral (B+) - BB1 Agreement"), string("AmplitudeBinning_Ebeta"), int(kBlack), N_rebin);
	for(int i=the_energyset.size()-1; i>=0; i--)
	{
		energy = the_energyset.at(i).the_int;
		
		my_new_map = new MapSetup();
		my_new_map -> LoadFromFile( make_mapname_from_monoenergy(the_energyset.at(i).the_double) );
		
		h = (TH1D*)my_new_map->measured_bincenter_EnergyB_p_bb1agree->Clone();
		integral = h->Integral();
		
		h_bp -> Fill(energy, integral);
		
		//
		/*
		cout << std::fixed;
		cout << std::setprecision(0);
		cout << "E = " << energy << " keV" << endl;
		cout << "Integrated N_bottom (+) = " << integral << endl;
		cout << endl;
		*/
	}
	h_bp->Sumw2(false);
	return h_bp;
}
TH1D * make_integralhist_tm(int N_rebin)
{
	MapSetup * my_new_map;
	double energy;
	double integral;
	TH1D* h;
	
	TH1D * h_tm = CreateHist( string("Lineshape CountIntegral (T-) - BB1 Agreement"), string("AmplitudeBinning_Ebeta"), int(kBlack), N_rebin);
	for(int i=the_energyset.size()-1; i>=0; i--)
	{
		energy = the_energyset.at(i).the_int;
		
		my_new_map = new MapSetup();
		my_new_map -> LoadFromFile( make_mapname_from_monoenergy(the_energyset.at(i).the_double) );
		
		h = (TH1D*)my_new_map->measured_bincenter_EnergyT_m_bb1agree->Clone();
		integral = h->Integral();
		
		h_tm -> Fill(energy, integral);
		
		//
		/*
		cout << std::fixed;
		cout << std::setprecision(0);
		cout << "E = " << energy << " keV" << endl;
		cout << "Integrated N_top (-)    =  " << integral << endl;
		cout << endl;
		*/
	}
	h_tm->Sumw2(false);
	return h_tm;
}
TH1D * make_integralhist_bm(int N_rebin)
{
	MapSetup * my_new_map;
	double energy;
	double integral;
	TH1D* h;
	
	TH1D * h_bm = CreateHist( string("Lineshape CountIntegral (B-) - BB1 Agreement"), string("AmplitudeBinning_Ebeta"), int(kBlack), N_rebin);
	for(int i=the_energyset.size()-1; i>=0; i--)
	{
		energy = the_energyset.at(i).the_int;
		
		my_new_map = new MapSetup();
		my_new_map -> LoadFromFile( make_mapname_from_monoenergy(the_energyset.at(i).the_double) );
		
		h = (TH1D*)my_new_map->measured_bincenter_EnergyB_m_bb1agree->Clone();
		integral = h->Integral();
		
		h_bm -> Fill(energy, integral);
		
		//
		/*
		cout << std::fixed;
		cout << std::setprecision(0);
		cout << "E = " << energy << " keV" << endl;
		cout << "Integrated N_bottom (-) = " << integral << endl;
		cout << endl;
		*/
	}
	h_bm->Sumw2(false);
	return h_bm;
}





















