// ==================================================================== //
// Code by Melissa Anholm
// 
// 
// ==================================================================== //
#include <ncurses.h>

#include "TGraph.h"

#include "location.cpp"
#include "MetaChain.cpp"

//#include "scaleplots_fordan.cpp"
//#include "canvasplots.cpp"
#include "canvasplots.h"  // includes MapUtility.cpp. ..or not.  includes FitUtility.cpp though.  .. or not?
#include "MapUtility.h"  // 

//#include "MapUtility.cpp"
//#include "FitUtility.cpp"
#include "FitUtility.h"
//#include "ColorExtras.cpp"
//#include "GraphExtras.cpp"
//#include "AsymmetryCanvasLibs.cpp"

#include "make_showing_plots.cpp"


#include "TStyle.h"
#include "TCanvas.h"
#include "TApplication.h"
#include "TLegend.h"
#include "TPaveText.h"



#include <algorithm>    // std::max
using std::max;
using std::min;

//using std::vector;
using std::pair;
using std::make_pair;

// --- // --- // --- // --- // --- // --- // --- // --- // --- // --- // --- // --- //
//#define __SHORT_FORM_OF_FILE__ \
(strrchr(__FILE__,'/') ? strrchr(__FILE__,'/')+1 : __FILE__ )
// --- // --- // --- // --- // --- // --- // --- // --- // --- // --- // --- // --- //

// struct set_of_fitresult_pointers
// struct set_of_fitvectors
// class energy_specifier


vector<energy_specifier> load_energies()
{
	vector<energy_specifier> the_energyset;
	the_energyset.push_back(energy_specifier( "500",  500, 0.5));
//	the_energyset.push_back(energy_specifier( "625",  625, 0.625));
	the_energyset.push_back(energy_specifier( "750",  750, 0.75));
	the_energyset.push_back(energy_specifier( "875",  875, 0.875));
	the_energyset.push_back(energy_specifier("1000", 1000, 1.0));
	the_energyset.push_back(energy_specifier("1125", 1125, 1.125));
	the_energyset.push_back(energy_specifier("1250", 1250, 1.25));
	the_energyset.push_back(energy_specifier("1375", 1375, 1.375));
	the_energyset.push_back(energy_specifier("1500", 1500, 1.5));
//	the_energyset.push_back(energy_specifier("1625", 1625, 1.5));
//	the_energyset.push_back(energy_specifier("1750", 1750, 1.5));
//	the_energyset.push_back(energy_specifier("1875", 1875, 1.5));
	the_energyset.push_back(energy_specifier("2000", 2000, 2.0));
	the_energyset.push_back(energy_specifier("2500", 2500, 2.5));
	the_energyset.push_back(energy_specifier("3000", 3000, 3.0));
	the_energyset.push_back(energy_specifier("3500", 3500, 3.5));
	the_energyset.push_back(energy_specifier("4000", 4000, 4.0));
	the_energyset.push_back(energy_specifier("4500", 4500, 4.5));
	the_energyset.push_back(energy_specifier("5000", 5000, 5.0));
	
	return the_energyset;
}
vector<energy_specifier> the_energyset = load_energies();



// --- // --- // --- // --- // --- // --- // --- // --- // --- // --- // --- // --- //
void makesave_maphist(string namestub, double the_monoenergy)
{
	MapSetup * my_new_map = new MapSetup();
	TChain * TreeChain = get_chain_from_monoenergy(the_monoenergy, true);
	my_new_map -> LoadFromTree(TreeChain);
	my_new_map -> AdjustTheColors();
	//
	my_new_map -> save_to_file( make_mapname_from_monoenergy(namestub, the_monoenergy) );
	delete my_new_map;
}
void makesave_fullspectrum_maphist(string namestub)
{
//	double the_monoenergy = -0.01;
	double the_monoenergy = -10.0;
	MapSetup * my_new_map = new MapSetup();
	TChain * TreeChain = get_chain_from_monoenergy(the_monoenergy, true);
	my_new_map -> LoadFromTree(TreeChain);
	my_new_map -> AdjustTheColors();
	//
	my_new_map -> save_to_file( make_fullspectrum_name(namestub) );
	delete my_new_map;
}


void makesave_allthemaphists(string namestub, bool skipsome)
{
	for(int i=0; i<the_energyset.size(); i++)
	{
		int energy = the_energyset.at(i).the_int;
		if(skipsome)  // these are the ones we'll skip:
		{
			if( energy==5000 ) { continue; }  // skip this energy
			if( energy==4500 ) { continue; }  // skip this energy
			if( energy==4000 ) { continue; }  // skip this energy
			if( energy==3500 ) { continue; }  // skip this energy  
			if( energy==3000 ) { continue; }  // skip this energy  // 
			if( energy==2500 ) { continue; }  // skip this energy  // 
			if( energy==2000 ) { continue; }  // skip this energy  // 
			if( energy==1500 ) { continue; }  // skip this energy  // 
			if( energy==1375 ) { continue; }  // skip this energy  // 
			if( energy==1250 ) { continue; }  // skip this energy  // 
			if( energy==1125 ) { continue; }  // skip this energy  
			if( energy==1000 ) { continue; }  // skip this energy  // 
			if( energy== 875 ) { continue; }  // skip this energy  
			if( energy== 750 ) { continue; }  // skip this energy  // whatever, I'm not even looking through this now.
			if( energy== 500 ) { continue; }  // skip this energy  // doesn't even converge
		}
		makesave_maphist(namestub, the_energyset.at(i).the_double);
	}
	makesave_fullspectrum_maphist("map_out_full");
}


set_of_fitresult_pointers makefitset(TH1D* htp, TH1D* htm, TH1D* hbp, TH1D* hbm, int monoenergy_int, TF1 &R_tp, TF1 &R_tm, TF1 &R_bp, TF1 &R_bm, int fitmax_algorithm=3, bool finalfit=true)
//	call as, eg:  set_of_fitresult_pointers fitset = makefitset(htp, htm, hbp, hbm, monoenergy_int, *R_tp, *R_tm, *R_bp, *R_bm, fitmax_algorithm);  
//	this syntax allows the original TF1s to be modified in place.
{
//	cout << "called makefitset(...)" << endl;
	if( !htp || !htm || !hbp || !hbm ) // this isn't actually a failure mode...
	{
		cout << "Can't find the histogram(s)!" << endl;  
	}
//	double lambda_top    = 1.42;
//	double lambda_bottom = 1.32;
	
	double fitmin =  200.0;
	double fitmax = 5500.0;
	
	set_of_fitresult_pointers fitset;
	//
	if(fitmax_algorithm==0)
	{ }
	else if(fitmax_algorithm==1)
	{
		fitmax = monoenergy_int + 1000;
	}
	else if(fitmax_algorithm==2)
	{
		fitmax = monoenergy_int + 500;
	}
	else if(fitmax_algorithm==3)
	{
		int i_tp, i_tm, i_bp, i_bm, i_max;
		i_tp = htp->FindLastBinAbove(0);
		i_tm = htm->FindLastBinAbove(0);
		i_bp = hbp->FindLastBinAbove(0);
		i_bm = hbm->FindLastBinAbove(0);
		i_max = max( max(i_tp, i_tm), max(i_bp, i_bm));
		i_max++;  // first bin that we don't have to include.
		fitmax = htp->GetBinCenter(i_max);
		
		cout << "i_max for monoenergy:  " << monoenergy_int << " is:  " << i_max;// << endl;
		cout << ";\tfitmax is:  " << fitmax << endl;
	}
	else
	{
		cout << "fitmax_algorithm = " << fitmax_algorithm << " is not a recognized value." << endl;
		cout << "You fail." << endl;
		assert(0);
		return fitset;
	}
	bool determined = true;
	
	// L makes it a likelihood fit (can be combined with "M" and "E").  By default it will probably call Migrad.
	// M calls Hesse.  ('improves fit results', by searching for the best local minimum.)
	// E calls Minos.  (better error estimates)
	// --- * --- // --- * --- // --- * --- //
	cout << "... ... ..." << endl;
	cout << "Fitting Top Plus @ " << monoenergy_int << " keV:  " << endl;
	fitset.pointer_tp = htp->Fit(&R_tp, "S", "", fitmin, fitmax);
//	fitset.pointer_tp = htp->Fit(&R_tp, "LS", "", fitmin, fitmax);
//	fitset.pointer_tp = htp->Fit(&R_tp, "MLS", "", fitmin, fitmax);
	if(determined && !(fitset.pointer_tp->IsValid()) )
	{
		cout << "Trying again." << endl;
		fitset.pointer_tp = htp->Fit(&R_tp, "S", "", fitmin, fitmax);
	}
	if(finalfit)
	{
	//	fitset.pointer_tp = htp->Fit(&R_tp, "LES", "", fitmin, fitmax); 
		fitset.pointer_tp = htp->Fit(&R_tp, "ES", "", fitmin, fitmax); 
	}
	//
	cout << "..." << endl;
	cout << "Fitting Top Minus @ " << monoenergy_int << " keV:  " << endl;
	fitset.pointer_tm = htm->Fit(&R_tm, "S", "", fitmin, fitmax); 
//	fitset.pointer_tm = htm->Fit(&R_tm, "LS", "", fitmin, fitmax); 
//	fitset.pointer_tm = htm->Fit(&R_tm, "MLS", "", fitmin, fitmax); 
	if(determined && !(fitset.pointer_tm->IsValid()) )
	{
		cout << "Trying again." << endl;
		fitset.pointer_tm = htm->Fit(&R_tm, "S", "", fitmin, fitmax); 
	}
	if(finalfit)
	{
	//	fitset.pointer_tm = htm->Fit(&R_tm, "LES", "", fitmin, fitmax); 
		fitset.pointer_tm = htm->Fit(&R_tm, "ES", "", fitmin, fitmax); 
	}
	// --- * --- // --- * --- // --- * --- //
	cout << endl << "... ..." << endl;
	cout << "Fitting Bottom Plus @ " << monoenergy_int << " keV:  " << endl;
	fitset.pointer_bp = hbp->Fit(&R_bp, "S", "", fitmin, fitmax); 
//	fitset.pointer_bp = hbp->Fit(&R_bp, "LS", "", fitmin, fitmax); 
//	fitset.pointer_bp = hbp->Fit(&R_bp, "MLS", "", fitmin, fitmax); 
	if(determined && !(fitset.pointer_bp->IsValid()) )
	{
		cout << "Trying again." << endl;
		fitset.pointer_bp = hbp->Fit(&R_bp, "S", "", fitmin, fitmax); 
	}
	if(finalfit)
	{
	//	fitset.pointer_bp = hbp->Fit(&R_bp, "LES", "", fitmin, fitmax); 
		fitset.pointer_bp = hbp->Fit(&R_bp, "ES", "", fitmin, fitmax); 
	}
	//
	cout << "..." << endl;
	cout << "Fitting Bottom Minus @ " << monoenergy_int << " keV:  " << endl;
	fitset.pointer_bm = hbm->Fit(&R_bm, "S", "", fitmin, fitmax); 
//	fitset.pointer_bm = hbm->Fit(&R_bm, "LS", "", fitmin, fitmax); 
//	fitset.pointer_bm = hbm->Fit(&R_bm, "MLS", "", fitmin, fitmax); 
	if(determined && !(fitset.pointer_bm->IsValid()) )
	{
		cout << "Trying again." << endl;
		fitset.pointer_bm = hbm->Fit(&R_bm, "S", "", fitmin, fitmax); 
	}
	if(finalfit)
	{
	//	fitset.pointer_bm = hbm->Fit(&R_bm, "LES", "", fitmin, fitmax); 
		fitset.pointer_bm = hbm->Fit(&R_bm, "ES", "", fitmin, fitmax); 
	}
	return fitset;
}

set_of_fitresult_pointers fittest_params(MapSetup * my_new_map, int monoenergy_int, int fitmax_algorithm=0)  // INCOMPLETE !!!!
{
	set_of_fitresult_pointers fitset;

	TH1D* htp = my_new_map->measured_EnergyT_p_bb1agree;
	TH1D* htm = my_new_map->measured_EnergyT_m_bb1agree;

	TH1D* hbp = my_new_map->measured_EnergyB_p_bb1agree;
	TH1D* hbm = my_new_map->measured_EnergyB_m_bb1agree;

	// Original Fit Function Setup:
	// -- - -- // -- - -- // -- - -- //
	TF1 * R = make_response_function();
	R->SetLineColor(kBlack);
	
	TF1 * R_tp = (TF1*)R->Clone();
	TF1 * R_tm = (TF1*)R->Clone();
	TF1 * R_bp = (TF1*)R->Clone();
	TF1 * R_bm = (TF1*)R->Clone();
	
	// suppose I give it the new toeres and gres.  Does that give me a different result for E0?
	SetParam_toeres(*R_tp, *R_tm, *R_bp, *R_bm, monoenergy_int);
	R_tp -> FixParameter(R_tp->GetParNumber("10_toeres"), R_tp->GetParameter("10_toeres") );
	R_tm -> FixParameter(R_tm->GetParNumber("10_toeres"), R_tm->GetParameter("10_toeres") );
	R_bp -> FixParameter(R_bp->GetParNumber("10_toeres"), R_bp->GetParameter("10_toeres") );
	R_bm -> FixParameter(R_bm->GetParNumber("10_toeres"), R_bm->GetParameter("10_toeres") );
	
	SetParam_gres(*R_tp, *R_tm, *R_bp, *R_bm, monoenergy_int);  // this has changed slightly.  Should re-fix it at some point.
	R_tp -> FixParameter(R_tp->GetParNumber("12_gres"), R_tp->GetParameter("12_gres") );
	R_tm -> FixParameter(R_tm->GetParNumber("12_gres"), R_tm->GetParameter("12_gres") );
	R_bp -> FixParameter(R_bp->GetParNumber("12_gres"), R_bp->GetParameter("12_gres") );
	R_bm -> FixParameter(R_bm->GetParNumber("12_gres"), R_bm->GetParameter("12_gres") );
	
	SetParam_E0(*R_tp, *R_tm, *R_bp, *R_bm, monoenergy_int);
	R_tp -> FixParameter(R_tp->GetParNumber("00_E0"), R_tp->GetParameter("00_E0") );
	R_tm -> FixParameter(R_tm->GetParNumber("00_E0"), R_tm->GetParameter("00_E0") );
	R_bp -> FixParameter(R_bp->GetParNumber("00_E0"), R_bp->GetParameter("00_E0") );
	R_bm -> FixParameter(R_bm->GetParNumber("00_E0"), R_bm->GetParameter("00_E0") );

//	SetParam_DgE_tmp(*R_tp, *R_tm, *R_bp, *R_bm, monoenergy_int);  // A temporary kludge.  We'll want to absorb this info back into E0 later.  But for now, let's fit.
//	R_tp -> FixParameter(R_tp->GetParNumber("13_DgE"), R_tp->GetParameter("13_DgE") );
//	R_tm -> FixParameter(R_tm->GetParNumber("13_DgE"), R_tm->GetParameter("13_DgE") );
//	R_bp -> FixParameter(R_bp->GetParNumber("13_DgE"), R_bp->GetParameter("13_DgE") );
//	R_bm -> FixParameter(R_bm->GetParNumber("13_DgE"), R_bm->GetParameter("13_DgE") );
	R_tp -> FixParameter(R_tp->GetParNumber("13_DgE"), 0 );
	R_tm -> FixParameter(R_tm->GetParNumber("13_DgE"), 0 );
	R_bp -> FixParameter(R_bp->GetParNumber("13_DgE"), 0 );
	R_bm -> FixParameter(R_bm->GetParNumber("13_DgE"), 0 );

	// dE0 describes how far away the two peaks are from one another.
	SetParam_dE0(*R_tp, *R_tm, *R_bp, *R_bm, monoenergy_int);
	R_tp -> FixParameter(R_tp->GetParNumber("14_dE0"), R_tp->GetParameter("14_dE0") );
	R_tm -> FixParameter(R_tm->GetParNumber("14_dE0"), R_tm->GetParameter("14_dE0") );
	R_bp -> FixParameter(R_bp->GetParNumber("14_dE0"), R_bp->GetParameter("14_dE0") );
	R_bm -> FixParameter(R_bm->GetParNumber("14_dE0"), R_bm->GetParameter("14_dE0") );
	
	// W is now fixed like r4.
	SetParam_W(*R_tp, *R_tm, *R_bp, *R_bm, monoenergy_int);  // tried it again after resetting E0.  W is still pretty good.  re-fit it anyway.  it's better now.
	R_tp -> FixParameter(R_tp->GetParNumber("07_W"), R_tp->GetParameter("07_W") );
	R_tm -> FixParameter(R_tm->GetParNumber("07_W"), R_tm->GetParameter("07_W") );
	R_bp -> FixParameter(R_bp->GetParNumber("07_W"), R_bp->GetParameter("07_W") );
	R_bm -> FixParameter(R_bm->GetParNumber("07_W"), R_bm->GetParameter("07_W") );
	
	SetParam_lres(*R_tp, *R_tm, *R_bp, *R_bm, monoenergy_int); 
	R_tp -> FixParameter(R_tp->GetParNumber("11_lres"), R_tp->GetParameter("11_lres") );
	R_tm -> FixParameter(R_tm->GetParNumber("11_lres"), R_tm->GetParameter("11_lres") );
	R_bp -> FixParameter(R_bp->GetParNumber("11_lres"), R_bp->GetParameter("11_lres") );
	R_bm -> FixParameter(R_bm->GetParNumber("11_lres"), R_bm->GetParameter("11_lres") );
	
	SetParam_gfrac(*R_tp, *R_tm, *R_bp, *R_bm, monoenergy_int);
	R_tp -> FixParameter(R_tp->GetParNumber("09_gfrac"), R_tp->GetParameter("09_gfrac") );
	R_tm -> FixParameter(R_tm->GetParNumber("09_gfrac"), R_tm->GetParameter("09_gfrac") );
	R_bp -> FixParameter(R_bp->GetParNumber("09_gfrac"), R_bp->GetParameter("09_gfrac") );
	R_bm -> FixParameter(R_bm->GetParNumber("09_gfrac"), R_bm->GetParameter("09_gfrac") );
	
//	SetParam_alpha_tmp(*R_tp, *R_tm, *R_bp, *R_bm, monoenergy_int);
	SetParam_alpha(*R_tp, *R_tm, *R_bp, *R_bm, monoenergy_int);
	R_tp -> FixParameter(R_tp->GetParNumber("03_alpha"), R_tp->GetParameter("03_alpha") );
	R_tm -> FixParameter(R_tm->GetParNumber("03_alpha"), R_tm->GetParameter("03_alpha") );
	R_bp -> FixParameter(R_bp->GetParNumber("03_alpha"), R_bp->GetParameter("03_alpha") );
	R_bm -> FixParameter(R_bm->GetParNumber("03_alpha"), R_bm->GetParameter("03_alpha") );
	
	// release beta too??
	SetParam_beta(*R_tp, *R_tm, *R_bp, *R_bm, monoenergy_int);
	R_tp -> FixParameter(R_tp->GetParNumber("04_beta"), R_tp->GetParameter("04_beta") );
	R_tm -> FixParameter(R_tm->GetParNumber("04_beta"), R_tm->GetParameter("04_beta") );
	R_bp -> FixParameter(R_bp->GetParNumber("04_beta"), R_bp->GetParameter("04_beta") );
	R_bm -> FixParameter(R_bm->GetParNumber("04_beta"), R_bm->GetParameter("04_beta") );
	
	SetParam_gamma(*R_tp, *R_tm, *R_bp, *R_bm, monoenergy_int);
	R_tp -> FixParameter(R_tp->GetParNumber("05_gamma"), R_tp->GetParameter("05_gamma") );
	R_tm -> FixParameter(R_tm->GetParNumber("05_gamma"), R_tm->GetParameter("05_gamma") );
	R_bp -> FixParameter(R_bp->GetParNumber("05_gamma"), R_bp->GetParameter("05_gamma") );
	R_bm -> FixParameter(R_bm->GetParNumber("05_gamma"), R_bm->GetParameter("05_gamma") );
	
	SetParam_delta(*R_tp, *R_tm, *R_bp, *R_bm, monoenergy_int);
	R_tp -> FixParameter(R_tp->GetParNumber("06_delta"), R_tp->GetParameter("06_delta") );
	R_tm -> FixParameter(R_tm->GetParNumber("06_delta"), R_tm->GetParameter("06_delta") );
	R_bp -> FixParameter(R_bp->GetParNumber("06_delta"), R_bp->GetParameter("06_delta") );
	R_bm -> FixParameter(R_bm->GetParNumber("06_delta"), R_bm->GetParameter("06_delta") );
	
	
//	cout << "Fitting Round 1:  " << endl;
	fitset = makefitset(htp, htm, hbp, hbm, monoenergy_int, *R_tp, *R_tm, *R_bp, *R_bm, fitmax_algorithm, false);  
	
	
	return fitset;
}

set_of_fitresult_pointers fitset_bb1agree(MapSetup * my_new_map, int monoenergy_int, int fitmax_algorithm=0)
{
	set_of_fitresult_pointers fitset;
	
	TH1D* htp = my_new_map->measured_EnergyT_p_bb1agree;
	TH1D* htm = my_new_map->measured_EnergyT_m_bb1agree;

	TH1D* hbp = my_new_map->measured_EnergyB_p_bb1agree;
	TH1D* hbm = my_new_map->measured_EnergyB_m_bb1agree;
	
	// make the 4 response functions.
	// by default ....
	double lambda_top    = 1.42;
	double lambda_bottom = 1.32;
	
	
	// Original Fit Function Setup:
	// -- - -- // -- - -- // -- - -- //
	TF1 * R = make_response_function();
//	R->SetLineColor(kBlue);
	R->SetLineColor(kBlack);
	//
	
//	R -> SetParLimits(R->GetParNumber("00_E0"), monoenergy_int-350.0,   monoenergy_int-280.0);
	R -> SetParameter("00_E0", monoenergy_int-300.0);
//	R -> FixParameter(R->GetParNumber("00_E0"), monoenergy_int-300.0);
	
//	R -> FixParameter(R->GetParNumber("13_DgE"), 0 );

//	R -> FixParameter(R->GetParNumber("14_dE0"),    0.0 );   
//	R -> FixParameter(R->GetParNumber("14_dE0"),  -40.0 );   
	
//	R -> FixParameter(R->GetParNumber("09_gfrac"),  1.0);  // no landau.
//	R -> FixParameter(R->GetParNumber("09_gfrac"),  0.875);  // no landau.
	
	// if W is a straight line:  m = 0.0027; b=379.  -->  W=(0.0027)*(monoenergy)+379.0
//	R -> FixParameter(R->GetParNumber("07_W"),  (0.0027)*((double)monoenergy_int)+379.0 );

//	R -> FixParameter(R->GetParNumber("12_gres"),   1.0 );   // a fraction of toeres.
//	R -> FixParameter(R->GetParNumber("11_lres"),   0.007);  // moyal
//	R -> FixParameter(R->GetParNumber("11_lres"),   0.014);  // moyal

//	R -> FixParameter(R->GetParNumber("11_lres"),   0.02);  // landau

//	R -> SetParLimits(R->GetParNumber("10_toeres"), 0.5, 6.0);
//	R -> SetParameter(R->GetParNumber("10_toeres"), 2.3 );
//	R -> FixParameter(R->GetParNumber("10_toeres"), 2.3 );
	
//	R -> SetParameter(R->GetParNumber("07_W"),  (0.0027)*((double)monoenergy_int)+379.0 );
//	R -> SetParameter(R->GetParNumber("07_W"),  (0.0051)*((double)monoenergy_int)+371.0 );
//	R -> SetParameter(R->GetParNumber("07_W"),  (0.0094)*((double)monoenergy_int)+359.6 );
	
//	R -> SetParameter(R->GetParNumber("07_W"),  340.67 );
//	R -> FixParameter(R->GetParNumber("07_W"),  340.67 );
	
	
	TF1 * R_tp = (TF1*)R->Clone();
	TF1 * R_tm = (TF1*)R->Clone();
	TF1 * R_bp = (TF1*)R->Clone();
	TF1 * R_bm = (TF1*)R->Clone();
	
	
	if(monoenergy_int==500)
	{
		R_tp -> FixParameter(R_tp->GetParNumber("01_norm"), 0 );
		R_tm -> FixParameter(R_tm->GetParNumber("01_norm"), 0 );
		R_bp -> FixParameter(R_bp->GetParNumber("01_norm"), 0 );
		R_bm -> FixParameter(R_bm->GetParNumber("01_norm"), 0 );
		
		R_tp -> FixParameter(R_tp->GetParNumber("03_alpha"), 250);
		R_tm -> FixParameter(R_tm->GetParNumber("03_alpha"), 250 );
		R_bp -> FixParameter(R_bp->GetParNumber("03_alpha"), 250 );
		R_bm -> FixParameter(R_bm->GetParNumber("03_alpha"), 250 );
		
		R_tp -> FixParameter(R_tp->GetParNumber("04_beta"), 1 );
		R_tm -> FixParameter(R_tm->GetParNumber("04_beta"), 1 );
		R_bp -> FixParameter(R_bp->GetParNumber("04_beta"), 1 );
		R_bm -> FixParameter(R_bm->GetParNumber("04_beta"), 1 );
		
		R_tp -> FixParameter(R_tp->GetParNumber("06_delta"), 0.0 );
		R_tm -> FixParameter(R_tm->GetParNumber("06_delta"), 0.0 );
		R_bp -> FixParameter(R_bp->GetParNumber("06_delta"), 0.0 );
		R_bm -> FixParameter(R_bm->GetParNumber("06_delta"), 0.0 );
		
		// delta gets divided by gamma.  don't divide by zero.
		R_tp -> FixParameter(R_tp->GetParNumber("05_gamma"), 0.001 );
		R_tm -> FixParameter(R_tm->GetParNumber("05_gamma"), 0.001 );
		R_bp -> FixParameter(R_bp->GetParNumber("05_gamma"), 0.001 );
		R_bm -> FixParameter(R_bm->GetParNumber("05_gamma"), 0.001 );
		
		R_tp -> FixParameter(R_tp->GetParNumber("13_DgE"), 0 );
		R_tm -> FixParameter(R_tm->GetParNumber("13_DgE"), 0 );
		R_bp -> FixParameter(R_bp->GetParNumber("13_DgE"), 0 );
		R_bm -> FixParameter(R_bm->GetParNumber("13_DgE"), 0 );
		
		R_tp -> FixParameter(R_tp->GetParNumber("07_W"), 350 );
		R_tm -> FixParameter(R_tm->GetParNumber("07_W"), 350 );
		R_bp -> FixParameter(R_bp->GetParNumber("07_W"), 350 );
		R_bm -> FixParameter(R_bm->GetParNumber("07_W"), 350 );
		
		R_tp -> FixParameter(R_tp->GetParNumber("08_k"), 4.3);
		R_tm -> FixParameter(R_tm->GetParNumber("08_k"), 4.3);
		R_bp -> FixParameter(R_bp->GetParNumber("08_k"), 4.3);
		R_bm -> FixParameter(R_bm->GetParNumber("08_k"), 4.3);
	}
	
	
	// suppose I give it the new toeres and gres.  Does that give me a different result for E0?
	SetParam_toeres(*R_tp, *R_tm, *R_bp, *R_bm, monoenergy_int);
	R_tp -> FixParameter(R_tp->GetParNumber("10_toeres"), R_tp->GetParameter("10_toeres") );
	R_tm -> FixParameter(R_tm->GetParNumber("10_toeres"), R_tm->GetParameter("10_toeres") );
	R_bp -> FixParameter(R_bp->GetParNumber("10_toeres"), R_bp->GetParameter("10_toeres") );
	R_bm -> FixParameter(R_bm->GetParNumber("10_toeres"), R_bm->GetParameter("10_toeres") );
	
	SetParam_gres(*R_tp, *R_tm, *R_bp, *R_bm, monoenergy_int);  // this has changed slightly.  Should re-fix it at some point.
	R_tp -> FixParameter(R_tp->GetParNumber("12_gres"), R_tp->GetParameter("12_gres") );
	R_tm -> FixParameter(R_tm->GetParNumber("12_gres"), R_tm->GetParameter("12_gres") );
	R_bp -> FixParameter(R_bp->GetParNumber("12_gres"), R_bp->GetParameter("12_gres") );
	R_bm -> FixParameter(R_bm->GetParNumber("12_gres"), R_bm->GetParameter("12_gres") );
	
	SetParam_E0(*R_tp, *R_tm, *R_bp, *R_bm, monoenergy_int);
	R_tp -> FixParameter(R_tp->GetParNumber("00_E0"), R_tp->GetParameter("00_E0") );
	R_tm -> FixParameter(R_tm->GetParNumber("00_E0"), R_tm->GetParameter("00_E0") );
	R_bp -> FixParameter(R_bp->GetParNumber("00_E0"), R_bp->GetParameter("00_E0") );
	R_bm -> FixParameter(R_bm->GetParNumber("00_E0"), R_bm->GetParameter("00_E0") );

//	SetParam_DgE_tmp(*R_tp, *R_tm, *R_bp, *R_bm, monoenergy_int);  // A temporary kludge.  We'll want to absorb this info back into E0 later.  But for now, let's fit.
//	R_tp -> FixParameter(R_tp->GetParNumber("13_DgE"), R_tp->GetParameter("13_DgE") );
//	R_tm -> FixParameter(R_tm->GetParNumber("13_DgE"), R_tm->GetParameter("13_DgE") );
//	R_bp -> FixParameter(R_bp->GetParNumber("13_DgE"), R_bp->GetParameter("13_DgE") );
//	R_bm -> FixParameter(R_bm->GetParNumber("13_DgE"), R_bm->GetParameter("13_DgE") );
	R_tp -> FixParameter(R_tp->GetParNumber("13_DgE"), 0 );
	R_tm -> FixParameter(R_tm->GetParNumber("13_DgE"), 0 );
	R_bp -> FixParameter(R_bp->GetParNumber("13_DgE"), 0 );
	R_bm -> FixParameter(R_bm->GetParNumber("13_DgE"), 0 );

	// dE0 describes how far away the two peaks are from one another.
	SetParam_dE0(*R_tp, *R_tm, *R_bp, *R_bm, monoenergy_int);
	R_tp -> FixParameter(R_tp->GetParNumber("14_dE0"), R_tp->GetParameter("14_dE0") );
	R_tm -> FixParameter(R_tm->GetParNumber("14_dE0"), R_tm->GetParameter("14_dE0") );
	R_bp -> FixParameter(R_bp->GetParNumber("14_dE0"), R_bp->GetParameter("14_dE0") );
	R_bm -> FixParameter(R_bm->GetParNumber("14_dE0"), R_bm->GetParameter("14_dE0") );
	
	// W is now fixed like r4.
	SetParam_W(*R_tp, *R_tm, *R_bp, *R_bm, monoenergy_int);  // tried it again after resetting E0.  W is still pretty good.  re-fit it anyway.  it's better now.
	R_tp -> FixParameter(R_tp->GetParNumber("07_W"), R_tp->GetParameter("07_W") );
	R_tm -> FixParameter(R_tm->GetParNumber("07_W"), R_tm->GetParameter("07_W") );
	R_bp -> FixParameter(R_bp->GetParNumber("07_W"), R_bp->GetParameter("07_W") );
	R_bm -> FixParameter(R_bm->GetParNumber("07_W"), R_bm->GetParameter("07_W") );
	
	SetParam_lres(*R_tp, *R_tm, *R_bp, *R_bm, monoenergy_int); 
	R_tp -> FixParameter(R_tp->GetParNumber("11_lres"), R_tp->GetParameter("11_lres") );
	R_tm -> FixParameter(R_tm->GetParNumber("11_lres"), R_tm->GetParameter("11_lres") );
	R_bp -> FixParameter(R_bp->GetParNumber("11_lres"), R_bp->GetParameter("11_lres") );
	R_bm -> FixParameter(R_bm->GetParNumber("11_lres"), R_bm->GetParameter("11_lres") );
	
	SetParam_gfrac(*R_tp, *R_tm, *R_bp, *R_bm, monoenergy_int);
	R_tp -> FixParameter(R_tp->GetParNumber("09_gfrac"), R_tp->GetParameter("09_gfrac") );
	R_tm -> FixParameter(R_tm->GetParNumber("09_gfrac"), R_tm->GetParameter("09_gfrac") );
	R_bp -> FixParameter(R_bp->GetParNumber("09_gfrac"), R_bp->GetParameter("09_gfrac") );
	R_bm -> FixParameter(R_bm->GetParNumber("09_gfrac"), R_bm->GetParameter("09_gfrac") );
	
//	SetParam_alpha_tmp(*R_tp, *R_tm, *R_bp, *R_bm, monoenergy_int);
	SetParam_alpha(*R_tp, *R_tm, *R_bp, *R_bm, monoenergy_int);
	R_tp -> FixParameter(R_tp->GetParNumber("03_alpha"), R_tp->GetParameter("03_alpha") );
	R_tm -> FixParameter(R_tm->GetParNumber("03_alpha"), R_tm->GetParameter("03_alpha") );
	R_bp -> FixParameter(R_bp->GetParNumber("03_alpha"), R_bp->GetParameter("03_alpha") );
	R_bm -> FixParameter(R_bm->GetParNumber("03_alpha"), R_bm->GetParameter("03_alpha") );
	
	// release beta too??
	SetParam_beta(*R_tp, *R_tm, *R_bp, *R_bm, monoenergy_int);
	R_tp -> FixParameter(R_tp->GetParNumber("04_beta"), R_tp->GetParameter("04_beta") );
	R_tm -> FixParameter(R_tm->GetParNumber("04_beta"), R_tm->GetParameter("04_beta") );
	R_bp -> FixParameter(R_bp->GetParNumber("04_beta"), R_bp->GetParameter("04_beta") );
	R_bm -> FixParameter(R_bm->GetParNumber("04_beta"), R_bm->GetParameter("04_beta") );
	
	SetParam_gamma(*R_tp, *R_tm, *R_bp, *R_bm, monoenergy_int);
	R_tp -> FixParameter(R_tp->GetParNumber("05_gamma"), R_tp->GetParameter("05_gamma") );
	R_tm -> FixParameter(R_tm->GetParNumber("05_gamma"), R_tm->GetParameter("05_gamma") );
	R_bp -> FixParameter(R_bp->GetParNumber("05_gamma"), R_bp->GetParameter("05_gamma") );
	R_bm -> FixParameter(R_bm->GetParNumber("05_gamma"), R_bm->GetParameter("05_gamma") );
	
	SetParam_delta(*R_tp, *R_tm, *R_bp, *R_bm, monoenergy_int);
	R_tp -> FixParameter(R_tp->GetParNumber("06_delta"), R_tp->GetParameter("06_delta") );
	R_tm -> FixParameter(R_tm->GetParNumber("06_delta"), R_tm->GetParameter("06_delta") );
	R_bp -> FixParameter(R_bp->GetParNumber("06_delta"), R_bp->GetParameter("06_delta") );
	R_bm -> FixParameter(R_bm->GetParNumber("06_delta"), R_bm->GetParameter("06_delta") );
	
	SetParam_scale(*R_tp, *R_tm, *R_bp, *R_bm, monoenergy_int);
	R_tp -> FixParameter(R_tp->GetParNumber("02_scale"), R_tp->GetParameter("02_scale") );
	R_tm -> FixParameter(R_tm->GetParNumber("02_scale"), R_tm->GetParameter("02_scale") );
	R_bp -> FixParameter(R_bp->GetParNumber("02_scale"), R_bp->GetParameter("02_scale") );
	R_bm -> FixParameter(R_bm->GetParNumber("02_scale"), R_bm->GetParameter("02_scale") );

	SetParam_norm(*R_tp, *R_tm, *R_bp, *R_bm, monoenergy_int);
	R_tp -> FixParameter(R_tp->GetParNumber("01_norm"), R_tp->GetParameter("01_norm") );
	R_tm -> FixParameter(R_tm->GetParNumber("01_norm"), R_tm->GetParameter("01_norm") );
	R_bp -> FixParameter(R_bp->GetParNumber("01_norm"), R_bp->GetParameter("01_norm") );
	R_bm -> FixParameter(R_bm->GetParNumber("01_norm"), R_bm->GetParameter("01_norm") );
	
	SetParam_k(*R_tp, *R_tm, *R_bp, *R_bm, monoenergy_int);
	R_tp -> FixParameter(R_tp->GetParNumber("08_k"), R_tp->GetParameter("08_k") );
	R_tm -> FixParameter(R_tm->GetParNumber("08_k"), R_tm->GetParameter("08_k") );
	R_bp -> FixParameter(R_bp->GetParNumber("08_k"), R_bp->GetParameter("08_k") );
	R_bm -> FixParameter(R_bm->GetParNumber("08_k"), R_bm->GetParameter("08_k") );
	
	//
	
	// what if I just fix k to ... something?  then maybe scale and alpha will have less stupid values.  and of course beta.
	// yep, r2 fit still works for making alpha, beta, and scale look pretty, even with k fixed.
//	R_tp -> FixParameter(R_tp->GetParNumber("08_k"), 4.3 );
//	R_tm -> FixParameter(R_tm->GetParNumber("08_k"), 4.3 );
//	R_bp -> FixParameter(R_bp->GetParNumber("08_k"), 4.3 );
//	R_bm -> FixParameter(R_bm->GetParNumber("08_k"), 4.3 );
	

	// r1 is just so we can get norm and fix it.  doesn't come out right if we fix E0 to the thing too..
	cout << "Fitting Round 1:  " << endl;
	fitset = makefitset(htp, htm, hbp, hbm, monoenergy_int, *R_tp, *R_tm, *R_bp, *R_bm, fitmax_algorithm, false);  
	//	this syntax allows the original TF1s to be modified in place and come back to us, where we can do things with them.
	/*
	R_tp -> FixParameter(R_tp->GetParNumber("02_scale"), R_tp->GetParameter("02_scale") );
	R_tm -> FixParameter(R_tm->GetParNumber("02_scale"), R_tm->GetParameter("02_scale") );
	R_bp -> FixParameter(R_bp->GetParNumber("02_scale"), R_bp->GetParameter("02_scale") );
	R_bm -> FixParameter(R_bm->GetParNumber("02_scale"), R_bm->GetParameter("02_scale") );

	R_tp -> FixParameter(R_tp->GetParNumber("01_norm"), R_tp->GetParameter("01_norm") );
	R_tm -> FixParameter(R_tm->GetParNumber("01_norm"), R_tm->GetParameter("01_norm") );
	R_bp -> FixParameter(R_bp->GetParNumber("01_norm"), R_bp->GetParameter("01_norm") );
	R_bm -> FixParameter(R_bm->GetParNumber("01_norm"), R_bm->GetParameter("01_norm") );
	
	
	R_tp -> ReleaseParameter(R_tp->GetParNumber("13_DgE"));
	R_tm -> ReleaseParameter(R_tm->GetParNumber("13_DgE"));
	R_bp -> ReleaseParameter(R_bp->GetParNumber("13_DgE"));
	R_bm -> ReleaseParameter(R_bm->GetParNumber("13_DgE"));
	R_tp -> SetParLimits(R_tp->GetParNumber("13_DgE"), -15, 15);
	R_tm -> SetParLimits(R_tm->GetParNumber("13_DgE"), -15, 15);
	R_bp -> SetParLimits(R_bp->GetParNumber("13_DgE"), -15, 15);
	R_bm -> SetParLimits(R_bm->GetParNumber("13_DgE"), -15, 15);
	
	R_tp -> ReleaseParameter(R_tp->GetParNumber("14_dE0"));
	R_tm -> ReleaseParameter(R_tm->GetParNumber("14_dE0"));
	R_bp -> ReleaseParameter(R_bp->GetParNumber("14_dE0"));
	R_bm -> ReleaseParameter(R_bm->GetParNumber("14_dE0"));
	R_tp -> SetParLimits(R_tp->GetParNumber("14_dE0"), -30, 50);
	R_tm -> SetParLimits(R_tm->GetParNumber("14_dE0"), -30, 50);
	R_bp -> SetParLimits(R_bp->GetParNumber("14_dE0"), -30, 50);
	R_bm -> SetParLimits(R_bm->GetParNumber("14_dE0"), -30, 50);
	
//	R_tp -> ReleaseParameter(R_tp->GetParNumber("12_gres"));
//	R_tm -> ReleaseParameter(R_tm->GetParNumber("12_gres"));
//	R_bp -> ReleaseParameter(R_bp->GetParNumber("12_gres"));
//	R_bm -> ReleaseParameter(R_bm->GetParNumber("12_gres"));
	
	// lres was good immediately before we released all these things.  Need to release it too??  Yes, yes we did.  it's better now.
//	R_tp -> ReleaseParameter(R_tp->GetParNumber("11_lres"));
//	R_tm -> ReleaseParameter(R_tm->GetParNumber("11_lres"));
//	R_bp -> ReleaseParameter(R_bp->GetParNumber("11_lres"));
//	R_bm -> ReleaseParameter(R_bm->GetParNumber("11_lres"));
	
	// we just re-fit gfrac, but we might need to do it again...
	// ok, this time we fit it so good..
//	R_tp -> ReleaseParameter(R_tp->GetParNumber("09_gfrac"));
//	R_tm -> ReleaseParameter(R_tm->GetParNumber("09_gfrac"));
//	R_bp -> ReleaseParameter(R_bp->GetParNumber("09_gfrac"));
//	R_bm -> ReleaseParameter(R_bm->GetParNumber("09_gfrac"));
//	R_tp -> SetParLimits(R_tp->GetParNumber("09_gfrac"), 0.8, 0.999);
//	R_tm -> SetParLimits(R_tm->GetParNumber("09_gfrac"), 0.8, 0.999);
//	R_bp -> SetParLimits(R_bp->GetParNumber("09_gfrac"), 0.8, 0.999);
//	R_bm -> SetParLimits(R_bm->GetParNumber("09_gfrac"), 0.8, 0.999);
	
	// must refit:
	//	lres
	//	gres  -- done.
	//	dE0:  optional.
	//	gfrac:  optional?
	//	DgE:  optional?
	
	
	// what if I release k, too?  to go with lres?
//	R_tp -> ReleaseParameter(R_tp->GetParNumber("08_k"));
//	R_tm -> ReleaseParameter(R_tm->GetParNumber("08_k"));
//	R_bp -> ReleaseParameter(R_bp->GetParNumber("08_k"));
//	R_bm -> ReleaseParameter(R_bm->GetParNumber("08_k"));
	// .... yeah fuck that.
	
	
	if(monoenergy_int==500)
	{
		
	//	R_tp -> FixParameter(R_tp->GetParNumber("11_lres"), R_tp->GetParameter("11_lres") );
	//	R_tm -> FixParameter(R_tm->GetParNumber("11_lres"), R_tm->GetParameter("11_lres") );
	//	R_bp -> FixParameter(R_bp->GetParNumber("11_lres"), R_bp->GetParameter("11_lres") );
	//	R_bm -> FixParameter(R_bm->GetParNumber("11_lres"), R_bm->GetParameter("11_lres") );
		
	//	R_tp -> FixParameter(R_tp->GetParNumber("09_gfrac"), R_tp->GetParameter("09_gfrac") );
	//	R_tm -> FixParameter(R_tm->GetParNumber("09_gfrac"), R_tm->GetParameter("09_gfrac") );
	//	R_bp -> FixParameter(R_bp->GetParNumber("09_gfrac"), R_bp->GetParameter("09_gfrac") );
	//	R_bm -> FixParameter(R_bm->GetParNumber("09_gfrac"), R_bm->GetParameter("09_gfrac") );
		
		R_tp -> FixParameter(R_tp->GetParNumber("14_dE0"), R_tp->GetParameter("14_dE0") );
		R_tm -> FixParameter(R_tm->GetParNumber("14_dE0"), R_tm->GetParameter("14_dE0") );
		R_bp -> FixParameter(R_bp->GetParNumber("14_dE0"), R_bp->GetParameter("14_dE0") );
		R_bm -> FixParameter(R_bm->GetParNumber("14_dE0"), R_bm->GetParameter("14_dE0") );

		R_tp -> FixParameter(R_tp->GetParNumber("13_DgE"), R_tp->GetParameter("13_DgE") );
		R_tm -> FixParameter(R_tm->GetParNumber("13_DgE"), R_tm->GetParameter("13_DgE") );
		R_bp -> FixParameter(R_bp->GetParNumber("13_DgE"), R_bp->GetParameter("13_DgE") );
		R_bm -> FixParameter(R_bm->GetParNumber("13_DgE"), R_bm->GetParameter("13_DgE") );
		
	//	R_tp -> FixParameter(R_tp->GetParNumber("03_alpha"), R_tp->GetParameter("03_alpha") );
	//	R_tm -> FixParameter(R_tm->GetParNumber("03_alpha"), R_tm->GetParameter("03_alpha") );
	//	R_bp -> FixParameter(R_bp->GetParNumber("03_alpha"), R_bp->GetParameter("03_alpha") );
	//	R_bm -> FixParameter(R_bm->GetParNumber("03_alpha"), R_bm->GetParameter("03_alpha") );
	
	//	R_tp -> FixParameter(R_tp->GetParNumber("04_beta"), R_tp->GetParameter("04_beta") );
	//	R_tm -> FixParameter(R_tm->GetParNumber("04_beta"), R_tm->GetParameter("04_beta") );
	//	R_bp -> FixParameter(R_bp->GetParNumber("04_beta"), R_bp->GetParameter("04_beta") );
	//	R_bm -> FixParameter(R_bm->GetParNumber("04_beta"), R_bm->GetParameter("04_beta") );
	}
	
	cout << "Fitting Round 2:  " << endl;
	fitset = makefitset(htp, htm, hbp, hbm, monoenergy_int, *R_tp, *R_tm, *R_bp, *R_bm, fitmax_algorithm, false);  
	*/

//	R_tp -> ReleaseParameter(R_tp->GetParNumber("00_E0"));
//	R_tm -> ReleaseParameter(R_tm->GetParNumber("00_E0"));
//	R_bp -> ReleaseParameter(R_bp->GetParNumber("00_E0"));
//	R_bm -> ReleaseParameter(R_bm->GetParNumber("00_E0"));
//	
//	R_tp -> ReleaseParameter(R_tp->GetParNumber("07_W"));
//	R_tm -> ReleaseParameter(R_tm->GetParNumber("07_W"));
//	R_bp -> ReleaseParameter(R_bp->GetParNumber("07_W"));
//	R_bm -> ReleaseParameter(R_bm->GetParNumber("07_W"));
	
//	R_tp -> ReleaseParameter(R_tp->GetParNumber("03_alpha"));
//	R_tm -> ReleaseParameter(R_tm->GetParNumber("03_alpha"));
//	R_bp -> ReleaseParameter(R_bp->GetParNumber("03_alpha"));
//	R_bm -> ReleaseParameter(R_bm->GetParNumber("03_alpha"));
	
//	R_tp -> ReleaseParameter(R_tp->GetParNumber("04_beta"));
//	R_tm -> ReleaseParameter(R_tm->GetParNumber("04_beta"));
//	R_bp -> ReleaseParameter(R_bp->GetParNumber("04_beta"));
//	R_bm -> ReleaseParameter(R_bm->GetParNumber("04_beta"));
	
	// lres is actually still fine!
	
	// release dE0 too??

	// release lres too?

	/*
	R_tp -> ReleaseParameter(R_tp->GetParNumber("05_gamma"));
	R_tm -> ReleaseParameter(R_tm->GetParNumber("05_gamma"));
	R_bp -> ReleaseParameter(R_bp->GetParNumber("05_gamma"));
	R_bm -> ReleaseParameter(R_bm->GetParNumber("05_gamma"));
	R_tp -> SetParLimits(R_tp->GetParNumber("05_gamma"), 0.3,  0.5);
	R_tm -> SetParLimits(R_tm->GetParNumber("05_gamma"), 0.3,  0.5);
	R_bp -> SetParLimits(R_bp->GetParNumber("05_gamma"), 0.3,  0.5);
	R_bm -> SetParLimits(R_bm->GetParNumber("05_gamma"), 0.3,  0.5);

	R_tp -> ReleaseParameter(R_tp->GetParNumber("06_delta"));
	R_tm -> ReleaseParameter(R_tm->GetParNumber("06_delta"));
	R_bp -> ReleaseParameter(R_bp->GetParNumber("06_delta"));
	R_bm -> ReleaseParameter(R_bm->GetParNumber("06_delta"));
	if(monoenergy_int==500)
	{
		R_tp -> FixParameter(R_tp->GetParNumber("06_delta"), R_tp->GetParameter("06_delta") );
		R_tm -> FixParameter(R_tm->GetParNumber("06_delta"), R_tm->GetParameter("06_delta") );
		R_bp -> FixParameter(R_bp->GetParNumber("06_delta"), R_bp->GetParameter("06_delta") );
		R_bm -> FixParameter(R_bm->GetParNumber("06_delta"), R_bm->GetParameter("06_delta") );
	}

	*/
	
	/*
	// instead of fixing it to the results from the last fit, I think I should re-fitfit DgE, before it's worth it to float alpha and beta again.
//	SetParam_DgE_tmp(*R_tp, *R_tm, *R_bp, *R_bm, monoenergy_int);
//	R_tp -> FixParameter(R_tp->GetParNumber("13_DgE"), R_tp->GetParameter("13_DgE") );
//	R_tm -> FixParameter(R_tm->GetParNumber("13_DgE"), R_tm->GetParameter("13_DgE") );
//	R_bp -> FixParameter(R_bp->GetParNumber("13_DgE"), R_bp->GetParameter("13_DgE") );
//	R_bm -> FixParameter(R_bm->GetParNumber("13_DgE"), R_bm->GetParameter("13_DgE") );
	
//	R_tp -> ReleaseParameter(R_tp->GetParNumber("03_alpha"));
//	R_tm -> ReleaseParameter(R_tm->GetParNumber("03_alpha"));
//	R_bp -> ReleaseParameter(R_bp->GetParNumber("03_alpha"));
//	R_bm -> ReleaseParameter(R_bm->GetParNumber("03_alpha"));
//	R_tp -> SetParLimits(R_tp->GetParNumber("03_alpha"), 100,   1200.0);
//	R_tm -> SetParLimits(R_tm->GetParNumber("03_alpha"), 100,   1200.0);
//	R_bp -> SetParLimits(R_bp->GetParNumber("03_alpha"), 100,   1200.0);
//	R_bm -> SetParLimits(R_bm->GetParNumber("03_alpha"), 100,   1200.0);
	
	R_tp -> ReleaseParameter(R_tp->GetParNumber("04_beta"));
	R_tm -> ReleaseParameter(R_tm->GetParNumber("04_beta"));
	R_bp -> ReleaseParameter(R_bp->GetParNumber("04_beta"));
	R_bm -> ReleaseParameter(R_bm->GetParNumber("04_beta"));
	
	*/
	/*
	// float E0 now?  And W ?  
	// E0 is still good.
	// W is still good too.
	// What if I float W, E0, *and* DgE?  All at once?
	// ... it changes W, and E0 and DgE are a mess.  
	// Let's put W where it goes and look at E0 and DgE.
	
	R_tp -> ReleaseParameter(R_tp->GetParNumber("13_DgE"));
	R_tm -> ReleaseParameter(R_tm->GetParNumber("13_DgE"));
	R_bp -> ReleaseParameter(R_bp->GetParNumber("13_DgE"));
	R_bm -> ReleaseParameter(R_bm->GetParNumber("13_DgE"));
	R_tp -> SetParLimits(R_tp->GetParNumber("13_DgE"), -10, 10);
	R_tm -> SetParLimits(R_tm->GetParNumber("13_DgE"), -10, 10);
	R_bp -> SetParLimits(R_bp->GetParNumber("13_DgE"), -10, 10);
	R_bm -> SetParLimits(R_bm->GetParNumber("13_DgE"), -10, 10);
	
	*/
	
	/*
	// if I only fix gamma when I release these other parameters, does it suddenly come out okay again?
	// I think I need to re-float gfrac for this, maybe...
	// and maybe E0 ?	
//	R_tp -> ReleaseParameter(R_tp->GetParNumber("03_alpha"));
//	R_tm -> ReleaseParameter(R_tm->GetParNumber("03_alpha"));
//	R_bp -> ReleaseParameter(R_bp->GetParNumber("03_alpha"));
//	R_bm -> ReleaseParameter(R_bm->GetParNumber("03_alpha"));

	R_tp -> ReleaseParameter(R_tp->GetParNumber("04_beta"));
	R_tm -> ReleaseParameter(R_tm->GetParNumber("04_beta"));
	R_bp -> ReleaseParameter(R_bp->GetParNumber("04_beta"));
	R_bm -> ReleaseParameter(R_bm->GetParNumber("04_beta"));

//	R_tp -> ReleaseParameter(R_tp->GetParNumber("14_dE0"));
//	R_tm -> ReleaseParameter(R_tm->GetParNumber("14_dE0"));
//	R_bp -> ReleaseParameter(R_bp->GetParNumber("14_dE0"));
//	R_bm -> ReleaseParameter(R_bm->GetParNumber("14_dE0"));
	*/

	/*
	
	// ok, after round 2, I need to make some plots for the meeting tomorrow.  So let's float everything I haven't fitfit already.
	// norm, k, scale, alpha, beta, gamma, gausfrac, lres.
	*/
	/*
	// try fixing gamma to, like, *anything* next.
	double gamma_more = 0.425;
	double gamma_less = 0.385;
	R_tp -> FixParameter(R_tp->GetParNumber("05_gamma"), gamma_less );
	R_tm -> FixParameter(R_tm->GetParNumber("05_gamma"), gamma_more );
	R_bp -> FixParameter(R_bp->GetParNumber("05_gamma"), gamma_more );
	R_bm -> FixParameter(R_bm->GetParNumber("05_gamma"), gamma_less );
	*/
	
//	R_tp -> FixParameter(R_tp->GetParNumber("13_DgE"), 0 );
//	R_tm -> FixParameter(R_tm->GetParNumber("13_DgE"), 0 );
//	R_bp -> FixParameter(R_bp->GetParNumber("13_DgE"), 0 );
//	R_bm -> FixParameter(R_bm->GetParNumber("13_DgE"), 0 );
//	R_tp -> ReleaseParameter(R_tp->GetParNumber("00_E0"));
//	R_tm -> ReleaseParameter(R_tm->GetParNumber("00_E0"));
//	R_bp -> ReleaseParameter(R_bp->GetParNumber("00_E0"));
//	R_bm -> ReleaseParameter(R_bm->GetParNumber("00_E0"));
//	R_tp -> SetParLimits(R_tp->GetParNumber("00_E0"), monoenergy_int-300.0 -40, monoenergy_int-300.0 +20);
//	R_tm -> SetParLimits(R_tm->GetParNumber("00_E0"), monoenergy_int-300.0 -40, monoenergy_int-300.0 +20);
//	R_bp -> SetParLimits(R_bp->GetParNumber("00_E0"), monoenergy_int-300.0 -40, monoenergy_int-300.0 +20);
//	R_bm -> SetParLimits(R_bm->GetParNumber("00_E0"), monoenergy_int-300.0 -40, monoenergy_int-300.0 +20);
	
	/*
	// try floating lres and see what happens.  probably need to float dE0 too.
//	R_tp -> ReleaseParameter(R_tp->GetParNumber("14_dE0"));
//	R_tm -> ReleaseParameter(R_tm->GetParNumber("14_dE0"));
//	R_bp -> ReleaseParameter(R_bp->GetParNumber("14_dE0"));
//	R_bm -> ReleaseParameter(R_bm->GetParNumber("14_dE0"));

	R_tp -> ReleaseParameter(R_tp->GetParNumber("11_lres"));
	R_tm -> ReleaseParameter(R_tm->GetParNumber("11_lres"));
	R_bp -> ReleaseParameter(R_bp->GetParNumber("11_lres"));
	R_bm -> ReleaseParameter(R_bm->GetParNumber("11_lres"));
	R_tp -> SetParLimits(R_tp->GetParNumber("11_lres"), 0, 0.1);
	R_tm -> SetParLimits(R_tm->GetParNumber("11_lres"), 0, 0.1);
	R_bp -> SetParLimits(R_bp->GetParNumber("11_lres"), 0, 0.1);
	R_bm -> SetParLimits(R_bm->GetParNumber("11_lres"), 0, 0.1);
	if(monoenergy_int==500)
	{
		R_tp -> FixParameter(R_tp->GetParNumber("11_lres"), R_tp->GetParameter("11_lres") );
		R_tm -> FixParameter(R_tm->GetParNumber("11_lres"), R_tm->GetParameter("11_lres") );
		R_bp -> FixParameter(R_bp->GetParNumber("11_lres"), R_bp->GetParameter("11_lres") );
		R_bm -> FixParameter(R_bm->GetParNumber("11_lres"), R_bm->GetParameter("11_lres") );
	}
	
//	R_tp -> FixParameter(R_tp->GetParNumber("11_lres"), R_tp->GetParameter("11_lres") );
//	R_tm -> FixParameter(R_tm->GetParNumber("11_lres"), R_tm->GetParameter("11_lres") );
//	R_bp -> FixParameter(R_bp->GetParNumber("11_lres"), R_bp->GetParameter("11_lres") );
//	R_bm -> FixParameter(R_bm->GetParNumber("11_lres"), R_bm->GetParameter("11_lres") );
	
	

	if(monoenergy_int==500)
	{
		R_tp -> FixParameter(R_tp->GetParNumber("09_gfrac"), R_tp->GetParameter("09_gfrac") );
		R_tm -> FixParameter(R_tm->GetParNumber("09_gfrac"), R_tm->GetParameter("09_gfrac") );
		R_bp -> FixParameter(R_bp->GetParNumber("09_gfrac"), R_bp->GetParameter("09_gfrac") );
		R_bm -> FixParameter(R_bm->GetParNumber("09_gfrac"), R_bm->GetParameter("09_gfrac") );
	}

	cout << "Fitting Round 3:  " << endl;
	fitset = makefitset(htp, htm, hbp, hbm, monoenergy_int, *R_tp, *R_tm, *R_bp, *R_bm, fitmax_algorithm, false);  
	*/

	
//	R_tp -> ReleaseParameter(R_tp->GetParNumber("11_lres"));
//	R_tm -> ReleaseParameter(R_tm->GetParNumber("11_lres"));
//	R_bp -> ReleaseParameter(R_bp->GetParNumber("11_lres"));
//	R_bm -> ReleaseParameter(R_bm->GetParNumber("11_lres"));

	
	// let's check toeres again..
//	R_tp -> ReleaseParameter(R_tp->GetParNumber("10_toeres"));
//	R_tm -> ReleaseParameter(R_tm->GetParNumber("10_toeres"));
//	R_bp -> ReleaseParameter(R_bp->GetParNumber("10_toeres"));
//	R_bm -> ReleaseParameter(R_bm->GetParNumber("10_toeres"));
//	R_tp -> SetParLimits(R_tp->GetParNumber("10_toeres"), 0.5, 9.0);
//	R_tm -> SetParLimits(R_tm->GetParNumber("10_toeres"), 0.5, 9.0);
//	R_bp -> SetParLimits(R_bp->GetParNumber("10_toeres"), 0.5, 9.0);
//	R_bm -> SetParLimits(R_bm->GetParNumber("10_toeres"), 0.5, 9.0);

//	gres is already set above, before r1.
//	SetParam_gres(*R_tp, *R_tm, *R_bp, *R_bm, monoenergy_int);  // this has changed slightly.  Should re-fix it at some point. ...done.
//	R_tp -> FixParameter(R_tp->GetParNumber("12_gres"), R_tp->GetParameter("12_gres") );
//	R_tm -> FixParameter(R_tm->GetParNumber("12_gres"), R_tm->GetParameter("12_gres") );
//	R_bp -> FixParameter(R_bp->GetParNumber("12_gres"), R_bp->GetParameter("12_gres") );
//	R_bm -> FixParameter(R_bm->GetParNumber("12_gres"), R_bm->GetParameter("12_gres") );
//	R_tp -> ReleaseParameter(R_tp->GetParNumber("12_gres"));
//	R_tm -> ReleaseParameter(R_tm->GetParNumber("12_gres"));
//	R_bp -> ReleaseParameter(R_bp->GetParNumber("12_gres"));
//	R_bm -> ReleaseParameter(R_bm->GetParNumber("12_gres"));
	
//	R_tp -> ReleaseParameter(R_tp->GetParNumber("13_DgE"));
//	R_tm -> ReleaseParameter(R_tm->GetParNumber("13_DgE"));
//	R_bp -> ReleaseParameter(R_bp->GetParNumber("13_DgE"));
//	R_bm -> ReleaseParameter(R_bm->GetParNumber("13_DgE"));
//	R_tp -> SetParLimits(R_tp->GetParNumber("13_DgE"), -10, 10);
//	R_tm -> SetParLimits(R_tm->GetParNumber("13_DgE"), -10, 10);
//	R_bp -> SetParLimits(R_bp->GetParNumber("13_DgE"), -10, 10);
//	R_bm -> SetParLimits(R_bm->GetParNumber("13_DgE"), -10, 10);
//	SetParam_DgE_tmp(*R_tp, *R_tm, *R_bp, *R_bm, monoenergy_int);  // A temporary kludge.  We'll want to absorb this info back into E0 later.  But for now, let's fit.
//	R_tp -> FixParameter(R_tp->GetParNumber("13_DgE"), R_tp->GetParameter("13_DgE") );
//	R_tm -> FixParameter(R_tm->GetParNumber("13_DgE"), R_tm->GetParameter("13_DgE") );
//	R_bp -> FixParameter(R_bp->GetParNumber("13_DgE"), R_bp->GetParameter("13_DgE") );
//	R_bm -> FixParameter(R_bm->GetParNumber("13_DgE"), R_bm->GetParameter("13_DgE") );

//	R_tp -> ReleaseParameter(R_tp->GetParNumber("14_dE0"));
//	R_tm -> ReleaseParameter(R_tm->GetParNumber("14_dE0"));
//	R_bp -> ReleaseParameter(R_bp->GetParNumber("14_dE0"));
//	R_bm -> ReleaseParameter(R_bm->GetParNumber("14_dE0"));
//	SetParam_dE0(*R_tp, *R_tm, *R_bp, *R_bm, monoenergy_int);
//	R_tp -> FixParameter(R_tp->GetParNumber("14_dE0"), R_tp->GetParameter("14_dE0") );
//	R_tm -> FixParameter(R_tm->GetParNumber("14_dE0"), R_tm->GetParameter("14_dE0") );
//	R_bp -> FixParameter(R_bp->GetParNumber("14_dE0"), R_bp->GetParameter("14_dE0") );
//	R_bm -> FixParameter(R_bm->GetParNumber("14_dE0"), R_bm->GetParameter("14_dE0") );

	// what if I release norm?
	
	
	
	/*
	R_tp -> ReleaseParameter(R_tp->GetParNumber("06_delta"));
	R_tm -> ReleaseParameter(R_tm->GetParNumber("06_delta"));
	R_bp -> ReleaseParameter(R_bp->GetParNumber("06_delta"));
	R_bm -> ReleaseParameter(R_bm->GetParNumber("06_delta"));

	// Fix Norm.  R1 was really only to get it anyway.
	R_tp -> FixParameter(R_tp->GetParNumber("01_norm"), R_tp->GetParameter("01_norm") );
	R_tm -> FixParameter(R_tm->GetParNumber("01_norm"), R_tm->GetParameter("01_norm") );
	R_bp -> FixParameter(R_bp->GetParNumber("01_norm"), R_bp->GetParameter("01_norm") );
	R_bm -> FixParameter(R_bm->GetParNumber("01_norm"), R_bm->GetParameter("01_norm") );
	// Fix E0 to the non-flat thing so we can do the rest of our shit.
	SetParam_E0(*R_tp, *R_tm, *R_bp, *R_bm, monoenergy_int);
	R_tp -> FixParameter(R_tp->GetParNumber("00_E0"), R_tp->GetParameter("00_E0") );
	R_tm -> FixParameter(R_tm->GetParNumber("00_E0"), R_tm->GetParameter("00_E0") );
	R_bp -> FixParameter(R_bp->GetParNumber("00_E0"), R_bp->GetParameter("00_E0") );
	R_bm -> FixParameter(R_bm->GetParNumber("00_E0"), R_bm->GetParameter("00_E0") );
	
	
	// won't do any good to have this float right now.
	R_tp -> FixParameter(R_tp->GetParNumber("13_DgE"), 0 );
	R_tm -> FixParameter(R_tm->GetParNumber("13_DgE"), 0 );
	R_bp -> FixParameter(R_bp->GetParNumber("13_DgE"), 0 );
	R_bm -> FixParameter(R_bm->GetParNumber("13_DgE"), 0 );
	
	
	SetParam_delta(*R_tp, *R_tm, *R_bp, *R_bm, monoenergy_int);
	R_tp -> FixParameter(R_tp->GetParNumber("06_delta"), R_tp->GetParameter("06_delta") );
	R_tm -> FixParameter(R_tm->GetParNumber("06_delta"), R_tm->GetParameter("06_delta") );
	R_bp -> FixParameter(R_bp->GetParNumber("06_delta"), R_bp->GetParameter("06_delta") );
	R_bm -> FixParameter(R_bm->GetParNumber("06_delta"), R_bm->GetParameter("06_delta") );

	// given that we've set E0, is W still correct?  I should probably re-fit it.  Let's see what it does though.
//	SetParam_W(*R_tp, *R_tm, *R_bp, *R_bm, monoenergy_int);
//	R_tp -> FixParameter(R_tp->GetParNumber("07_W"), R_tp->GetParameter("07_W") );
//	R_tm -> FixParameter(R_tm->GetParNumber("07_W"), R_tm->GetParameter("07_W") );
//	R_bp -> FixParameter(R_bp->GetParNumber("07_W"), R_bp->GetParameter("07_W") );
//	R_bm -> FixParameter(R_bm->GetParNumber("07_W"), R_bm->GetParameter("07_W") );
//	R_tp -> ReleaseParameter(R_tp->GetParNumber("07_W"));
//	R_tm -> ReleaseParameter(R_tm->GetParNumber("07_W"));
//	R_bp -> ReleaseParameter(R_bp->GetParNumber("07_W"));
//	R_bm -> ReleaseParameter(R_bm->GetParNumber("07_W"));
//	R_tp -> SetParLimits(R_tp->GetParNumber("07_W"), 250, 400);
//	R_tm -> SetParLimits(R_tm->GetParNumber("07_W"), 250, 400);
//	R_bp -> SetParLimits(R_bp->GetParNumber("07_W"), 250, 400);
//	R_bm -> SetParLimits(R_bm->GetParNumber("07_W"), 250, 400);
	
	//	// fix 05_gamma to the fit values...
	//	SetParam_gamma(*R_tp, *R_tm, *R_bp, *R_bm, monoenergy_int);
	//	R_tp -> FixParameter(R_tp->GetParNumber("05_gamma"), R_tp->GetParameter("05_gamma") );
	//	R_tm -> FixParameter(R_tm->GetParNumber("05_gamma"), R_tm->GetParameter("05_gamma") );
	//	R_bp -> FixParameter(R_bp->GetParNumber("05_gamma"), R_bp->GetParameter("05_gamma") );
	//	R_bm -> FixParameter(R_bm->GetParNumber("05_gamma"), R_bm->GetParameter("05_gamma") );
	
	
	
	
//	if(monoenergy_int==500)  // early return.  500 only has the one parameter, and its been fit in the first round.
//	{	
//		// only do this after we've set E0 to the correct thing though.
//		cout << "Convergence Summary @ E = " << monoenergy_int << " keV:  " << endl;
//		fitset.print_fitconvergence();
//		return fitset;
//	}
		
	cout << "Fitting Round 2:  " << endl;
	fitset = makefitset(htp, htm, hbp, hbm, monoenergy_int, *R_tp, *R_tm, *R_bp, *R_bm, fitmax_algorithm, false);  

	// release the various things!  ....and fit them and fix them again.
	SetParam_gres(*R_tp, *R_tm, *R_bp, *R_bm, monoenergy_int);
	R_tp -> FixParameter(R_tp->GetParNumber("12_gres"), R_tp->GetParameter("12_gres") );
	R_tm -> FixParameter(R_tm->GetParNumber("12_gres"), R_tm->GetParameter("12_gres") );
	R_bp -> FixParameter(R_bp->GetParNumber("12_gres"), R_bp->GetParameter("12_gres") );
	R_bm -> FixParameter(R_bm->GetParNumber("12_gres"), R_bm->GetParameter("12_gres") );

	SetParam_toeres_again(*R_tp, *R_tm, *R_bp, *R_bm, monoenergy_int);
	R_tp -> FixParameter(R_tp->GetParNumber("10_toeres"), R_tp->GetParameter("10_toeres") );
	R_tm -> FixParameter(R_tm->GetParNumber("10_toeres"), R_tm->GetParameter("10_toeres") );
	R_bp -> FixParameter(R_bp->GetParNumber("10_toeres"), R_bp->GetParameter("10_toeres") );
	R_bm -> FixParameter(R_bm->GetParNumber("10_toeres"), R_bm->GetParameter("10_toeres") );

	cout << "Fitting Round 3:  " << endl;
	fitset = makefitset(htp, htm, hbp, hbm, monoenergy_int, *R_tp, *R_tm, *R_bp, *R_bm, fitmax_algorithm, false);  
	
	// after r2, we fixed these things so we could fitfit and re-fitfit toeres and gres, respectively.  Now let's leave them floating and see what happens...
	// maybe also release k ?  ... yeah, that was stupid.  But let's re-fitfit W before r3.
	*/
	
	
	/*
	// Scale and alpha go together.  They're both looking pretty smooth.  or at least, they were before I released k before r2.
	// I could fit them both now, but maybe I'd rather not just yet...
	// I don't know what to float next, so let's just see how this goes.
	R_tp -> FixParameter(R_tp->GetParNumber("02_scale"), R_tp->GetParameter("02_scale") );
	R_tm -> FixParameter(R_tm->GetParNumber("02_scale"), R_tm->GetParameter("02_scale") );
	R_bp -> FixParameter(R_bp->GetParNumber("02_scale"), R_bp->GetParameter("02_scale") );
	R_bm -> FixParameter(R_bm->GetParNumber("02_scale"), R_bm->GetParameter("02_scale") );

	R_tp -> FixParameter(R_tp->GetParNumber("03_alpha"), R_tp->GetParameter("03_alpha") );
	R_tm -> FixParameter(R_tm->GetParNumber("03_alpha"), R_tm->GetParameter("03_alpha") );
	R_bp -> FixParameter(R_bp->GetParNumber("03_alpha"), R_bp->GetParameter("03_alpha") );
	R_bm -> FixParameter(R_bm->GetParNumber("03_alpha"), R_bm->GetParameter("03_alpha") );
	
	// beta is also reasonably smooth.  I could fix it, but k is still a fucking mess.  
	// still, fixed beta might be for the best if I'm going to start fiddling with the center peak...
	R_tp -> FixParameter(R_tp->GetParNumber("04_beta"), R_tp->GetParameter("04_beta") );
	R_tm -> FixParameter(R_tm->GetParNumber("04_beta"), R_tm->GetParameter("04_beta") );
	R_bp -> FixParameter(R_bp->GetParNumber("04_beta"), R_bp->GetParameter("04_beta") );
	R_bm -> FixParameter(R_bm->GetParNumber("04_beta"), R_bm->GetParameter("04_beta") );
	*/

//	R_tp -> ReleaseParameter(R_tp->GetParNumber("12_gres"));
//	R_tm -> ReleaseParameter(R_tm->GetParNumber("12_gres"));
//	R_bp -> ReleaseParameter(R_bp->GetParNumber("12_gres"));
//	R_bm -> ReleaseParameter(R_bm->GetParNumber("12_gres"));
	
	// try releasing toeres and see what happens.  W is still/again free.
//	R_tp -> ReleaseParameter(R_tp->GetParNumber("10_toeres"));
//	R_tm -> ReleaseParameter(R_tm->GetParNumber("10_toeres"));
//	R_bp -> ReleaseParameter(R_bp->GetParNumber("10_toeres"));
//	R_bm -> ReleaseParameter(R_bm->GetParNumber("10_toeres"));
//	R_tp -> SetParLimits(R_tp->GetParNumber("10_toeres"), 0.5, 9.0);
//	R_tm -> SetParLimits(R_tm->GetParNumber("10_toeres"), 0.5, 9.0);
//	R_bp -> SetParLimits(R_bp->GetParNumber("10_toeres"), 0.5, 9.0);
//	R_bm -> SetParLimits(R_bm->GetParNumber("10_toeres"), 0.5, 9.0);

	// actually, let's lock down W.  it'll make things simpler.  ...but it makes toeres come out qualitatively different.  :(
//	SetParam_W(*R_tp, *R_tm, *R_bp, *R_bm, monoenergy_int);
//	R_tp -> FixParameter(R_tp->GetParNumber("07_W"), R_tp->GetParameter("07_W") );
//	R_tm -> FixParameter(R_tm->GetParNumber("07_W"), R_tm->GetParameter("07_W") );
//	R_bp -> FixParameter(R_bp->GetParNumber("07_W"), R_bp->GetParameter("07_W") );
//	R_bm -> FixParameter(R_bm->GetParNumber("07_W"), R_bm->GetParameter("07_W") );
	
//	R_tp -> ReleaseParameter(R_tp->GetParNumber("07_W"));
//	R_tm -> ReleaseParameter(R_tm->GetParNumber("07_W"));
//	R_bp -> ReleaseParameter(R_bp->GetParNumber("07_W"));
//	R_bm -> ReleaseParameter(R_bm->GetParNumber("07_W"));
//	R_tp -> SetParLimits(R_tp->GetParNumber("07_W"), 250, 400);
//	R_tm -> SetParLimits(R_tm->GetParNumber("07_W"), 250, 400);
//	R_bp -> SetParLimits(R_bp->GetParNumber("07_W"), 250, 400);
//	R_bm -> SetParLimits(R_bm->GetParNumber("07_W"), 250, 400);
	
	// floating dE0 decouples the centres of the landau and the gaussian.
//	R_tp -> ReleaseParameter(R_tp->GetParNumber("14_dE0"));
//	R_tm -> ReleaseParameter(R_tm->GetParNumber("14_dE0"));
//	R_bp -> ReleaseParameter(R_bp->GetParNumber("14_dE0"));
//	R_bm -> ReleaseParameter(R_bm->GetParNumber("14_dE0"));
	
	// need to re-fix gamma otherwise the gaussian will drift over there at low E.  Or something.  
//	gamma_more = 0.425;
//	gamma_less = 0.39;
//	R_tp -> FixParameter(R_tp->GetParNumber("05_gamma"), gamma_less );
//	R_tm -> FixParameter(R_tm->GetParNumber("05_gamma"), gamma_more );
//	R_bp -> FixParameter(R_bp->GetParNumber("05_gamma"), gamma_more );
//	R_bm -> FixParameter(R_bm->GetParNumber("05_gamma"), gamma_less );

	
	
	/*
	// unfix gamma and see what happens?
	// try releasing gamma?
	R_tp -> ReleaseParameter(R_tp->GetParNumber("05_gamma"));
	R_tm -> ReleaseParameter(R_tm->GetParNumber("05_gamma"));
	R_bp -> ReleaseParameter(R_bp->GetParNumber("05_gamma"));
	R_bm -> ReleaseParameter(R_bm->GetParNumber("05_gamma"));
	R_tp -> SetParLimits(R_tp->GetParNumber("05_gamma"), 0, 0.8);
	R_tm -> SetParLimits(R_tm->GetParNumber("05_gamma"), 0, 0.8);
	R_bp -> SetParLimits(R_bp->GetParNumber("05_gamma"), 0, 0.8);
	R_bm -> SetParLimits(R_bm->GetParNumber("05_gamma"), 0, 0.8);
	
	cout << "Fitting Round 2:  " << endl;
	fitset = makefitset(htp, htm, hbp, hbm, monoenergy_int, *R_tp, *R_tm, *R_bp, *R_bm, fitmax_algorithm, false);  
	*/
		
//	R_tp -> FixParameter(R_tp->GetParNumber("07_W"), R_tp->GetParameter("07_W") );
//	R_tm -> FixParameter(R_tm->GetParNumber("07_W"), R_tm->GetParameter("07_W") );
//	R_bp -> FixParameter(R_bp->GetParNumber("07_W"), R_bp->GetParameter("07_W") );
//	R_bm -> FixParameter(R_bm->GetParNumber("07_W"), R_bm->GetParameter("07_W") );
	/*
	// alpha and beta will only behave well at higher energies.  E>1000
//	R_tp -> FixParameter(R_tp->GetParNumber("04_beta"), R_tp->GetParameter("04_beta") );
//	R_tm -> FixParameter(R_tm->GetParNumber("04_beta"), R_tm->GetParameter("04_beta") );
//	R_bp -> FixParameter(R_bp->GetParNumber("04_beta"), R_bp->GetParameter("04_beta") );
//	R_bm -> FixParameter(R_bm->GetParNumber("04_beta"), R_bm->GetParameter("04_beta") );
//
//	R_tp -> FixParameter(R_tp->GetParNumber("03_alpha"), R_tp->GetParameter("03_alpha") );
//	R_tm -> FixParameter(R_tm->GetParNumber("03_alpha"), R_tm->GetParameter("03_alpha") );
//	R_bp -> FixParameter(R_bp->GetParNumber("03_alpha"), R_bp->GetParameter("03_alpha") );
//	R_bm -> FixParameter(R_bm->GetParNumber("03_alpha"), R_bm->GetParameter("03_alpha") );
	*/
	/*
	R_tp -> ReleaseParameter(R_tp->GetParNumber("13_DgE"));
	R_tm -> ReleaseParameter(R_tm->GetParNumber("13_DgE"));
	R_bp -> ReleaseParameter(R_bp->GetParNumber("13_DgE"));
	R_bm -> ReleaseParameter(R_bm->GetParNumber("13_DgE"));
	R_tp -> SetParLimits(R_tp->GetParNumber("13_DgE"), -20, 5);
	R_tm -> SetParLimits(R_tm->GetParNumber("13_DgE"), -20, 5);
	R_bp -> SetParLimits(R_bp->GetParNumber("13_DgE"), -20, 5);
	R_bm -> SetParLimits(R_bm->GetParNumber("13_DgE"), -20, 5);

//	R_tp -> ReleaseParameter(R_tp->GetParNumber("14_dE0"));
//	R_tm -> ReleaseParameter(R_tm->GetParNumber("14_dE0"));
//	R_bp -> ReleaseParameter(R_bp->GetParNumber("14_dE0"));
//	R_bm -> ReleaseParameter(R_bm->GetParNumber("14_dE0"));
//	R_tp -> SetParLimits(R_tp->GetParNumber("14_dE0"), -50, 10);
//	R_tm -> SetParLimits(R_tm->GetParNumber("14_dE0"), -50, 10);
//	R_bp -> SetParLimits(R_bp->GetParNumber("14_dE0"), -50, 10);
//	R_bm -> SetParLimits(R_bm->GetParNumber("14_dE0"), -50, 10);


	if(monoenergy_int==500)
	{
		R_tp -> FixParameter(R_tp->GetParNumber("02_scale"), R_tp->GetParameter("02_scale") );
		R_tm -> FixParameter(R_tm->GetParNumber("02_scale"), R_tm->GetParameter("02_scale") );
		R_bp -> FixParameter(R_bp->GetParNumber("02_scale"), R_bp->GetParameter("02_scale") );
		R_bm -> FixParameter(R_bm->GetParNumber("02_scale"), R_bm->GetParameter("02_scale") );
		
		R_tp -> FixParameter(R_tp->GetParNumber("10_toeres"), R_tp->GetParameter("10_toeres") );
		R_tm -> FixParameter(R_tm->GetParNumber("10_toeres"), R_tm->GetParameter("10_toeres") );
		R_bp -> FixParameter(R_bp->GetParNumber("10_toeres"), R_bp->GetParameter("10_toeres") );
		R_bm -> FixParameter(R_bm->GetParNumber("10_toeres"), R_bm->GetParameter("10_toeres") );
		
		R_tp -> FixParameter(R_tp->GetParNumber("05_gamma"), R_tp->GetParameter("05_gamma") );
		R_tm -> FixParameter(R_tm->GetParNumber("05_gamma"), R_tm->GetParameter("05_gamma") );
		R_bp -> FixParameter(R_bp->GetParNumber("05_gamma"), R_bp->GetParameter("05_gamma") );
		R_bm -> FixParameter(R_bm->GetParNumber("05_gamma"), R_bm->GetParameter("05_gamma") );

	}

	cout << "Fitting Round 2:  " << endl;
	fitset = makefitset(htp, htm, hbp, hbm, monoenergy_int, *R_tp, *R_tm, *R_bp, *R_bm, fitmax_algorithm, false);  
	*/

	/*
	R_tp -> ReleaseParameter(R_tp->GetParNumber("12_gres"));
	R_tm -> ReleaseParameter(R_tm->GetParNumber("12_gres"));
	R_bp -> ReleaseParameter(R_bp->GetParNumber("12_gres"));
	R_bm -> ReleaseParameter(R_bm->GetParNumber("12_gres"));

//	R_tp -> ReleaseParameter(R_tp->GetParNumber("11_lres"));
//	R_tm -> ReleaseParameter(R_tm->GetParNumber("11_lres"));
//	R_bp -> ReleaseParameter(R_bp->GetParNumber("11_lres"));
//	R_bm -> ReleaseParameter(R_bm->GetParNumber("11_lres"));


	*/
	
	/*

	R_tp -> ReleaseParameter(R_tp->GetParNumber("13_DgE"));
	R_tm -> ReleaseParameter(R_tm->GetParNumber("13_DgE"));
	R_bp -> ReleaseParameter(R_bp->GetParNumber("13_DgE"));
	R_bm -> ReleaseParameter(R_bm->GetParNumber("13_DgE"));

	*/
	
	/*
//	double toeres_guess = 2.3;
//	R_tp -> FixParameter(R_tp->GetParNumber("10_toeres"), toeres_guess+0.5);
//	R_tm -> FixParameter(R_tm->GetParNumber("10_toeres"), toeres_guess+0.5);
//	R_bp -> FixParameter(R_bp->GetParNumber("10_toeres"), toeres_guess-0.5);
//	R_bm -> FixParameter(R_bm->GetParNumber("10_toeres"), toeres_guess-0.5);

	// currently, this only sets gamma:
	// now also delta:
//	SetParamsLike_tp(*R_tp, monoenergy_int);
//	SetParamsLike_tm(*R_tm, monoenergy_int);
//	SetParamsLike_bp(*R_bp, monoenergy_int);
//	SetParamsLike_bm(*R_bm, monoenergy_int);
	
	R_tp -> FixParameter(R_tp->GetParNumber("05_gamma"), R_tp->GetParameter("05_gamma") );
	R_tm -> FixParameter(R_tm->GetParNumber("05_gamma"), R_tm->GetParameter("05_gamma") );
	R_bp -> FixParameter(R_bp->GetParNumber("05_gamma"), R_bp->GetParameter("05_gamma") );
	R_bm -> FixParameter(R_bm->GetParNumber("05_gamma"), R_bm->GetParameter("05_gamma") );
	R_tp -> FixParameter(R_tp->GetParNumber("06_delta"), R_tp->GetParameter("06_delta") );
	R_tm -> FixParameter(R_tm->GetParNumber("06_delta"), R_tm->GetParameter("06_delta") );
	R_bp -> FixParameter(R_bp->GetParNumber("06_delta"), R_bp->GetParameter("06_delta") );
	R_bm -> FixParameter(R_bm->GetParNumber("06_delta"), R_bm->GetParameter("06_delta") );

	
//	if(monoenergy_int==500)
//	{
	//	R_tp -> FixParameter(R_tp->GetParNumber("03_alpha"), 0 );
	//	R_tm -> FixParameter(R_tm->GetParNumber("03_alpha"), 0 );
	//	R_bp -> FixParameter(R_bp->GetParNumber("03_alpha"), 0 );
	//	R_bm -> FixParameter(R_bm->GetParNumber("03_alpha"), 0 );
	//	
	//	R_tp -> FixParameter(R_tp->GetParNumber("04_beta"), 0 );
	//	R_tm -> FixParameter(R_tm->GetParNumber("04_beta"), 0 );
	//	R_bp -> FixParameter(R_bp->GetParNumber("04_beta"), 0 );
	//	R_bm -> FixParameter(R_bm->GetParNumber("04_beta"), 0 );
//	}
	
	*/
	
	/*
	//
	R_tp -> ReleaseParameter(R_tp->GetParNumber("00_E0"));
	R_tm -> ReleaseParameter(R_tm->GetParNumber("00_E0"));
	R_bp -> ReleaseParameter(R_bp->GetParNumber("00_E0"));
	R_bm -> ReleaseParameter(R_bm->GetParNumber("00_E0"));
	R_tp -> SetParLimits(R_tp->GetParNumber("00_E0"), monoenergy_int-350.0,   monoenergy_int-280.0);
	R_tm -> SetParLimits(R_tm->GetParNumber("00_E0"), monoenergy_int-350.0,   monoenergy_int-280.0);
	R_bp -> SetParLimits(R_bp->GetParNumber("00_E0"), monoenergy_int-350.0,   monoenergy_int-280.0);
	R_bm -> SetParLimits(R_bm->GetParNumber("00_E0"), monoenergy_int-350.0,   monoenergy_int-280.0);
	*/
	/*
	// I *can* fix E0 here.  But do I maybe want it to float as I float gfrac around?
	R_tp -> FixParameter(R_tp->GetParNumber("00_E0"), R_tp->GetParameter("00_E0") );
	R_tm -> FixParameter(R_tm->GetParNumber("00_E0"), R_tm->GetParameter("00_E0") );
	R_bp -> FixParameter(R_bp->GetParNumber("00_E0"), R_bp->GetParameter("00_E0") );
	R_bm -> FixParameter(R_bm->GetParNumber("00_E0"), R_bm->GetParameter("00_E0") );

	if(monoenergy_int==500)
	{
		R_tp -> FixParameter(R_tp->GetParNumber("02_scale"), R_tp->GetParameter("02_scale") );
		R_tm -> FixParameter(R_tm->GetParNumber("02_scale"), R_tm->GetParameter("02_scale") );
		R_bp -> FixParameter(R_bp->GetParNumber("02_scale"), R_bp->GetParameter("02_scale") );
		R_bm -> FixParameter(R_bm->GetParNumber("02_scale"), R_bm->GetParameter("02_scale") );
	}
	
	R_tp -> FixParameter(R_tp->GetParNumber("10_toeres"), R_tp->GetParameter("10_toeres") );
	R_tm -> FixParameter(R_tm->GetParNumber("10_toeres"), R_tm->GetParameter("10_toeres") );
	R_bp -> FixParameter(R_bp->GetParNumber("10_toeres"), R_bp->GetParameter("10_toeres") );
	R_bm -> FixParameter(R_bm->GetParNumber("10_toeres"), R_bm->GetParameter("10_toeres") );

	// but can I release gres here?? .... No.  No I can't.  
//	R_tp -> ReleaseParameter(R_tp->GetParNumber("12_gres"));
//	R_tm -> ReleaseParameter(R_tm->GetParNumber("12_gres"));
//	R_bp -> ReleaseParameter(R_bp->GetParNumber("12_gres"));
//	R_bm -> ReleaseParameter(R_bm->GetParNumber("12_gres"));
//	R_tp -> SetParLimits(R_tp->GetParNumber("12_gres"), 0.8, 2.0);
//	R_tm -> SetParLimits(R_tm->GetParNumber("12_gres"), 0.8, 2.0);
//	R_bp -> SetParLimits(R_bp->GetParNumber("12_gres"), 0.8, 2.0);
//	R_bm -> SetParLimits(R_bm->GetParNumber("12_gres"), 0.8, 2.0);
//	R_tp -> FixParameter(R_tp->GetParNumber("12_gres"), R_tp->GetParameter("12_gres") );
//	R_tm -> FixParameter(R_tm->GetParNumber("12_gres"), R_tm->GetParameter("12_gres") );
//	R_bp -> FixParameter(R_bp->GetParNumber("12_gres"), R_bp->GetParameter("12_gres") );
//	R_bm -> FixParameter(R_bm->GetParNumber("12_gres"), R_bm->GetParameter("12_gres") );

	//	
//	R_tp -> ReleaseParameter(R_tp->GetParNumber("09_gfrac"));
//	R_tm -> ReleaseParameter(R_tm->GetParNumber("09_gfrac"));
//	R_bp -> ReleaseParameter(R_bp->GetParNumber("09_gfrac"));
//	R_bm -> ReleaseParameter(R_bm->GetParNumber("09_gfrac"));
//	R_tp -> SetParLimits(R_tp->GetParNumber("09_gfrac"), 0.5, 0.999);
//	R_tm -> SetParLimits(R_tm->GetParNumber("09_gfrac"), 0.5, 0.999);
//	R_bp -> SetParLimits(R_bp->GetParNumber("09_gfrac"), 0.5, 0.999);
//	R_bm -> SetParLimits(R_bm->GetParNumber("09_gfrac"), 0.5, 0.999);
//	R_tp -> SetParameter(R_tp->GetParNumber("09_gfrac"), 0.9);
//	R_tm -> SetParameter(R_tm->GetParNumber("09_gfrac"), 0.9);
//	R_bp -> SetParameter(R_bp->GetParNumber("09_gfrac"), 0.9);
//	R_bm -> SetParameter(R_bm->GetParNumber("09_gfrac"), 0.9);
	// what if I *fixed* gfrac to 0 here, while I fuck with the other parameters?
	R_tp -> FixParameter(R_tp->GetParNumber("09_gfrac"), 0 );
	R_tm -> FixParameter(R_tm->GetParNumber("09_gfrac"), 0 );
	R_bp -> FixParameter(R_bp->GetParNumber("09_gfrac"), 0 );
	R_bm -> FixParameter(R_bm->GetParNumber("09_gfrac"), 0 );

	R_tp -> ReleaseParameter(R_tp->GetParNumber("11_lres"));
	R_tm -> ReleaseParameter(R_tm->GetParNumber("11_lres"));
	R_bp -> ReleaseParameter(R_bp->GetParNumber("11_lres"));
	R_bm -> ReleaseParameter(R_bm->GetParNumber("11_lres"));
	R_tp -> SetParLimits(R_tp->GetParNumber("11_lres"),  0.005,   0.025);  // max. ~0.05 for landau, ~0.013 for moyal.
	R_tm -> SetParLimits(R_tm->GetParNumber("11_lres"),  0.005,   0.025);
	R_bp -> SetParLimits(R_bp->GetParNumber("11_lres"),  0.005,   0.025);
	R_bm -> SetParLimits(R_bm->GetParNumber("11_lres"),  0.005,   0.025);
	
	R_tp -> SetParLimits(R_tp->GetParNumber("03_alpha"),  0.0,   0.9);
	R_tm -> SetParLimits(R_tm->GetParNumber("03_alpha"),  0.0,   0.9);
	R_bp -> SetParLimits(R_bp->GetParNumber("03_alpha"),  0.0,   0.9);
	R_bm -> SetParLimits(R_bm->GetParNumber("03_alpha"),  0.0,   0.9);
	
	// Fix beta to zero to get the best landau shape.  then k is irrelevant too.  
	R_tp -> FixParameter(R_tp->GetParNumber("04_beta"), 0 );
	R_tm -> FixParameter(R_tm->GetParNumber("04_beta"), 0 );
	R_bp -> FixParameter(R_bp->GetParNumber("04_beta"), 0 );
	R_bm -> FixParameter(R_bm->GetParNumber("04_beta"), 0 );	
	R_tp -> FixParameter(R_tp->GetParNumber("08_k"), R_tp->GetParameter("08_k") );
	R_tm -> FixParameter(R_tm->GetParNumber("08_k"), R_tm->GetParameter("08_k") );
	R_bp -> FixParameter(R_bp->GetParNumber("08_k"), R_bp->GetParameter("08_k") );
	R_bm -> FixParameter(R_bm->GetParNumber("08_k"), R_bm->GetParameter("08_k") );
	
	// release 14_dE0 ??  this improves the chi2 at low energies..
	R_tp -> ReleaseParameter(R_tp->GetParNumber("14_dE0"));
	R_tm -> ReleaseParameter(R_tm->GetParNumber("14_dE0"));
	R_bp -> ReleaseParameter(R_bp->GetParNumber("14_dE0"));
	R_bm -> ReleaseParameter(R_bm->GetParNumber("14_dE0"));

	// release norm too??
//	R_tp -> ReleaseParameter(R_tp->GetParNumber("01_norm"));
//	R_tm -> ReleaseParameter(R_tm->GetParNumber("01_norm"));
//	R_bp -> ReleaseParameter(R_bp->GetParNumber("01_norm"));
//	R_bm -> ReleaseParameter(R_bm->GetParNumber("01_norm"));

	cout << "Fitting Round 2:  " << endl;
	fitset = makefitset(htp, htm, hbp, hbm, monoenergy_int, *R_tp, *R_tm, *R_bp, *R_bm, fitmax_algorithm, false);  
	
	R_tp -> FixParameter(R_tp->GetParNumber("11_lres"), R_tp->GetParameter("11_lres") );
	R_tm -> FixParameter(R_tm->GetParNumber("11_lres"), R_tm->GetParameter("11_lres") );
	R_bp -> FixParameter(R_bp->GetParNumber("11_lres"), R_bp->GetParameter("11_lres") );
	R_bm -> FixParameter(R_bm->GetParNumber("11_lres"), R_bm->GetParameter("11_lres") );
	
	R_tp -> ReleaseParameter(R_tp->GetParNumber("09_gfrac"));
	R_tm -> ReleaseParameter(R_tm->GetParNumber("09_gfrac"));
	R_bp -> ReleaseParameter(R_bp->GetParNumber("09_gfrac"));
	R_bm -> ReleaseParameter(R_bm->GetParNumber("09_gfrac"));
	R_tp -> SetParameter(R_tp->GetParNumber("09_gfrac"), 0.5);
	R_tm -> SetParameter(R_tm->GetParNumber("09_gfrac"), 0.5);
	R_bp -> SetParameter(R_bp->GetParNumber("09_gfrac"), 0.5);
	R_bm -> SetParameter(R_bm->GetParNumber("09_gfrac"), 0.5);
	R_tp -> SetParLimits(R_tp->GetParNumber("09_gfrac"), 0.1, 0.999);
	R_tm -> SetParLimits(R_tm->GetParNumber("09_gfrac"), 0.1, 0.999);
	R_bp -> SetParLimits(R_bp->GetParNumber("09_gfrac"), 0.1, 0.999);
	R_bm -> SetParLimits(R_bm->GetParNumber("09_gfrac"), 0.1, 0.999);
	
	R_tp -> FixParameter(R_tp->GetParNumber("14_dE0"), R_tp->GetParameter("14_dE0") );
	R_tm -> FixParameter(R_tm->GetParNumber("14_dE0"), R_tm->GetParameter("14_dE0") );
	R_bp -> FixParameter(R_bp->GetParNumber("14_dE0"), R_bp->GetParameter("14_dE0") );
	R_bm -> FixParameter(R_bm->GetParNumber("14_dE0"), R_bm->GetParameter("14_dE0") );
//	R_tp -> ReleaseParameter(R_tp->GetParNumber("14_dE0"));
//	R_tm -> ReleaseParameter(R_tm->GetParNumber("14_dE0"));
//	R_bp -> ReleaseParameter(R_bp->GetParNumber("14_dE0"));
//	R_bm -> ReleaseParameter(R_bm->GetParNumber("14_dE0"));
	
	R_tp -> ReleaseParameter(R_tp->GetParNumber("04_beta"));
	R_tm -> ReleaseParameter(R_tm->GetParNumber("04_beta"));
	R_bp -> ReleaseParameter(R_bp->GetParNumber("04_beta"));
	R_bm -> ReleaseParameter(R_bm->GetParNumber("04_beta"));
	R_tp -> SetParLimits(R_tp->GetParNumber("04_beta"), 0, 0.04);
	R_tm -> SetParLimits(R_tm->GetParNumber("04_beta"), 0, 0.04);
	R_bp -> SetParLimits(R_bp->GetParNumber("04_beta"), 0, 0.04);
	R_bm -> SetParLimits(R_bm->GetParNumber("04_beta"), 0, 0.04);
//	R_tp -> ReleaseParameter(R_tp->GetParNumber("08_k"));
//	R_tm -> ReleaseParameter(R_tm->GetParNumber("08_k"));
//	R_bp -> ReleaseParameter(R_bp->GetParNumber("08_k"));
//	R_bm -> ReleaseParameter(R_bm->GetParNumber("08_k"));
	
	if(monoenergy_int==500)
	{
		R_tp -> FixParameter(R_tp->GetParNumber("04_beta"), 0 );
		R_tm -> FixParameter(R_tm->GetParNumber("04_beta"), 0 );
		R_bp -> FixParameter(R_bp->GetParNumber("04_beta"), 0 );
		R_bm -> FixParameter(R_bm->GetParNumber("04_beta"), 0 );
	}
	
//	R_tp -> FixParameter(R_tp->GetParNumber("10_toeres"), 2.3+0.15 );
//	R_tm -> FixParameter(R_tm->GetParNumber("10_toeres"), 2.3+0.15 );
//	R_bp -> FixParameter(R_bp->GetParNumber("10_toeres"), 2.3-0.15 );
//	R_bm -> FixParameter(R_bm->GetParNumber("10_toeres"), 2.3-0.15 );
	
//	R_tp -> ReleaseParameter(R_tp->GetParNumber("12_gres"));
//	R_tm -> ReleaseParameter(R_tm->GetParNumber("12_gres"));
//	R_bp -> ReleaseParameter(R_bp->GetParNumber("12_gres"));
//	R_bm -> ReleaseParameter(R_bm->GetParNumber("12_gres"));
//	R_tp -> SetParLimits(R_tp->GetParNumber("12_gres"), 0.4, 2.0);
//	R_tm -> SetParLimits(R_tm->GetParNumber("12_gres"), 0.4, 2.0);
//	R_bp -> SetParLimits(R_bp->GetParNumber("12_gres"), 0.4, 2.0);
//	R_bm -> SetParLimits(R_bm->GetParNumber("12_gres"), 0.4, 2.0);
	
	R_tp -> ReleaseParameter(R_tp->GetParNumber("01_norm"));
	R_tm -> ReleaseParameter(R_tm->GetParNumber("01_norm"));
	R_bp -> ReleaseParameter(R_bp->GetParNumber("01_norm"));
	R_bm -> ReleaseParameter(R_bm->GetParNumber("01_norm"));

	
	cout << "Fitting Round 3:  " << endl;
	fitset = makefitset(htp, htm, hbp, hbm, monoenergy_int, *R_tp, *R_tm, *R_bp, *R_bm, fitmax_algorithm, false);  
	*/
	
	/*
	R_tp -> FixParameter(R_tp->GetParNumber("11_lres"), R_tp->GetParameter("11_lres") );
	R_tm -> FixParameter(R_tm->GetParNumber("11_lres"), R_tm->GetParameter("11_lres") );
	R_bp -> FixParameter(R_bp->GetParNumber("11_lres"), R_bp->GetParameter("11_lres") );
	R_bm -> FixParameter(R_bm->GetParNumber("11_lres"), R_bm->GetParameter("11_lres") );
	
	R_tp -> ReleaseParameter(R_tp->GetParNumber("09_gfrac"));
	R_tm -> ReleaseParameter(R_tm->GetParNumber("09_gfrac"));
	R_bp -> ReleaseParameter(R_bp->GetParNumber("09_gfrac"));
	R_bm -> ReleaseParameter(R_bm->GetParNumber("09_gfrac"));
	R_tp -> SetParLimits(R_tp->GetParNumber("09_gfrac"), 0.1, 0.999);
	R_tm -> SetParLimits(R_tm->GetParNumber("09_gfrac"), 0.1, 0.999);
	R_bp -> SetParLimits(R_bp->GetParNumber("09_gfrac"), 0.1, 0.999);
	R_bm -> SetParLimits(R_bm->GetParNumber("09_gfrac"), 0.1, 0.999);
	R_tp -> SetParameter(R_tp->GetParNumber("09_gfrac"), 0.5);
	R_tm -> SetParameter(R_tm->GetParNumber("09_gfrac"), 0.5);
	R_bp -> SetParameter(R_bp->GetParNumber("09_gfrac"), 0.5);
	R_bm -> SetParameter(R_bm->GetParNumber("09_gfrac"), 0.5);

	cout << "Fitting Round 3:  " << endl;
	fitset = makefitset(htp, htm, hbp, hbm, monoenergy_int, *R_tp, *R_tm, *R_bp, *R_bm, fitmax_algorithm, false);  
	*/
	
//	R_tp -> FixParameter(R_tp->GetParNumber("10_toeres"), R_tp->GetParameter("10_toeres") );
//	R_tm -> FixParameter(R_tm->GetParNumber("10_toeres"), R_tm->GetParameter("10_toeres") );
//	R_bp -> FixParameter(R_bp->GetParNumber("10_toeres"), R_bp->GetParameter("10_toeres") );
//	R_bm -> FixParameter(R_bm->GetParNumber("10_toeres"), R_bm->GetParameter("10_toeres") );
	
	// 359 is very slightly better than 371.  379 is very slightly worse than those.
	// 340 is better by E0, but but worse by residuals in the relevant area.
//	R_tp -> FixParameter(R_tp->GetParNumber("07_W"), 340.67 );
//	R_tm -> FixParameter(R_tm->GetParNumber("07_W"), 340.67 );
//	R_bp -> FixParameter(R_bp->GetParNumber("07_W"), 340.67 );
//	R_bm -> FixParameter(R_bm->GetParNumber("07_W"), 340.67 );
//	R_tp -> FixParameter(R_tp->GetParNumber("07_W"), (0.0027)*((double)monoenergy_int)+379.0 );
//	R_tm -> FixParameter(R_tm->GetParNumber("07_W"), (0.0027)*((double)monoenergy_int)+379.0 );
//	R_bp -> FixParameter(R_bp->GetParNumber("07_W"), (0.0027)*((double)monoenergy_int)+379.0 );
//	R_bm -> FixParameter(R_bm->GetParNumber("07_W"), (0.0027)*((double)monoenergy_int)+379.0 );
//	R_tp -> FixParameter(R_tp->GetParNumber("07_W"), (0.0051)*((double)monoenergy_int)+371.0 );
//	R_tm -> FixParameter(R_tm->GetParNumber("07_W"), (0.0051)*((double)monoenergy_int)+371.0 );
//	R_bp -> FixParameter(R_bp->GetParNumber("07_W"), (0.0051)*((double)monoenergy_int)+371.0 );
//	R_bm -> FixParameter(R_bm->GetParNumber("07_W"), (0.0051)*((double)monoenergy_int)+371.0 );
//	R_tp -> FixParameter(R_tp->GetParNumber("07_W"), (0.0094)*((double)monoenergy_int)+359.6 );
//	R_tm -> FixParameter(R_tm->GetParNumber("07_W"), (0.0094)*((double)monoenergy_int)+359.6 );
//	R_bp -> FixParameter(R_bp->GetParNumber("07_W"), (0.0094)*((double)monoenergy_int)+359.6 );
//	R_bm -> FixParameter(R_bm->GetParNumber("07_W"), (0.0094)*((double)monoenergy_int)+359.6 );

	
	// Everything seems to be working OK.  E0 is smooth.  We can fix it and turn our attention to gausfrac.
	// This is where I left things last night.
//	R_tp -> FixParameter(R_tp->GetParNumber("00_E0"), R_tp->GetParameter("00_E0") );
//	R_tm -> FixParameter(R_tm->GetParNumber("00_E0"), R_tm->GetParameter("00_E0") );
//	R_bp -> FixParameter(R_bp->GetParNumber("00_E0"), R_bp->GetParameter("00_E0") );
//	R_bm -> FixParameter(R_bm->GetParNumber("00_E0"), R_bm->GetParameter("00_E0") );
	
//	R_tp -> FixParameter(R_tp->GetParNumber("07_W"), (0.0094)*((double)monoenergy_int)+359.6 );
//	R_tm -> FixParameter(R_tm->GetParNumber("07_W"), (0.0094)*((double)monoenergy_int)+359.6 );
//	R_bp -> FixParameter(R_bp->GetParNumber("07_W"), (0.0094)*((double)monoenergy_int)+359.6 );
//	R_bm -> FixParameter(R_bm->GetParNumber("07_W"), (0.0094)*((double)monoenergy_int)+359.6 );
	// 340 then 359 has better residuals as it approaches low energies.  worse chi2 though.  overall:  best yet.
	// next up:  340 then 371.
//	R_tp -> FixParameter(R_tp->GetParNumber("07_W"), (0.0051)*((double)monoenergy_int)+371.0 );
//	R_tm -> FixParameter(R_tm->GetParNumber("07_W"), (0.0051)*((double)monoenergy_int)+371.0 );
//	R_bp -> FixParameter(R_bp->GetParNumber("07_W"), (0.0051)*((double)monoenergy_int)+371.0 );
//	R_bm -> FixParameter(R_bm->GetParNumber("07_W"), (0.0051)*((double)monoenergy_int)+371.0 );

	// 340 and release is best at high and medium energies;  340 is best at lower energies.
	// then, don't let W drop below 340, but let it float *up* to ~400ish.  (which line makes *that* work?!)
	// or... don't let it float up just yet.  release the gausfrac first.  It legit might fix the problem.
//	R_tp -> ReleaseParameter(R_tp->GetParNumber("07_W"));
//	R_tm -> ReleaseParameter(R_tm->GetParNumber("07_W"));
//	R_bp -> ReleaseParameter(R_bp->GetParNumber("07_W"));
//	R_bm -> ReleaseParameter(R_bm->GetParNumber("07_W"));
	
	
	
//	cout << "Fitting Round 3:  " << endl;
//	fitset = makefitset(htp, htm, hbp, hbm, monoenergy_int, *R_tp, *R_tm, *R_bp, *R_bm, fitmax_algorithm, false);  
	
	// E0 is fixed; what if we released W ?
	/*
	// if we release toeres, will everything be destroyed?
	// .. as it turns out, yes, if lres and gres are released too.
	// is it ok with only gfrac released?  .. yes, it's basically fine.
	// I worry that it migth make W more unphysical though.
	// But alas, W needs to be released with toeres, otherwise toeres will be prevented from changing too much.  I think.
	R_tp -> ReleaseParameter(R_tp->GetParNumber("10_toeres"));
	R_tm -> ReleaseParameter(R_tm->GetParNumber("10_toeres"));
	R_bp -> ReleaseParameter(R_bp->GetParNumber("10_toeres"));
	R_bm -> ReleaseParameter(R_bm->GetParNumber("10_toeres"));
	R_tp -> SetParLimits(R_tp->GetParNumber("10_toeres"), 1.5, 6.0);
	R_tm -> SetParLimits(R_tm->GetParNumber("10_toeres"), 1.5, 6.0);
	R_bp -> SetParLimits(R_bp->GetParNumber("10_toeres"), 1.5, 6.0);
	R_bm -> SetParLimits(R_bm->GetParNumber("10_toeres"), 1.5, 6.0);
//	R_tp -> ReleaseParameter(R_tp->GetParNumber("07_W") );
//	R_tm -> ReleaseParameter(R_tm->GetParNumber("07_W") );
//	R_bp -> ReleaseParameter(R_bp->GetParNumber("07_W") );
//	R_bm -> ReleaseParameter(R_bm->GetParNumber("07_W") );
//	R_tp -> SetParLimits(R_tp->GetParNumber("07_W"), 250, 420);
//	R_tm -> SetParLimits(R_tm->GetParNumber("07_W"), 250, 420);
//	R_bp -> SetParLimits(R_bp->GetParNumber("07_W"), 250, 420);
//	R_bm -> SetParLimits(R_bm->GetParNumber("07_W"), 250, 420);
	
	*/
	/*
	// instead of floating gres and lres, I could also just try floating toeres..  
	// but I don't actually know what nominal value "lres" should take..
	R_tp -> ReleaseParameter(R_tp->GetParNumber("12_gres"));
	R_tm -> ReleaseParameter(R_tm->GetParNumber("12_gres"));
	R_bp -> ReleaseParameter(R_bp->GetParNumber("12_gres"));
	R_bm -> ReleaseParameter(R_bm->GetParNumber("12_gres"));
	R_tp -> SetParLimits(R_tp->GetParNumber("12_gres"), 0.8, 2.0);
	R_tm -> SetParLimits(R_tm->GetParNumber("12_gres"), 0.8, 2.0);
	R_bp -> SetParLimits(R_bp->GetParNumber("12_gres"), 0.8, 2.0);
	R_bm -> SetParLimits(R_bm->GetParNumber("12_gres"), 0.8, 2.0);

	R_tp -> ReleaseParameter(R_tp->GetParNumber("11_lres"));
	R_tm -> ReleaseParameter(R_tm->GetParNumber("11_lres"));
	R_bp -> ReleaseParameter(R_bp->GetParNumber("11_lres"));
	R_bm -> ReleaseParameter(R_bm->GetParNumber("11_lres"));
	R_tp -> SetParLimits(R_tp->GetParNumber("11_lres"),  1.0e-4,   0.013);
	R_tm -> SetParLimits(R_tm->GetParNumber("11_lres"),  1.0e-4,   0.013);
	R_bp -> SetParLimits(R_bp->GetParNumber("11_lres"),  1.0e-4,   0.013);
	R_bm -> SetParLimits(R_bm->GetParNumber("11_lres"),  1.0e-4,   0.013);
	*/
	
//	cout << "Fitting Round 3:  " << endl;
//	fitset = makefitset(htp, htm, hbp, hbm, monoenergy_int, *R_tp, *R_tm, *R_bp, *R_bm, fitmax_algorithm, false);  

	/*
	// Most of the parameters look smooth at this point, if I run fit2 with toeres still fixed at least...
	// If I do release toeres here, it makes E0 look slightly worse, but it and other parameters are still smooth.
	// Note that at low E, norm (fixed now) is definitely and obviously wrong when we float toeres.  Must float norm with toeres, when toeres floats.
	// if we do this here though, is there even any point to fitting round 1 ?
	R_tp -> ReleaseParameter(R_tp->GetParNumber("10_toeres"));
	R_tm -> ReleaseParameter(R_tm->GetParNumber("10_toeres"));
	R_bp -> ReleaseParameter(R_bp->GetParNumber("10_toeres"));
	R_bm -> ReleaseParameter(R_bm->GetParNumber("10_toeres"));
	R_tp -> SetParLimits(R_tp->GetParNumber("10_toeres"), 0.5, 6.0);
	R_tm -> SetParLimits(R_tm->GetParNumber("10_toeres"), 0.5, 6.0);
	R_bp -> SetParLimits(R_bp->GetParNumber("10_toeres"), 0.5, 6.0);
	R_bm -> SetParLimits(R_bm->GetParNumber("10_toeres"), 0.5, 6.0);
//	R_tp -> FixParameter(R_tp->GetParNumber("10_toeres"), R_tp->GetParameter("10_toeres") );
//	R_tm -> FixParameter(R_tm->GetParNumber("10_toeres"), R_tm->GetParameter("10_toeres") );
//	R_bp -> FixParameter(R_bp->GetParNumber("10_toeres"), R_bp->GetParameter("10_toeres") );
//	R_bm -> FixParameter(R_bm->GetParNumber("10_toeres"), R_bm->GetParameter("10_toeres") );

	*/
	
	/*
	cout << "Fitting Round 3:  " << endl;
	fitset = makefitset(htp, htm, hbp, hbm, monoenergy_int, *R_tp, *R_tm, *R_bp, *R_bm, fitmax_algorithm, false);  
	
	// Now what?  Let gausfrac float too?
	// should I fix norm and/or E0 again?  ...I could probably do with re-fixing 'norm'.
	R_tp -> FixParameter(R_tp->GetParNumber("01_norm"), R_tp->GetParameter("01_norm") );
	R_tm -> FixParameter(R_tm->GetParNumber("01_norm"), R_tm->GetParameter("01_norm") );
	R_bp -> FixParameter(R_bp->GetParNumber("01_norm"), R_bp->GetParameter("01_norm") );
	R_bm -> FixParameter(R_bm->GetParNumber("01_norm"), R_bm->GetParameter("01_norm") );
	// ...and maybe E0 too.  We'll un-fix it later, maybe?
	// should I fix delta and gamma at this point??  they're *probably* as good as they're going to get for a while...  Toeres too.  I can float gres by itself.
	R_tp -> FixParameter(R_tp->GetParNumber("05_gamma"), R_tp->GetParameter("05_gamma") );
	R_tm -> FixParameter(R_tm->GetParNumber("05_gamma"), R_tm->GetParameter("05_gamma") );
	R_bp -> FixParameter(R_bp->GetParNumber("05_gamma"), R_bp->GetParameter("05_gamma") );
	R_bm -> FixParameter(R_bm->GetParNumber("05_gamma"), R_bm->GetParameter("05_gamma") );

	R_tp -> FixParameter(R_tp->GetParNumber("06_delta"), R_tp->GetParameter("06_delta") );
	R_tm -> FixParameter(R_tm->GetParNumber("06_delta"), R_tm->GetParameter("06_delta") );
	R_bp -> FixParameter(R_bp->GetParNumber("06_delta"), R_bp->GetParameter("06_delta") );
	R_bm -> FixParameter(R_bm->GetParNumber("06_delta"), R_bm->GetParameter("06_delta") );

	R_tp -> FixParameter(R_tp->GetParNumber("10_toeres"), R_tp->GetParameter("10_toeres") );
	R_tm -> FixParameter(R_tm->GetParNumber("10_toeres"), R_tm->GetParameter("10_toeres") );
	R_bp -> FixParameter(R_bp->GetParNumber("10_toeres"), R_bp->GetParameter("10_toeres") );
	R_bm -> FixParameter(R_bm->GetParNumber("10_toeres"), R_bm->GetParameter("10_toeres") );
	
	
	
	// lres is the landau resolution.
	
	
	cout << "Fitting Round 4:  " << endl;
	fitset = makefitset(htp, htm, hbp, hbm, monoenergy_int, *R_tp, *R_tm, *R_bp, *R_bm, fitmax_algorithm, false);  
	*/
	
	// -- - -- // -- - -- // -- - -- //
	
	cout << "Convergence Summary @ E = " << monoenergy_int << " keV:  " << endl;
	fitset.print_fitconvergence();
	
	return fitset;
}

void fit_em_all_bb1agree(/* TFile * canvasfile,*/ int fitmax_algorithm=0, bool skip_theonesthatworked=false)  // overwrites the relevant histogram (many files), with the fit included now too.
{
	double energy;
//	TF1 * R;
	MapSetup * my_new_map;
	set_of_fitresult_pointers theset;
	
	for(int i=the_energyset.size()-1; i>=0; i--)  // count down instead.  (am I off by one?)
	{
		energy = the_energyset.at(i).the_int;
		cout << "i= " << i << ";\tenergy = " << the_energyset.at(i).the_int << endl;
		if(skip_theonesthatworked)  // these are the ones we'll skip:
		{
		//	if( energy==5000 ) { continue; }  // skip this energy
		//	if( energy==4500 ) { continue; }  // skip this energy
		//	if( energy==4000 ) { continue; }  // skip this energy
		//	if( energy==3500 ) { continue; }  // skip this energy  
			if( energy==3000 ) { continue; }  // skip this energy  // 
		//	if( energy==2500 ) { continue; }  // skip this energy  // 
		//	if( energy==2000 ) { continue; }  // skip this energy  // 
		//	if( energy==1500 ) { continue; }  // skip this energy  // 
		//	if( energy==1375 ) { continue; }  // skip this energy  // 
		//	if( energy==1250 ) { continue; }  // skip this energy  // 
		//	if( energy==1125 ) { continue; }  // skip this energy  
		//	if( energy==1000 ) { continue; }  // skip this energy  // 
		//	if( energy== 875 ) { continue; }  // skip this energy  
		//	if( energy== 750 ) { continue; }  // skip this energy  // whatever, I'm not even looking through this now.
		//	if( energy== 500 ) { continue; }  // skip this energy  // doesn't even converge
		}
		
		//
		my_new_map = new MapSetup();
		my_new_map -> LoadFromFile( make_mapname_from_monoenergy(the_energyset.at(i).the_double) );
		//
		theset = fitset_bb1agree(my_new_map, the_energyset.at(i).the_int, fitmax_algorithm);
		
		TFile * f = my_new_map->RecreateAndLeaveOpen();  // why are we doing this here?  it's so the histograms will now be saved with their fit functions, that we've *just* created..
		
		f->cd();
		theset.pointer_tp -> Write("",TObject::kOverwrite);
		theset.pointer_tm -> Write("",TObject::kOverwrite);
		theset.pointer_bp -> Write("",TObject::kOverwrite);
		theset.pointer_bm -> Write("",TObject::kOverwrite);
		f->Close();  // close the multi-files when you're done with them.  
		
		/*
		// put the saving of fitplotcans (makesave_fitplotcans_bb1agree) in *here*.  
		string extraname;
		int n_rebin = 10;
		
		extraname = "cp_fitview_"+int_to_string( the_energyset.at(i).the_int )+"_rebin"+int_to_string(n_rebin);
		TCanvas * cp = make_bb1agreecanvas_p(my_new_map, n_rebin, true, extraname, the_energyset.at(i).the_int);
		
		extraname = "cm_fitview_"+int_to_string( the_energyset.at(i).the_int )+"_rebin"+int_to_string(n_rebin);
		TCanvas * cm = make_bb1agreecanvas_m(my_new_map, n_rebin, true, extraname, the_energyset.at(i).the_int);
		
		canvasfile -> cd();
		cp -> Write("",TObject::kOverwrite);
		cm -> Write("",TObject::kOverwrite);
		*/
	}
}

void makesave_fitplotcans_bb1agree(TFile * f)  // 
{
	string extraname;
	int n_rebin = 10;
	for(int i=0; i<the_energyset.size(); i++)
	{
		MapSetup* my_new_map = new MapSetup();
		my_new_map -> LoadFromFile( make_mapname_from_monoenergy(the_energyset.at(i).the_double) );
		
		extraname = "cp_fitview_"+int_to_string( the_energyset.at(i).the_int )+"_rebin"+int_to_string(n_rebin);
		TCanvas * cp = make_bb1agreecanvas_p(my_new_map, n_rebin, true, extraname, the_energyset.at(i).the_int);
		
		extraname = "cm_fitview_"+int_to_string( the_energyset.at(i).the_int )+"_rebin"+int_to_string(n_rebin);
		TCanvas * cm = make_bb1agreecanvas_m(my_new_map, n_rebin, true, extraname, the_energyset.at(i).the_int);
		
		f -> cd();
		cp -> Write("",TObject::kOverwrite);
		cm -> Write("",TObject::kOverwrite);
	}
	cout << "The fitplot canvases have been saved to:  " << f->GetName() << " (and it's still open)" << endl;
}

void makesave_alltheplotcans(TFile * f, int n_rebin=1, bool plot_ff=false)  // function should only be called when we add more data.  call from main().
{
	string extraname;
//	for(double energy=0.5; energy<=5.0; energy+=0.5)
	for(int i=0; i<the_energyset.size(); i++)
	{
		MapSetup* my_new_map = new MapSetup();
		my_new_map -> LoadFromFile( make_mapname_from_monoenergy(the_energyset.at(i).the_double) );
		
		extraname = int_to_string( the_energyset.at(i).the_int )+"_rebin"+int_to_string(n_rebin);
		
		TCanvas * cp = make_plotcanvas_p(my_new_map, n_rebin, plot_ff, "cp_"+extraname, the_energyset.at(i).the_int ); 
		TCanvas * cm = make_plotcanvas_m(my_new_map, n_rebin, plot_ff, "cm_"+extraname, the_energyset.at(i).the_int );
		
		f -> cd();
		
		cp -> Write("",TObject::kOverwrite);
		cm -> Write("",TObject::kOverwrite);
	}
	cout << "All the plot canvases have been saved to " << f->GetName() << endl;
	return;
}
set_of_fitresult_pointers get_fitresults_bb1agree_fromfile(TFile * f)
{
	set_of_fitresult_pointers my_fits;
	my_fits.pointer_tp = (TFitResult*)f->Get("TFitResult-Measured ScintT Energy(+) - BB1 Agreement-R");
	my_fits.pointer_tm = (TFitResult*)f->Get("TFitResult-Measured ScintT Energy(-) - BB1 Agreement-R");
	my_fits.pointer_bp = (TFitResult*)f->Get("TFitResult-Measured ScintB Energy(+) - BB1 Agreement-R");
	my_fits.pointer_bm = (TFitResult*)f->Get("TFitResult-Measured ScintB Energy(-) - BB1 Agreement-R");
	

//  KEY: TFitResult	TFitResult-Measured ScintT Energy(+) - BB1 Agreement-R;1	TFitResult-Measured ScintT Energy(+) - BB1 Agreement
//  KEY: TFitResult	TFitResult-Measured ScintT Energy(-) - BB1 Agreement-R;1	TFitResult-Measured ScintT Energy(-) - BB1 Agreement
//  KEY: TFitResult	TFitResult-Measured ScintB Energy(+) - BB1 Agreement-R;1	TFitResult-Measured ScintB Energy(+) - BB1 Agreement
//  KEY: TFitResult	TFitResult-Measured ScintB Energy(-) - BB1 Agreement-R;1	TFitResult-Measured ScintB Energy(-) - BB1 Agreement

	return my_fits;
}
set_of_fitresult_pointers get_fitresults_bb1agree(double the_energy_double) // looks in make_mapname_from_monoenergy(the_energy_double).
{
	TFile * f = new TFile( make_mapname_from_monoenergy( the_energy_double ).c_str() );
	set_of_fitresult_pointers my_fits = get_fitresults_bb1agree_fromfile( f );
	f->Close();
	return my_fits;
}


void makesave_allthe_residucans(TFile * f, int N_rebin=1)
{
	TCanvas * c;
//	string namestub;
	TH1D* h;
	TH1D* h_mono;
	MapSetup* my_new_map = new MapSetup();

//	// Top +
//	for(int i=0; i<the_energyset.size(); i++)
//	{
	for(int i=the_energyset.size()-1; i>=0; i--) // count down instead.
	{
		my_new_map -> LoadFromFile( make_mapname_from_monoenergy(the_energyset.at(i).the_double) );
		
		h = my_new_map->measured_EnergyT_p_bb1agree;
		h_mono = my_new_map->naive_EnergyT_p_hist;
		c = make_better_residucan(h, h_mono, "Top (+)", the_energyset.at(i), N_rebin);
		
		f->cd();
		c->Write("",TObject::kOverwrite);
//	}
//	// Top -
//	for(int i=0; i<the_energyset.size(); i++)
//	{
//		my_new_map -> LoadFromFile( make_mapname_from_monoenergy(the_energyset.at(i).the_double) );
		h = my_new_map->measured_EnergyT_m_bb1agree;
		h_mono = my_new_map->naive_EnergyT_m_hist;
		c = make_better_residucan(h, h_mono, "Top (-)", the_energyset.at(i), N_rebin);
		
		f->cd();
		c->Write("",TObject::kOverwrite);
//	}
//	// Bottom +
//	for(int i=0; i<the_energyset.size(); i++)
//	{
//		my_new_map -> LoadFromFile( make_mapname_from_monoenergy(the_energyset.at(i).the_double) );
		h = my_new_map->measured_EnergyB_p_bb1agree;
		h_mono = my_new_map->naive_EnergyB_p_hist;
		c = make_better_residucan(h, h_mono, "Bottom (+)", the_energyset.at(i), N_rebin);
		
		f->cd();
		c->Write("",TObject::kOverwrite);
//	}
//	// Bottom -
//	for(int i=0; i<the_energyset.size(); i++)
//	{
//		my_new_map -> LoadFromFile( make_mapname_from_monoenergy(the_energyset.at(i).the_double) );
		h = my_new_map->measured_EnergyB_m_bb1agree;
		h_mono = my_new_map->naive_EnergyB_m_hist;
	//	c = make_one_residucan(h, h_mono, "Bottom (-)", the_energyset.at(i), N_rebin);
		c = make_better_residucan(h, h_mono, "Bottom (-)", the_energyset.at(i), N_rebin);
		
		f->cd();
		c->Write("",TObject::kOverwrite);
	}
	cout << "The residucans have been saved to (root) file." << endl;
}

// --- // --- // --- // --- // --- // --- // --- // --- // --- // --- // --- // --- //
// Look through a file and save all canvases into a PDF format.
void saveall_as_pdf(TFile * f, string pdf_outname)
{
	TCanvas * c = new TCanvas();
	c -> Print( (pdf_outname+"[").c_str() );
	
	for(auto k : *f->GetListOfKeys()) 
	{
		TKey *key = static_cast<TKey*>(k);
		TClass *cl = gROOT->GetClass(key->GetClassName());
		if (!cl->InheritsFrom("TCanvas")) continue;
		c = key->ReadObject<TCanvas>();
		c -> Print( pdf_outname.c_str() );
	}
	c -> Print( (pdf_outname+"]").c_str() );
	
	return;
}
// --- // --- //
// Print all the fit convergence info:
void printall_fitconvergence()
{
	set_of_fitresult_pointers the_fitresults;
	for(int i=the_energyset.size()-1; i>=0; i--)  // count down instead.
	{
		the_fitresults = get_fitresults_bb1agree(the_energyset.at(i).the_double);
		
		cout << "** ** ** " << endl;
		cout << "Convergence Summary @ E = " << the_energyset.at(i).the_int << " keV:  " << endl;
		the_fitresults.print_fitconvergence();
	}
	
	return;
}

// --- // --- // --- // --- // --- // --- // --- // --- // --- // --- // --- // --- //

int main(int argc, char *argv[]) 
{
	setup_location();
	TApplication* rootapp = 0;
	char ** mychar = NULL;
	TFile * f;
	rootapp = new TApplication("blarg",0, mychar);
	
	/*
	bool skipsome = false;
	makesave_allthemaphists("map_out", skipsome);  // do this to generate the maps from the ttrees.  saves to many individual mono-files.  Do this when you add new data.
	// Make a set of plot canvases:
//	f = new TFile("just_the_hists.root", "UPDATE");
//	makesave_alltheplotcans(f, 1, false);  // overwrites individual many-colored plot canvases to canvases.root.  Only need to do this when we add more data.
//	f->Close();
	*/
	
	/*
//	f = new TFile("canvases.root", "UPDATE");
	f = new TFile("canvases.root", "RECREATE");
	// 0 = fit the whole damn thing
	// 1 = monoenergy+1000
	// 2 = monoenergy+500
	// 3 = to last nonzero bin in the four-set of hists.
	int fitmax_algorithm=3;  // 3 will be the new default.
	bool skip_theonesthatworked=false;
	fit_em_all_bb1agree(fitmax_algorithm, skip_theonesthatworked);  // loops through fits, then saves the fitresults back to the (multiple) files with the histograms they're fit to.  Saves many fitplotcans to one single multi-file.  Makes makesave_fitplotcans_bb1agree obsolete.
//	makesave_fitplotcans_bb1agree(f);  // just plots the spectra with the fits.  no residuals, no components.
	
	makesave_fitresultgraphs_and_cans_bb1agree(f);  // saves graphs and canvases for the fit parameters at many energies to canvases.root.
	makesave_allthe_residucans(f, 10);
	f->Close();
	
	// Make the PDF:
	f = new TFile("canvases.root");  // only open for reading now.
	saveall_as_pdf(f, "output_canvases.pdf");
	f->Close();
	
	printall_fitconvergence();  // 
	*/
	
	/*
	// make a fit test canvas set?  
	double energy;
	MapSetup * my_new_map;
	set_of_fitresult_pointers theset;
	
	for(int i=the_energyset.size()-1; i>=0; i--)  // count down instead.  (am I off by one?)
	{
		my_new_map = new MapSetup();
		my_new_map -> LoadFromFile( make_mapname_from_monoenergy(the_energyset.at(i).the_double) );
		//
		theset = fittest_params(my_new_map, the_energyset.at(i).the_int, fitmax_algorithm);
	
		TFile * f = my_new_map->RecreateAndLeaveOpen();  
		f->cd();
		theset.pointer_tp -> Write("",TObject::kOverwrite);
		theset.pointer_tm -> Write("",TObject::kOverwrite);
		theset.pointer_bp -> Write("",TObject::kOverwrite);
		theset.pointer_bm -> Write("",TObject::kOverwrite);
		f->Close();  // close the multi-files when you're done with them.  
	}
	*/
	
//	int N_rebin = 9*5*3;
	int N_rebin = 9*5;
	
	
	TCanvas * cp = plot_inputcompare_p(N_rebin);
	TCanvas * cm = plot_inputcompare_m(N_rebin);
	
	/*
//	TH1D * h_tp_naive = make_inputhist_tp(N_rebin);
//	TH1D * h_bp_naive = make_inputhist_bp(N_rebin);
//	TH1D * h_tm_naive = make_inputhist_tm(N_rebin);
//	TH1D * h_bm_naive = make_inputhist_bm(N_rebin);
	*/
	
	TH1D * h_tp =  make_integralhist_tp(N_rebin);
	TH1D * h_bp =  make_integralhist_bp(N_rebin);
	TH1D * h_tm =  make_integralhist_tm(N_rebin);
	TH1D * h_bm =  make_integralhist_bm(N_rebin);
	
	TCanvas * cp_int = plot_lineshapecompare_p(h_tp, h_bp);
	TCanvas * cm_int = plot_lineshapecompare_m(h_tm, h_bm);
		
	
	cout << "Running the rootapp." << endl;
	rootapp->Run();
	beep();
	beep();
	beep();
	return 0;
}




























