// ==================================================================== //
// Code by Melissa Anholm
// 
// ==================================================================== //

// --- // --- // --- // --- // --- // --- // --- // --- // --- // --- // --- // --- //
string make_mapnamekludge_from_monoenergy(string namestub, double monoenergy)  // energy in MeV.
{
	int monoenergy_int = int(monoenergy*1000.0);
	std::stringstream ss;
	ss << namestub << "_countkludge_" << monoenergy_int << ".root";
	
	string thename = ss.str();
	return thename;
}
string make_mapnamekludge_from_monoenergy(double monoenergy)  // energy in MeV.
{
	string namestub = "map_out";
	return make_mapnamekludge_from_monoenergy(namestub, monoenergy);
}

// --- // --- // --- // --- // --- // --- // --- // --- // --- // --- // --- // --- //
// --- // --- // --- // --- // --- // --- // --- // --- // --- // --- // --- // --- //
// --- // --- // --- // --- // --- // --- // --- // --- // --- // --- // --- // --- //

void MapSetup::LoadFromTree_CountKludge(TChain * the_tree, int N_rebin_hists)
{
	cout << "Called MapSetup::LoadFromTree_CountKludge(...) with N_rebin_hists=" << N_rebin_hists << endl;
//	cout << "countkludge=" << countkludge << endl;
	
//	gROOT->ForceStyle();
	gStyle->SetOptStat(0);
	
	int parallel_color;
	int antiparallel_color;
	
	naive_EnergyT_p_hist  = CreateHist( string("Naive Upper Energy(+)"), string("Mapping_Ebeta"), int(kBlack), N_rebin_hists);
	naive_EnergyB_p_hist  = CreateHist( string("Naive Lower Energy(+)"), string("Mapping_Ebeta"), int(kBlack), N_rebin_hists);
	naive_EnergyT_m_hist = CreateHist( string("Naive Upper Energy(-)"), string("Mapping_Ebeta"), int(kBlack), N_rebin_hists);
	naive_EnergyB_m_hist = CreateHist( string("Naive Lower Energy(-)"), string("Mapping_Ebeta"), int(kBlack), N_rebin_hists);
	//
	measured_EnergyT_p_hist = CreateHist( string("Measured ScintT Energy(+)"), string("Mapping_Ebeta"), int(kBlack), N_rebin_hists);
	measured_EnergyB_p_hist = CreateHist( string("Measured ScintB Energy(+)"), string("Mapping_Ebeta"), int(kBlack), N_rebin_hists);
	measured_EnergyT_m_hist = CreateHist( string("Measured ScintT Energy(-)"), string("Mapping_Ebeta"), int(kBlack), N_rebin_hists);
	measured_EnergyB_m_hist = CreateHist( string("Measured ScintB Energy(-)"), string("Mapping_Ebeta"), int(kBlack), N_rebin_hists);
	//
	measured_EnergyT_p_bb1agree = CreateHist( string("Measured ScintT Energy(+) - BB1 Agreement"), string("Mapping_Ebeta"), int(kBlack), N_rebin_hists);
	measured_EnergyB_p_bb1agree = CreateHist( string("Measured ScintB Energy(+) - BB1 Agreement"), string("Mapping_Ebeta"), int(kBlack), N_rebin_hists);
	measured_EnergyT_m_bb1agree = CreateHist( string("Measured ScintT Energy(-) - BB1 Agreement"), string("Mapping_Ebeta"), int(kBlack), N_rebin_hists);
	measured_EnergyB_m_bb1agree = CreateHist( string("Measured ScintB Energy(-) - BB1 Agreement"), string("Mapping_Ebeta"), int(kBlack), N_rebin_hists);
	//
	measured_EnergyT_p_bb1_r155 = CreateHist( string("Measured ScintT Energy(+) - rBB1<=15.5"), string("Mapping_Ebeta"), int(kBlack), N_rebin_hists);
	measured_EnergyB_p_bb1_r155 = CreateHist( string("Measured ScintB Energy(+) - rBB1<=15.5"), string("Mapping_Ebeta"), int(kBlack), N_rebin_hists);
	measured_EnergyT_m_bb1_r155 = CreateHist( string("Measured ScintT Energy(-) - rBB1<=15.5"), string("Mapping_Ebeta"), int(kBlack), N_rebin_hists);
	measured_EnergyB_m_bb1_r155 = CreateHist( string("Measured ScintB Energy(-) - rBB1<=15.5"), string("Mapping_Ebeta"), int(kBlack), N_rebin_hists);
	//
	measured_EnergyT_p_bb1_r105 = CreateHist( string("Measured ScintT Energy(+) - rBB1<=10.5"), string("Mapping_Ebeta"), int(kBlack), N_rebin_hists);
	measured_EnergyB_p_bb1_r105 = CreateHist( string("Measured ScintB Energy(+) - rBB1<=10.5"), string("Mapping_Ebeta"), int(kBlack), N_rebin_hists);
	measured_EnergyT_m_bb1_r105 = CreateHist( string("Measured ScintT Energy(-) - rBB1<=10.5"), string("Mapping_Ebeta"), int(kBlack), N_rebin_hists);
	measured_EnergyB_m_bb1_r105 = CreateHist( string("Measured ScintB Energy(-) - rBB1<=10.5"), string("Mapping_Ebeta"), int(kBlack), N_rebin_hists);
	
	// bb1radiuscut, costheta_actual vs costheta_generated
	costheta_v_costheta_p = CreateHist2d( string("costheta v costheta (+) - BB1 Agreement"), string("costheta"), string("costheta") );
		costheta_v_costheta_p -> GetXaxis() -> SetTitle("Generated Cos(theta)");
		costheta_v_costheta_p -> GetYaxis() -> SetTitle("Observed Cos(theta)");
	costheta_v_costheta_m = CreateHist2d( string("costheta v costheta (-) - BB1 Agreement"), string("costheta"), string("costheta") );
		costheta_v_costheta_m -> GetXaxis() -> SetTitle("Generated Cos(theta)");
		costheta_v_costheta_m -> GetYaxis() -> SetTitle("Observed Cos(theta)");
	
//	kludge_costheta_actual_forobservedr155 = CreateHist( string("Generated Cos(theta)"), string("costheta"), int(kBlack), N_rebin_hists);
//	kludge_costheta_actual_forobservedr155 -> GetXaxis() -> SetTitle("Generated Cos(theta)");
	
	
	// --- // --- // --- // --- // --- // --- // --- // --- // --- // --- // --- // --- //
	double naive_hit_t;
	double naive_hit_b;
	the_tree -> SetBranchAddress("naive_hit_t", &naive_hit_t);
	the_tree -> SetBranchAddress("naive_hit_b", &naive_hit_b);
	double gen_Tbeta;
	the_tree -> SetBranchAddress("gen_Tbeta", &gen_Tbeta);
	
	double gen_xhit_t;
	double gen_yhit_t;
	double gen_xhit_b;
	double gen_yhit_b;
	the_tree -> SetBranchAddress("gen_xhit_t", &gen_xhit_t);
	the_tree -> SetBranchAddress("gen_yhit_t", &gen_yhit_t);
	the_tree -> SetBranchAddress("gen_xhit_b", &gen_xhit_b);
	the_tree -> SetBranchAddress("gen_yhit_b", &gen_yhit_b);
	// in the future, this will come pre-pixellated.  but for now, we'll pixellate it here.
	double naive_hit_r;
	double tmp_x, tmp_y;
	
	int TTLBit_SigmaPlus;
	the_tree -> SetBranchAddress("TTLBit_SigmaPlus", &TTLBit_SigmaPlus);
	
	Double_t ScintT;
	Double_t ScintB;
	the_tree -> SetBranchAddress("upper_scint_E", &ScintT);
	the_tree -> SetBranchAddress("lower_scint_E", &ScintB);
	
	// BB1s:  
	vector<double> * bb1_t_x = 0;
	vector<double> * bb1_t_y = 0;
	vector<double> * bb1_t_E = 0;
	vector<double> * bb1_t_r = 0;
	vector<double> * bb1_b_x = 0;
	vector<double> * bb1_b_y = 0;
	vector<double> * bb1_b_E = 0;
	vector<double> * bb1_b_r = 0;
	
	
	// set branch addresses whether we're using a cut or not.
	the_tree -> SetBranchAddress("bb1_top_x", &bb1_t_x);
	the_tree -> SetBranchAddress("bb1_top_y", &bb1_t_y);
	the_tree -> SetBranchAddress("bb1_top_E", &bb1_t_E);
	the_tree -> SetBranchAddress("bb1_top_r", &bb1_t_r);
	the_tree -> SetBranchAddress("bb1_bottom_x", &bb1_b_x);
	the_tree -> SetBranchAddress("bb1_bottom_y", &bb1_b_y);
	the_tree -> SetBranchAddress("bb1_bottom_E", &bb1_b_E);
	the_tree -> SetBranchAddress("bb1_bottom_r", &bb1_b_r);
	
	double gen_t_r;
	double gen_b_r;
	the_tree -> SetBranchAddress("gen_rhit_t", &gen_t_r);
	the_tree -> SetBranchAddress("gen_rhit_b", &gen_b_r);
	double gen_costheta;
	the_tree -> SetBranchAddress("gen_costheta", &gen_costheta);
	double zhit=103.627357;
	
	int n_hits_t = 0;
	int n_hits_b = 0;
	
	int nentries = the_tree->GetEntries();
	cout << "nentries = " << nentries << endl;
	for(int i=0; i<nentries; i++)
	{
		the_tree -> GetEntry(i);
		if( (i % 100000) == 0) { cout << "Reached entry "<< i << endl; }
		
		n_hits_t = bb1_t_r->size();
		n_hits_b = bb1_b_r->size();
		
		// * // // // * // // // * // // // * // 
		if(TTLBit_SigmaPlus==1)
		{
			// naive hits:  
			if(naive_hit_t==1)
			{
				naive_EnergyT_p_hist -> Fill(gen_Tbeta);
			}
		//	if(naive_hit_b==1)
		//	{
		//		naive_EnergyB_p_hist -> Fill(gen_Tbeta);
		//	}
			
			// measured hits:
			if(ScintT>0) // has a scint hit.
			{ 
				measured_EnergyT_p_hist -> Fill(ScintT); 
				if(n_hits_t>0) // has a corresponding bb1 hit
				{
					measured_EnergyT_p_bb1agree -> Fill(ScintT);
					costheta_v_costheta_p -> Fill(gen_costheta, get_costheta(bb1_t_r->at(0), zhit));
					
					if( bb1_t_r->at(0) <= 15.5 ) // bb1 radius <= 15.5 mm
					{
						measured_EnergyT_p_bb1_r155 -> Fill(ScintT);
						if( bb1_t_r->at(0) <= 10.5 ) // bb1 radius <= 10.5 mm
						{
							measured_EnergyT_p_bb1_r105 -> Fill(ScintT);
						}
					}
				}
			}
			//
		//	if(ScintB>0) 
		//	{
		//		measured_EnergyB_p_hist -> Fill(ScintB); 
		//		if(n_hits_b>0)
		//		{
		//			measured_EnergyB_p_bb1agree -> Fill(ScintB); 
		//			costheta_v_costheta_p -> Fill(gen_costheta, get_costheta(bb1_b_r->at(0), -1.0*zhit));
		//			
		//			if( bb1_b_r->at(0) <= 15.5 ) // bb1 radius <= 15.5 mm
		//			{
		//				measured_EnergyB_p_bb1_r155 -> Fill(ScintB);
		//				if( bb1_b_r->at(0) <= 10.5 ) // bb1 radius <= 10.5 mm
		//				{
		//					measured_EnergyB_p_bb1_r105 -> Fill(ScintB);
		//				}
		//			}
		//		}
		//	}
			
		}
		// * // // // * // // // * // // // * // 
		else if(TTLBit_SigmaPlus==0)
		{
			// naive hits:
		//	if(naive_hit_t==1)
		//	{
		//		naive_EnergyT_m_hist -> Fill(gen_Tbeta);
		//	}
			if(naive_hit_b==1)
			{
				naive_EnergyB_m_hist -> Fill(gen_Tbeta);
			}
			// measured hits:
		//	if(ScintT>0) 
		//	{ 
		//		measured_EnergyT_m_hist -> Fill(ScintT); 
		//		if(n_hits_t>0) 
		//		{
		//			measured_EnergyT_m_bb1agree -> Fill(ScintT);
		//			costheta_v_costheta_m -> Fill(gen_costheta, get_costheta(bb1_t_r->at(0), zhit));
		//			
		//			if( bb1_t_r->at(0) <= 15.5 ) // bb1 radius <= 15.5 mm
		//			{
		//				measured_EnergyT_m_bb1_r155 -> Fill(ScintT);
		//				if( bb1_t_r->at(0) <= 10.5 ) // bb1 radius <= 10.5 mm
		//				{
		//					measured_EnergyT_m_bb1_r105 -> Fill(ScintT);
		//				}
		//			}
		//		}
		//	}
			if(ScintB>0) 
			{ 
				measured_EnergyB_m_hist -> Fill(ScintB); 
				if(n_hits_b>0)
				{
					measured_EnergyB_m_bb1agree -> Fill(ScintB); 
					costheta_v_costheta_m -> Fill(gen_costheta, get_costheta(bb1_b_r->at(0), -1.0*zhit));
					
					if( bb1_b_r->at(0) <= 15.5 )
					{
						measured_EnergyB_m_bb1_r155 -> Fill(ScintB);
						if( bb1_b_r->at(0) <= 10.5 ) 
						{
							measured_EnergyB_m_bb1_r105 -> Fill(ScintB);
						}
					}
				}
			}
		}
		else
		{
			cout << "It's broken." << endl;
			assert(0);
			return;
		}
	}
	int n_countsT = naive_EnergyT_p_hist->Integral();
	int n_countsB = naive_EnergyB_m_hist->Integral();
	int the_counterT = 0;
	int the_counterB = 0;
	bool keepgoing = true;
	
	cout << "Counts in \'less\' histograms:  T+:  " << naive_EnergyT_p_hist->Integral();
	cout << ";\tB-:  " << naive_EnergyB_m_hist->Integral() << endl;
	
//	cout << "Use total events:  " << n_counts << endl;
//	cout << "Second loop..." << endl;
	for(int i=0; i<nentries; i++)
	{
		the_tree -> GetEntry(i);
		if( (i % 100000) == 0) { cout << "Reached entry "<< i << endl; }
	//	if(!keepgoing) { continue; }  // I think this exits out of the whole for-loop..
		
		n_hits_t = bb1_t_r->size();
		n_hits_b = bb1_b_r->size();
		
		// * // // // * // // // * // // // * // 
		if(TTLBit_SigmaPlus==1)
		{
			if(the_counterB >= n_countsB ) { continue; }
			// naive hits:  
		//	if(naive_hit_t==1)
		//	{
		//		naive_EnergyT_p_hist -> Fill(gen_Tbeta);
		//	}
			if(naive_hit_b==1)
			{
				naive_EnergyB_p_hist -> Fill(gen_Tbeta);
				the_counterB++;
			}
			
			// measured hits:
		//	if(ScintT>0) // has a scint hit.
		//	{ 
		//		measured_EnergyT_p_hist -> Fill(ScintT); 
		//		if(n_hits_t>0) // has a corresponding bb1 hit
		//		{
		//			measured_EnergyT_p_bb1agree -> Fill(ScintT);
		//			costheta_v_costheta_p -> Fill(gen_costheta, get_costheta(bb1_t_r->at(0), zhit));
		//			
		//			if( bb1_t_r->at(0) <= 15.5 ) // bb1 radius <= 15.5 mm
		//			{
		//				measured_EnergyT_p_bb1_r155 -> Fill(ScintT);
		//				if( bb1_t_r->at(0) <= 10.5 ) // bb1 radius <= 10.5 mm
		//				{
		//					measured_EnergyT_p_bb1_r105 -> Fill(ScintT);
		//				}
		//			}
		//		}
		//	}
			//
			if(ScintB>0) 
			{
				measured_EnergyB_p_hist -> Fill(ScintB); 
				if(n_hits_b>0)
				{
					measured_EnergyB_p_bb1agree -> Fill(ScintB); 
					costheta_v_costheta_p -> Fill(gen_costheta, get_costheta(bb1_b_r->at(0), -1.0*zhit));
					
					if( bb1_b_r->at(0) <= 15.5 ) // bb1 radius <= 15.5 mm
					{
						measured_EnergyB_p_bb1_r155 -> Fill(ScintB);
						if( bb1_b_r->at(0) <= 10.5 ) // bb1 radius <= 10.5 mm
						{
							measured_EnergyB_p_bb1_r105 -> Fill(ScintB);
						}
					}
				}
			}
			
		}
		// * // // // * // // // * // // // * // 
		else if(TTLBit_SigmaPlus==0)
		{
			if(the_counterT >= n_countsT ) { continue; }
			// naive hits:
			if(naive_hit_t==1)
			{
				naive_EnergyT_m_hist -> Fill(gen_Tbeta);
				the_counterT++;
			}
		//	if(naive_hit_b==1)
		//	{
		//		naive_EnergyB_m_hist -> Fill(gen_Tbeta);
		//	}
			
			// measured hits:
			if(ScintT>0) 
			{ 
				measured_EnergyT_m_hist -> Fill(ScintT); 
				if(n_hits_t>0) 
				{
					measured_EnergyT_m_bb1agree -> Fill(ScintT);
					costheta_v_costheta_m -> Fill(gen_costheta, get_costheta(bb1_t_r->at(0), zhit));
					
					if( bb1_t_r->at(0) <= 15.5 ) // bb1 radius <= 15.5 mm
					{
						measured_EnergyT_m_bb1_r155 -> Fill(ScintT);
						if( bb1_t_r->at(0) <= 10.5 ) // bb1 radius <= 10.5 mm
						{
							measured_EnergyT_m_bb1_r105 -> Fill(ScintT);
						}
					}
				}
			}
		//	if(ScintB>0) 
		//	{ 
		//		measured_EnergyB_m_hist -> Fill(ScintB); 
		//		if(n_hits_b>0)
		//		{
		//			measured_EnergyB_m_bb1agree -> Fill(ScintB); 
		//			costheta_v_costheta_m -> Fill(gen_costheta, get_costheta(bb1_b_r->at(0), -1.0*zhit));
		//			
		//			if( bb1_b_r->at(0) <= 15.5 )
		//			{
		//				measured_EnergyB_m_bb1_r155 -> Fill(ScintB);
		//				if( bb1_b_r->at(0) <= 10.5 ) 
		//				{
		//					measured_EnergyB_m_bb1_r105 -> Fill(ScintB);
		//				}
		//			}
		//		}
		//	}
			
		}
		else
		{
			cout << "It's broken." << endl;
			assert(0);
			return;
		}
		
	//	if(the_counter >= n_counts) 
	//	{
	//		keepgoing = false;
	//		cout << "We've maxxed out our counts at i=" << i << " / " << nentries << " entries." << endl;
	//		return;
	//	}
		if( the_counterT >= n_countsT && the_counterB >= n_countsB )
		{
			cout << "We've maxxed out our counts at i=" << i << " / " << nentries << " entries." << endl;
			return;
		}
	}
	//
	return;
}
