#include "TF1.h"
#include "TMath.h"
#include <TFitResult.h>
#include <TFitResultPtr.h>
#include <TROOT.h>

//#include "MapUtility.cpp"
//#include "MakeSomeTF1s.cpp"

#include "FitUtility.h"

#include <string>
#include <iostream>  // cout
using std::string;
using std::cout;
using std::endl;


// --- // --- // --- // --- // --- // --- // --- // --- // --- // --- // --- // --- //

// --- // --- // --- // --- // --- // --- // --- // --- // --- // --- // --- // --- //
// --- // --- // --- // --- // --- // --- // --- // --- // --- // --- // --- // --- //


// --- // --- // --- // --- // --- // --- // --- // --- // --- // --- // --- // --- //
void SetParamsLike_BB1Agree_highenergy(TF1 &R)
{
//	bool use_truelandau = false;
	
	// call as:  SetParamsLike_BB1Agree_highenergy(*R);
	// R.SetParameter("norm",  1.0);
	
	// --- // --- //
	R.SetParameter("00_E0",    4.19232e+03);  // just set it to a value.  I'll pre-set it according to monoenergy later.
	R.SetParLimits(R.GetParNumber("00_E0"),     0.0,   5.5e3);  // the broadest of possible physical limits.  we'll give it better limits later.

	R.SetParameter("01_norm",  1.0e6);
	R.SetParLimits(R.GetParNumber("01_norm"),   0.0,   1e7);  // norm can't go negative.
	
	R.SetParameter("02_scale", 1.5);
	R.SetParLimits(R.GetParNumber("02_scale"), 0, 5);  // scale can't go negative either.
		
	R.SetParameter("03_alpha", 300.0);
	R.SetParLimits(R.GetParNumber("03_alpha"),  100,   1200.0);
	
	R.SetParameter("04_beta", 0.0);
	R.SetParLimits(R.GetParNumber("04_beta"), 0.0,   5);
//	R.FixParameter(R.GetParNumber("04_beta"), 0.0);

	R.SetParameter("05_gamma", 0.00100);
	R.SetParLimits(R.GetParNumber("05_gamma"),  0.00090,  0.00120);
	
	R.SetParameter("06_delta", 7.06801e-02);
//	R.SetParLimits(R.GetParNumber("06_delta"),  0.0,  0.03); 

	R.SetParameter("07_W",     380.0);
	R.SetParLimits(R.GetParNumber("07_W"),      200.0, 450.0);

	// 08_k
	R.SetParameter("08_k", 4.3);
	R.SetParLimits(R.GetParNumber("08_k"), 3.5,   12);  // 
//	R.FixParameter(R.GetParNumber("08_k"), 0.003);
//	R.SetParameter("08_k", 6);
//	R.SetParLimits(R.GetParNumber("08_k"), 3.5,   12);  // 
	
	// 09_gfrac
	R.SetParameter("09_gfrac", 0);
	R.SetParLimits(R.GetParNumber("09_gfrac"), 0,   1.0);

	R.SetParameter("10_toeres", 1.0);  // set it according to top and bottom detector later.  But keep it fixed initially.
	R.SetParLimits(R.GetParNumber("10_toeres"),      0.5,   13.0);
//	R.FixParameter(R.GetParNumber("10_toeres"),   1.0);
	
	R.SetParameter("11_lres", 0.006);  // as a fraction of toeres
	R.SetParLimits(R.GetParNumber("11_lres"), 1.0e-4,   0.02);  //
	
	// 12_gres
	R.SetParameter("12_gres", 1.0);
	R.SetParLimits(R.GetParNumber("12_gres"), 1.0e-4,   8.0);
//	R.FixParameter(R.GetParNumber("12_gres"), 1.0);  // ... as a fraction of toeres
	
	R.SetParameter("13_DgE", 0.0);
//	R.SetParLimits(R.GetParNumber("13_DgE"),    -2.0,  3.0);
	R.SetParLimits(R.GetParNumber("13_DgE"),    -10.0,  15.0);
	
	R.SetParameter("14_dE0",      0.0);  
//	R.SetParLimits(R.GetParNumber("14_dE0"),  -1050.0,  1350.0);
	R.SetParLimits(R.GetParNumber("14_dE0"),  -150.0,  50.0);
//	R.SetParLimits(R.GetParNumber("14_dE0"),   0.0,  350.0);


	//
	//
	return;
}

void SetParam_W(TF1 &R_tp, TF1 &R_tm, TF1 &R_bp, TF1 &R_bm, int mono_int)
{
	TF1 * f_W = new TF1("f_W", "[A]*(1-exp(-(x-[x0])/[tau])) + [m]*x + [b]", 0, 5500);
	// round 1 W fitting:
//	f_W -> SetParameter("tau",       590.606);
//	f_W -> SetParameter("const",     142.134);
//	f_W -> SetParameter("m",         0.000758319);
//	f_W -> SetParameter("x_offset",  464.723);
//	f_W -> SetParameter("y_offset",  249.255);
	
	// round 2 W fitting:  (probably still too small at low E)
//	f_W -> SetParameter("A",     2.43388e+01);
//	f_W -> SetParameter("b",     3.41575e+02);
//	f_W -> SetParameter("m",     4.79062e-03);
//	f_W -> SetParameter("tau",   2.99893e+02);
//	f_W -> SetParameter("x0",    1.00000e+03);
	
	// round 3 fitting:
//	f_W -> SetParameter("A",     3.61785e+01);
//	f_W -> SetParameter("b",     3.49689e+02);
//	f_W -> SetParameter("m",     1.64815e-03);
//	f_W -> SetParameter("tau",   5.89839e+02);
//	f_W -> SetParameter("x0",    1.00000e+03);
	
	// round 4:
//	f_W -> SetParameter("A",     3.22836e+01);
//	f_W -> SetParameter("b",     3.44911e+02);
//	f_W -> SetParameter("m",     3.06040e-03);
//	f_W -> SetParameter("tau",   3.72862e+02);
//	f_W -> SetParameter("x0",    1.00000e+03);
	
	// round 5:
//	f_W -> SetParameter("A",     2.97282e+01);
//	f_W -> SetParameter("b",     3.46700e+02);
//	f_W -> SetParameter("m",     4.44000e-03);
//	f_W -> SetParameter("tau",   4.76979e+02);
//	f_W -> SetParameter("x0",    1.00000e+03);
	
	// r6:
//	f_W -> SetParameter("A",     2.93000e+01);
//	f_W -> SetParameter("b",     3.41975e+02);
//	f_W -> SetParameter("m",     6.59984e-03);
//	f_W -> SetParameter("tau",   4.76979e+02);
//	f_W -> SetParameter("x0",    1.00000e+03);
	
	// r7:
	f_W -> SetParameter("A",     2.58610e+01);
	f_W -> SetParameter("b",     3.45363e+02);
	f_W -> SetParameter("m",     5.18086e-03);
	f_W -> SetParameter("tau",   4.42501e+02);
	f_W -> SetParameter("x0",    1.00000e+03);
	
	
	double thenew_W = f_W->Eval( (double)mono_int );
	
	R_tp.SetParameter("07_W", thenew_W);
	R_tm.SetParameter("07_W", thenew_W);
	R_bp.SetParameter("07_W", thenew_W);
	R_bm.SetParameter("07_W", thenew_W);
	
	return;
}

void SetParam_gamma(TF1 &R_tp, TF1 &R_tm, TF1 &R_bp, TF1 &R_bm, int mono_int)
{
	TF1 * f_gamma = new TF1("f_gamma", "[A]*(1-exp(-(x-[x0])/[tau])) + [m]*x + [b] + [ag]*exp(-((x-[xg])/[sg])^2)", 0, 5500);

	TF1 * fgamma_more = (TF1*)f_gamma->Clone();
	TF1 * fgamma_less = (TF1*)f_gamma->Clone();
	
	// round 1:
//	TF1 * f_gamma = new TF1("f_gamma", "[A]*(1-exp(-(x+[Estart])/[tau])) + [b]", 0, 5500);
//	fgamma_more->SetParameter("A",       0.318575);
//	fgamma_more->SetParameter("Estart",  0);
//	fgamma_more->SetParameter("b",       0.116292);
//	fgamma_more->SetParameter("tau",     713.337);
//	fgamma_less->SetParameter("A",       0.290501);
//	fgamma_less->SetParameter("Estart",  0);
//	fgamma_less->SetParameter("b",       0.110630);
//	fgamma_less->SetParameter("tau",     689.100);
	
	// round 2:
//	TF1 * f_gamma = new TF1("f_gamma", "[A]*(1-exp(-(x-[x0])/[tau])) + [m]*x + [b]", 0, 5500);
//	fgamma_less->SetParameter("A",       1.16285e-01);
//	fgamma_less->SetParameter("b",       2.81651e-01);
//	fgamma_less->SetParameter("m",      -3.99370e-06);
//	fgamma_less->SetParameter("tau",     7.18289e+02);
//	fgamma_less->SetParameter("x0",      500);
//	fgamma_more->SetParameter("A",       0.155827);
//	fgamma_more->SetParameter("b",       0.271980);
//	fgamma_more->SetParameter("m",      -3.99370e-06);
//	fgamma_more->SetParameter("tau",     7.18289e+02);
//	fgamma_more->SetParameter("x0",      500);
	
	// round 3:
//	fgamma_less->SetParameter("A",       8.81298e-02);
//	fgamma_less->SetParameter("b",       3.63239e-01);
//	fgamma_less->SetParameter("m",      -1.15165e-05);
//	fgamma_less->SetParameter("tau",     2.13567e+03);
//	fgamma_less->SetParameter("x0",      1000);
//	fgamma_more->SetParameter("A",       1.61247e-01);
//	fgamma_more->SetParameter("b",       3.15279e-01);
//	fgamma_more->SetParameter("m",      -1.15165e-05);
//	fgamma_more->SetParameter("tau",     1.76847e+03);
//	fgamma_more->SetParameter("x0",      10);  // welp.  that was a weird typo in the fit.  whatever though, that's the answer now.

	// round 4:
	fgamma_less->SetParameter("A",       6.40915e-05);
	fgamma_less->SetParameter("ag",      6.32792e-05);
	fgamma_less->SetParameter("b",       1.02186e-03);
	fgamma_less->SetParameter("m",      -2.83347e-08);
	fgamma_less->SetParameter("sg",      3.12371e+02);
	fgamma_less->SetParameter("tau",     1.24932e+03);
	fgamma_less->SetParameter("x0",      1000);
	fgamma_less->SetParameter("xg",      740);

	fgamma_more->SetParameter("A",       1.21407e-04);
	fgamma_more->SetParameter("ag",      3.91971e-05);
	fgamma_more->SetParameter("b",       1.08609e-03);
	fgamma_more->SetParameter("m",      -3.62101e-08);
	fgamma_more->SetParameter("sg",      4.76996e+02);
	fgamma_more->SetParameter("tau",     1.37207e+03);
	fgamma_more->SetParameter("x0",      1000);
	fgamma_more->SetParameter("xg",      740);

	
	double thenew_gamma_more = fgamma_more->Eval( (double)mono_int);
	double thenew_gamma_less = fgamma_less->Eval( (double)mono_int);
	
	R_tp.SetParameter("05_gamma", thenew_gamma_less);
	R_tm.SetParameter("05_gamma", thenew_gamma_more);
	R_bp.SetParameter("05_gamma", thenew_gamma_more);
	R_bm.SetParameter("05_gamma", thenew_gamma_less);
}

void SetParam_delta(TF1 &R_tp, TF1 &R_tm, TF1 &R_bp, TF1 &R_bm, int mono_int)
{

	TF1 * f_delta = new TF1("f_delta", "[A]*(x-[h])^2 + [k]", 0, 5500);
	TF1 * fdelta_more = (TF1*)f_delta->Clone();
	TF1 * fdelta_less = (TF1*)f_delta->Clone();
	
	fdelta_less->SetParameter("A",      -2.06669e-09);
	fdelta_less->SetParameter("h",       3.99604e+03);
	fdelta_less->SetParameter("k",       2.33331e-02);
	
	fdelta_more->SetParameter("A",      -2.42582e-09);
	fdelta_more->SetParameter("h",       3.99604e+03);
	fdelta_more->SetParameter("k",       2.81498e-02);
	
	/*
	TF1 * f_delta = new TF1("f_delta", "[A]*sin([f]*(x-[x0])) + [m]*x + [b]", 0, 5500);
	TF1 * fdelta_more = (TF1*)f_delta->Clone();
	TF1 * fdelta_less = (TF1*)f_delta->Clone();
	
//	fdelta_more->SetParameter("A",       6.43840e-3);
//	fdelta_more->SetParameter("b",       7.01815e-3);
//	fdelta_more->SetParameter("f",       9.05566e-4);
//	fdelta_more->SetParameter("m",       3.86549e-6);
//	fdelta_more->SetParameter("x0",      1.62599e3);
	fdelta_more->SetParameter("A",       8.31202e-03);
	fdelta_more->SetParameter("b",       1.13601e-03);
	fdelta_more->SetParameter("f",       8.53482e-04);
	fdelta_more->SetParameter("m",       5.57231e-06);
	fdelta_more->SetParameter("x0",      1.22387e+03);

//	fdelta_less->SetParameter("A",       0.00522072);
//	fdelta_less->SetParameter("b",       0.00426871);
//	fdelta_less->SetParameter("f",       9.05566e-4); // same
//	fdelta_less->SetParameter("m",       3.67501e-6);
//	fdelta_less->SetParameter("x0",      1.62599e3);  // same
	fdelta_less->SetParameter("A",       6.17089e-03);
	fdelta_less->SetParameter("b",       4.11640e-04);
	fdelta_less->SetParameter("f",       8.53482e-04);  // same
	fdelta_less->SetParameter("m",       4.91935e-06);
	fdelta_less->SetParameter("x0",      1.22387e+03);  // same
	*/
	
	double thenew_delta_more = fdelta_more->Eval( (double)mono_int);
	double thenew_delta_less = fdelta_less->Eval( (double)mono_int);
	
	if(thenew_delta_more<0)
	{
		thenew_delta_more = 0.0;
	}
	if(thenew_delta_less<0)
	{
		thenew_delta_less = 0.0;
	}
	
	R_tp.SetParameter("06_delta", thenew_delta_less);
	R_tm.SetParameter("06_delta", thenew_delta_more);
	R_bp.SetParameter("06_delta", thenew_delta_more);
	R_bm.SetParameter("06_delta", thenew_delta_less);
}

void SetParam_E0(TF1 &R_tp, TF1 &R_tm, TF1 &R_bp, TF1 &R_bm, int mono_int)
{
	TF1 * f_E0 = new TF1("f_E0", "([Ae]*(1-exp(-(x-[x0])/[tau]))+[m]*x +[k])*0.5*(1+TMath::Erf((x-[x0e])/[sigma] )) + [k] +[zz]", 0, 5500);
	
	// round 4:
	f_E0->SetParameter("Ae",       6.54141e+01);
	f_E0->SetParameter("k",        7.64810e+01);
	f_E0->SetParameter("m",       -3.35824e-03);
	f_E0->SetParameter("sigma",    4.19311e+02);
	f_E0->SetParameter("tau",      1.05627e+03);
	f_E0->SetParameter("x0",       500.0);
	f_E0->SetParameter("x0e",      3.99554e+02);
	f_E0->SetParameter("zz",      -200.0);
	
	// round 3:
//	f_E0->SetParameter("Ae",       6.75448e+01);
//	f_E0->SetParameter("k",        7.64653e+01);
//	f_E0->SetParameter("m",       -5.63814e-03);
//	f_E0->SetParameter("sigma",    4.66233e+02);
//	f_E0->SetParameter("tau",      8.92567e+02);
//	f_E0->SetParameter("x0",       500.0);
//	f_E0->SetParameter("x0e",      3.99554e+02);
//	f_E0->SetParameter("zz",      -200.0);

	// round 2:
//	f_E0->SetParameter("Ae",       7.05862e+01);
//	f_E0->SetParameter("k",        7.74834e+01);
//	f_E0->SetParameter("m",       -5.18160e-03);
//	f_E0->SetParameter("sigma",    2.19276e+02);
//	f_E0->SetParameter("tau",      1.12881e+03);
//	f_E0->SetParameter("x0",       5.00000e+02);
//	f_E0->SetParameter("x0e",      5.77051e+02);
//	f_E0->SetParameter("zz",      -2.00000e+02);
	
//	old E0 values:
//	TF1 * f_E0_other = new TF1("f_E0", "[Ae]*(1-exp(-(x-[x0])/[tau])) + [k] + [m]*x", 0, 5500);
//	f_E0->SetParameter("Ae",       4.09334e1);
//	f_E0->SetParameter("k",       -2.47821e1);
//	f_E0->SetParameter("m",       -3.31120e-3);
//	f_E0->SetParameter("tau",      9.03542e2);
//	f_E0->SetParameter("x0",       1.0e3);
	
	double thenew_E0 = f_E0->Eval( (double)mono_int );
	if(mono_int-300.0 + thenew_E0 <= 50) 
	{
		//thenew_E0 = 1.0;
		thenew_E0 = -(mono_int-300.0) + 50.0;
	}
//	cout << "thenew_E0 = " << thenew_E0 << endl;
	
	R_tp.SetParameter("00_E0", mono_int-300.0 + thenew_E0);
	R_tm.SetParameter("00_E0", mono_int-300.0 + thenew_E0);
	R_bp.SetParameter("00_E0", mono_int-300.0 + thenew_E0);
	R_bm.SetParameter("00_E0", mono_int-300.0 + thenew_E0);
}

void SetParam_toeres(TF1 &R_tp, TF1 &R_tm, TF1 &R_bp, TF1 &R_bm, int mono_int)
{
	TF1 * f_toeres = new TF1("f_toeres", "[ap]*(x-[h])^2 + [k] + [Ah]*(1/(x-[Estart]))", 0, 5500);
	TF1 * f_toeres_top    = (TF1*)f_toeres->Clone();
	TF1 * f_toeres_bottom = (TF1*)f_toeres->Clone();
	
	
	// round 1:
//	f_toeres_top->SetParameter("Ah",      9.22931e3);
//	f_toeres_top->SetParameter("Estart", -5.81910e2);
//	f_toeres_top->SetParameter("ap",      4.75712e-10);
//	f_toeres_top->SetParameter("h",      -1.86633e5);
//	f_toeres_top->SetParameter("k",      -1.67935e1);
//	f_toeres_bottom->SetParameter("Ah",      7.24652e3);
//	f_toeres_bottom->SetParameter("Estart", -3.86435e2);
//	f_toeres_bottom->SetParameter("ap",      2.13367e-10);
//	f_toeres_bottom->SetParameter("h",      -2.84104e5);
//	f_toeres_bottom->SetParameter("k",      -1.69271e1);
	
	// round 3:
//	f_toeres_top->SetParameter("Ah",      3.49108e3);
//	f_toeres_top->SetParameter("Estart",  2.48678e2);
//	f_toeres_top->SetParameter("ap",      4.62123e-10);
//	f_toeres_top->SetParameter("h",      -2.69192e5);
//	f_toeres_top->SetParameter("k",      -3.32334e1);
//	f_toeres_bottom->SetParameter("Ah",      3.61317e3);
//	f_toeres_bottom->SetParameter("Estart",  2.21528e2);
//	f_toeres_bottom->SetParameter("ap",      4.38977e-10);
//	f_toeres_bottom->SetParameter("h",      -2.84075e5);
//	f_toeres_bottom->SetParameter("k",      -3.54241e1);
	
	// r4:
	f_toeres_top->SetParameter("Ah",      4.21612e3);
	f_toeres_top->SetParameter("Estart",  1.34274e2);
	f_toeres_top->SetParameter("ap",      5.80669e-10);
	f_toeres_top->SetParameter("h",      -2.63904e5);
	f_toeres_top->SetParameter("k",      -4.07267e1);

	f_toeres_bottom->SetParameter("Ah",      4.4104e3);
	f_toeres_bottom->SetParameter("Estart",  107.97);
	f_toeres_bottom->SetParameter("ap",      5.68775e-10);
	f_toeres_bottom->SetParameter("h",      -2.68232e5);
	f_toeres_bottom->SetParameter("k",      -4.13796e1);
	
	double thenew_toeres_top;//    = f_toeres_top   ->Eval( (double)mono_int);
	double thenew_toeres_bottom;// = f_toeres_bottom->Eval( (double)mono_int);
	if(mono_int>135.0)
	{
		thenew_toeres_top    = f_toeres_top   ->Eval( (double)mono_int);
		thenew_toeres_bottom = f_toeres_bottom->Eval( (double)mono_int);
	}
	else
	{
		thenew_toeres_top    = 80.0;
		thenew_toeres_bottom = 80.0;
	}
	
	R_tp.SetParameter("10_toeres", thenew_toeres_top);
	R_tm.SetParameter("10_toeres", thenew_toeres_top);
	R_bp.SetParameter("10_toeres", thenew_toeres_bottom);
	R_bm.SetParameter("10_toeres", thenew_toeres_bottom);
	
	return;
}
/*
void SetParam_toeres_again(TF1 &R_tp, TF1 &R_tm, TF1 &R_bp, TF1 &R_bm, int mono_int)
{
//	TF1 * f_toeres = new TF1("f_toeres", "[ap]*(x-[h])^2 + [k] + [Ah]*(1/(x-[Estart]))", 0, 5500);
	TF1 * f_toeres = new TF1("f_toeres", "[A]*exp(-(x-[x0])/[tau]) + [m]*x +[b]", 0, 5500);
	TF1 * f_toeres_top    = (TF1*)f_toeres->Clone();
	TF1 * f_toeres_bottom = (TF1*)f_toeres->Clone();
	
	
	f_toeres_top->SetParameter("A",       6.93491);
	f_toeres_top->SetParameter("b",       1.78095);
	f_toeres_top->SetParameter("m",       5.61579e-5);
	f_toeres_top->SetParameter("tau",     6.35777e2);
	f_toeres_top->SetParameter("x0",      500.0);
	
	f_toeres_bottom->SetParameter("A",       6.74596);
	f_toeres_bottom->SetParameter("b",       1.73445);
	f_toeres_bottom->SetParameter("m",       5.25607e-5);
	f_toeres_bottom->SetParameter("tau",     6.41521e2);
	f_toeres_bottom->SetParameter("x0",      500.0);

	double thenew_toeres_top    = f_toeres_top   ->Eval( (double)mono_int);
	double thenew_toeres_bottom = f_toeres_bottom->Eval( (double)mono_int);
	
	R_tp.SetParameter("10_toeres", thenew_toeres_top);
	R_tm.SetParameter("10_toeres", thenew_toeres_top);
	R_bp.SetParameter("10_toeres", thenew_toeres_bottom);
	R_bm.SetParameter("10_toeres", thenew_toeres_bottom);
	
	return;
}
*/
void SetParam_gres(TF1 &R_tp, TF1 &R_tm, TF1 &R_bp, TF1 &R_bm, int mono_int)
{
	TF1 * f_gres = new TF1("f_gres", "[A]*(1-exp(-(x-[x0])/[tau])) + [m]*x + [b]", 0, 5500);
	
//	r1:
//	f_gres -> SetParameter("A",     1.21928);
//	f_gres -> SetParameter("b",     6.46198e-01);
//	f_gres -> SetParameter("m",    -1.46231e-04);
//	f_gres -> SetParameter("tau",   6.36035e+02);
//	f_gres -> SetParameter("x0",    500.0);
	
	// r2:
//	f_gres -> SetParameter("A",     1.32631e+00);
//	f_gres -> SetParameter("b",     5.81004e-01);
//	f_gres -> SetParameter("m",    -1.62495e-04);
//	f_gres -> SetParameter("tau",   5.69861e+02);
//	f_gres -> SetParameter("x0",    500.0);

	// r3:
//	f_gres -> SetParameter("A",     1.01750e+00);
//	f_gres -> SetParameter("b",     8.22243e-01);
//	f_gres -> SetParameter("m",    -1.44041e-04);
//	f_gres -> SetParameter("tau",   7.09751e+02);
//	f_gres -> SetParameter("x0",    500.0);
	
	// r4:
	f_gres -> SetParameter("A",     1.44290e+00);
	f_gres -> SetParameter("b",     1.01601e-01);
	f_gres -> SetParameter("m",    -8.99046e-05);
	f_gres -> SetParameter("tau",   4.77278e+02);
	f_gres -> SetParameter("x0",    500.0);


	double thenew_gres = f_gres->Eval( (double)mono_int);
	if(thenew_gres<0.05)
	{
		thenew_gres = 0.05;
	}
	
	R_tp.SetParameter("12_gres", thenew_gres);
	R_tm.SetParameter("12_gres", thenew_gres);
	R_bp.SetParameter("12_gres", thenew_gres);
	R_bm.SetParameter("12_gres", thenew_gres);
}

void SetParam_DgE_tmp(TF1 &R_tp, TF1 &R_tm, TF1 &R_bp, TF1 &R_bm, int mono_int)
{
	TF1 * f_DgE = new TF1("f_DgE", "[b] + [m]*x", 0, 5500);
	// r1:
//	f_DgE -> SetParameter("b",     -1.30387);
//	f_DgE -> SetParameter("m",      0.000586707);
	
	// r2:
	f_DgE -> SetParameter("b",     -7.42784);
	f_DgE -> SetParameter("m",      0.00343322);
	
	double thenew_DgE = f_DgE->Eval( (double)mono_int);
	
	R_tp.SetParameter("13_DgE", thenew_DgE);
	R_tm.SetParameter("13_DgE", thenew_DgE);
	R_bp.SetParameter("13_DgE", thenew_DgE);
	R_bm.SetParameter("13_DgE", thenew_DgE);
}
void SetParam_dE0(TF1 &R_tp, TF1 &R_tm, TF1 &R_bp, TF1 &R_bm, int mono_int)
{
	TF1 * f_dE0 = new TF1("f_dE0", "[A]*(x-[h])^2 + [k]", 0, 5500);
	
	// r1:
//	TF1 * f_dE0 = new TF1("f_dE0", "([m]*x + [b])*0.5*(1+TMath::Erf( (x-[x0])/[sigma]))", 0, 5500);
//	f_dE0 -> SetParameter("b",     -3.22000e+01);
//	f_dE0 -> SetParameter("m",      1.30500e-02);
//	f_dE0 -> SetParameter("sigma",  2.95755e+02);
//	f_dE0 -> SetParameter("x0",     9.93347e+02);

	// r2:
	f_dE0->SetParameter("A",       1.02286e-06);
	f_dE0->SetParameter("h",      -4.19023e+03);
	f_dE0->SetParameter("k",      -4.72724e+01);

	
	double thenew_dE0= f_dE0->Eval( (double)mono_int);
	R_tp.SetParameter("14_dE0", thenew_dE0);
	R_tm.SetParameter("14_dE0", thenew_dE0);
	R_bp.SetParameter("14_dE0", thenew_dE0);
	R_bm.SetParameter("14_dE0", thenew_dE0);
}

void SetParam_lres(TF1 &R_tp, TF1 &R_tm, TF1 &R_bp, TF1 &R_bm, int mono_int)
{
	
	// r1:
//	TF1 * f_lres = new TF1("f_lres", "[A]*(exp(-(x-[x0])/[tau])) + [m]*x + [b]", 0, 5500);
//	f_lres -> SetParameter("A",     2.19305e-02);
//	f_lres -> SetParameter("b",     1.24883e-02);
//	f_lres -> SetParameter("m",     3.43142e-07);
//	f_lres -> SetParameter("tau",   6.66573e+02);
//	f_lres -> SetParameter("x0",    1000.0);

	// r2:
//	f_lres -> SetParameter("A",     2.18269e-02);
//	f_lres -> SetParameter("b",     1.32845e-02);
//	f_lres -> SetParameter("m",     1.86503e-07);
//	f_lres -> SetParameter("tau",   6.96056e+02);
//	f_lres -> SetParameter("x0",    1000.0);
	
	TF1 * f_lres = new TF1("f_lres", "[A]*(exp(-(x-[x0])/[tau])) + [m]*x + [b] + [ag]*exp(-((x-[xg])/[sg])^2)", 0, 5500);
	
	// r3:
	f_lres -> SetParameter("A",     3.16015e-02);
	f_lres -> SetParameter("ag",    6.73200e-02);
	f_lres -> SetParameter("b",     1.92218e-02);
	f_lres -> SetParameter("m",    -9.74354e-07);
	f_lres -> SetParameter("sg",    1.63493e+02);
	f_lres -> SetParameter("tau",   4.77801e+02);
	f_lres -> SetParameter("x0",    1000.0);
	f_lres -> SetParameter("xg",    7.91517e+02);

	double thenew_lres= f_lres->Eval( (double)mono_int);
	R_tp.SetParameter("11_lres", thenew_lres);
	R_tm.SetParameter("11_lres", thenew_lres);
	R_bp.SetParameter("11_lres", thenew_lres);
	R_bm.SetParameter("11_lres", thenew_lres);
}

void SetParam_gfrac(TF1 &R_tp, TF1 &R_tm, TF1 &R_bp, TF1 &R_bm, int mono_int)
{
	TF1 * f_gfrac = new TF1("f_gfrac", "[A]*(1-TMath::Erf( (x-[x0])/[sigma])) + [m]*x + [b] + [ag]*exp(-((x-[xg])/[sg])^2)", 0, 5500);
	
	// r3:
	f_gfrac -> SetParameter("A",     5.04000e-02);
	f_gfrac -> SetParameter("ag",   -1.88990e-02);
	f_gfrac -> SetParameter("b",     9.00319e-01);
	f_gfrac -> SetParameter("m",    -2.95756e-06);
	f_gfrac -> SetParameter("sg",    1.55113e+03);
	f_gfrac -> SetParameter("sigma", 3.08638e+02);
	f_gfrac -> SetParameter("x0",    9.58109e+02);
	f_gfrac -> SetParameter("xg",    2.24138e+03);
	
	double thenew_gfrac = f_gfrac->Eval( (double)mono_int);
	
	R_tp.SetParameter("09_gfrac", thenew_gfrac);
	R_tm.SetParameter("09_gfrac", thenew_gfrac);
	R_bp.SetParameter("09_gfrac", thenew_gfrac);
	R_bm.SetParameter("09_gfrac", thenew_gfrac);
	
//	TF1 * f_gfrac = new TF1("f_gfrac", "[m]*x+[b]", 0, 5500);
//	fgfrac_more -> SetParameter("b",     8.64677e-01);
//	fgfrac_more -> SetParameter("m",     3.19555e-06);
//	fgfrac_less -> SetParameter("b",     8.61448e-01);
//	fgfrac_less -> SetParameter("m",     3.21202e-06);
	
	// r2:
//	TF1 * f_gfrac = new TF1("f_gfrac", "[A]*(exp(-(x-[x0])/[tau])) + [m]*x + [b]", 0, 5500);
//	TF1 * fgfrac_more = (TF1*)f_gfrac->Clone();
//	TF1 * fgfrac_less = (TF1*)f_gfrac->Clone();
//	// r2:
//	fgfrac_more -> SetParameter("A",     2.95685e-01);
//	fgfrac_more -> SetParameter("b",     8.66996e-01);
//	fgfrac_more -> SetParameter("m",     2.65021e-06);
//	fgfrac_more -> SetParameter("tau",   2.74812e+02);
//	fgfrac_more -> SetParameter("x0",    500.0);
//	
//	fgfrac_less -> SetParameter("A",     2.82439e-01);
//	fgfrac_less -> SetParameter("b",     8.62303e-01);
//	fgfrac_less -> SetParameter("m",     3.08521e-06);
//	fgfrac_less -> SetParameter("tau",   3.08454e+02);
//	fgfrac_less -> SetParameter("x0",    500.0);
//	
//	double thenew_gfrac_more = fgfrac_more->Eval( (double)mono_int);
//	double thenew_gfrac_less = fgfrac_less->Eval( (double)mono_int);
//	
//	R_tp.SetParameter("09_gfrac", thenew_gfrac_less);
//	R_tm.SetParameter("09_gfrac", thenew_gfrac_more);
//	R_bp.SetParameter("09_gfrac", thenew_gfrac_more);
//	R_bm.SetParameter("09_gfrac", thenew_gfrac_less);
}

void SetParam_alpha_tmp(TF1 &R_tp, TF1 &R_tm, TF1 &R_bp, TF1 &R_bm, int mono_int) // WARNING!  This will probably need 4 paramsets.  obsolete now.
{
	TF1 * f_alpha = new TF1("f_alpha", "[b1]+ TMath::Erf((x-[xstart])/[sigma])*([A]*(exp(-(x-[x0])/[tau])) + [m]*x + [b])", 0, 5500);
	TF1 * falpha_more = (TF1*)f_alpha->Clone();
	TF1 * falpha_less = (TF1*)f_alpha->Clone();
	
	// r1:
//	TF1 * f_alpha = new TF1("f_alpha", "[A]*(exp(-(x-[x0])/[tau])) + [m]*x + [b]", 0, 5500);
//	falpha_more -> SetParameter("A",     3.74617e-01);
//	falpha_more -> SetParameter("b",     5.14064e-02);
//	falpha_more -> SetParameter("m",     1.80311e-05);
//	falpha_more -> SetParameter("tau",   6.06154e+02);
//	falpha_more -> SetParameter("x0",    1000.0);
//	falpha_less -> SetParameter("A",     3.05406e-01);
//	falpha_less -> SetParameter("b",     1.44823e-01);
//	falpha_less -> SetParameter("m",     1.23756e-05);
//	falpha_less -> SetParameter("tau",   5.76048e+02);
//	falpha_less -> SetParameter("x0",    1000.0);
	
	// r2:
	falpha_more -> SetParameter("A",      3.42833e-1);
	falpha_more -> SetParameter("b",     -6.04697e-1);
	falpha_more -> SetParameter("b1",     6.58196e-1);
	falpha_more -> SetParameter("m",      1.69317e-5);
	falpha_more -> SetParameter("sigma",  2.66e2);
	falpha_more -> SetParameter("tau",    6.34e2);
	falpha_more -> SetParameter("x0",     1000.0);
	falpha_more -> SetParameter("xstart", 7.35e2);
	
	falpha_less -> SetParameter("A",      2.8425e-1);
	falpha_less -> SetParameter("b",     -5.2277e-1);
	falpha_less -> SetParameter("b1",     6.53908e-1);
	falpha_less -> SetParameter("m",      1.48529e-5);
	falpha_less -> SetParameter("sigma",  2.66e2);
	falpha_less -> SetParameter("tau",    6.34e2);
	falpha_less -> SetParameter("x0",     1000.0);
	falpha_less -> SetParameter("xstart", 7.35e2);
	
	double thenew_alpha_more = falpha_more->Eval( (double)mono_int);
	double thenew_alpha_less = falpha_less->Eval( (double)mono_int);
	
	R_tp.SetParameter("03_alpha", thenew_alpha_less);
	R_tm.SetParameter("03_alpha", thenew_alpha_more);
	R_bp.SetParameter("03_alpha", thenew_alpha_more);
	R_bm.SetParameter("03_alpha", thenew_alpha_less);
}

void SetParam_alpha(TF1 &R_tp, TF1 &R_tm, TF1 &R_bp, TF1 &R_bm, int mono_int)
{
	TF1 * f_alpha = new TF1("f_alpha", "[A]*(exp((x-[x0])/[tau])) + [m]*x + [b]", 0, 5500);
	TF1 * falpha_more = (TF1*)f_alpha->Clone();
	TF1 * falpha_less = (TF1*)f_alpha->Clone();
		
	// r3:
//	falpha_more -> SetParameter("A",      1.07325e4);
//	falpha_more -> SetParameter("b",     -8.84534e3);
//	falpha_more -> SetParameter("m",     -8.07275e-1);
//	falpha_more -> SetParameter("tau",    1.28662e4);
//	falpha_more -> SetParameter("x0",     2000.0);
//	
//	falpha_less -> SetParameter("A",      2.31841e+04);
//	falpha_less -> SetParameter("b",     -2.06683e+04);
//	falpha_less -> SetParameter("m",     -1.06672e+00);
//	falpha_less -> SetParameter("tau",    1.97580e+04);
//	falpha_less -> SetParameter("x0",     2000.0);
	
	// r4:
	falpha_more -> SetParameter("A",      7.46589e+01);
	falpha_more -> SetParameter("b",      1.90261e+02);
	falpha_more -> SetParameter("m",      8.68462e-03);
	falpha_more -> SetParameter("tau",    1.72463e+03);
	falpha_more -> SetParameter("x0",     2000.0);
	
	falpha_less -> SetParameter("A",      7.78144e+03);
	falpha_less -> SetParameter("b",     -6.66835e+03);
	falpha_less -> SetParameter("m",     -3.63543e-01);
	falpha_less -> SetParameter("tau",    1.57100e+04);
	falpha_less -> SetParameter("x0",     2000.0);

	double thenew_alpha_more = falpha_more->Eval( (double)mono_int);
	double thenew_alpha_less = falpha_less->Eval( (double)mono_int);
	
	R_tp.SetParameter("03_alpha", thenew_alpha_less);
	R_tm.SetParameter("03_alpha", thenew_alpha_more);
	R_bp.SetParameter("03_alpha", thenew_alpha_more);
	R_bm.SetParameter("03_alpha", thenew_alpha_less);
}

void SetParam_beta(TF1 &R_tp, TF1 &R_tm, TF1 &R_bp, TF1 &R_bm, int mono_int)
{
	TF1 * f_beta = new TF1("f_beta", "(1+TMath::Erf((x-[xstart])/[sigma]))*([A]*(exp(-(x-[x0])/[tau])) + [m]*x + [b]) + [b2]", 0, 5500);
	
	TF1 * f_beta_more = (TF1*)f_beta->Clone();
	TF1 * f_beta_less = (TF1*)f_beta->Clone();
	
	// r2:
//	fbeta_more -> SetParameter("A",      1.02507e+00);
//	fbeta_more -> SetParameter("b",      3.58848e-01);
//	fbeta_more -> SetParameter("b2",     0);
//	fbeta_more -> SetParameter("m",      7.18603e-05);
//	fbeta_more -> SetParameter("sigma",  5.18102e+02);
//	fbeta_more -> SetParameter("tau",    4.15078e+02);
//	fbeta_more -> SetParameter("x0",     1000.0);
//	fbeta_more -> SetParameter("xstart", 5.48476e+02);
//	
//	fbeta_less -> SetParameter("A",      9.82958e-01);
//	fbeta_less -> SetParameter("b",      1.46939e-01);
//	fbeta_less -> SetParameter("b2",     0);
//	fbeta_less -> SetParameter("m",      9.69763e-05);
//	fbeta_less -> SetParameter("sigma",  2.67812e+02);
//	fbeta_less -> SetParameter("tau",    5.52750e+02);
//	fbeta_less -> SetParameter("x0",     1000.0);
//	fbeta_less -> SetParameter("xstart", 6.22114e+02);
	
	// r3:
//	fbeta_more -> SetParameter("A",      9.29704e-1);
//	fbeta_more -> SetParameter("b",      4.42587e-1);
//	fbeta_more -> SetParameter("b2",     0);
//	fbeta_more -> SetParameter("m",      5.02198e-5);
//	fbeta_more -> SetParameter("sigma",  2.59383e+02);
//	fbeta_more -> SetParameter("tau",    5.49771e2);
//	fbeta_more -> SetParameter("x0",     1000.0);
//	fbeta_more -> SetParameter("xstart", 6.29177e+02);
//	fbeta_less -> SetParameter("A",      9.08194e-01);
//	fbeta_less -> SetParameter("b",      3.57069e-01);
//	fbeta_less -> SetParameter("b2",     0);
//	fbeta_less -> SetParameter("m",      3.62829e-05);
//	fbeta_less -> SetParameter("sigma",  2.59383e+02);
//	fbeta_less -> SetParameter("tau",    5.51160e+02);
//	fbeta_less -> SetParameter("x0",     1000.0);
//	fbeta_less -> SetParameter("xstart", 6.29177e+02);

	// r4:
//	f_beta_more -> SetParameter("A",      9.63861e-01);
//	f_beta_more -> SetParameter("b",     -9.44253e-01);
//	f_beta_more -> SetParameter("b2",     2.72785e+00);
//	f_beta_more -> SetParameter("m",      5.73989e-05);
//	f_beta_more -> SetParameter("sigma",  2.05042e+02);
//	f_beta_more -> SetParameter("tau",    4.96496e+02);
//	f_beta_more -> SetParameter("x0",     1000.0);
//	f_beta_more -> SetParameter("xstart", 9.67350e+02);
//	f_beta_less -> SetParameter("A",      9.43843e-01);
//	f_beta_less -> SetParameter("b",     -1.03172e+00);
//	f_beta_less -> SetParameter("b2",     2.6);
//	f_beta_less -> SetParameter("m",      5.92912e-05);
//	f_beta_less -> SetParameter("sigma",  1.69099e+02);
//	f_beta_less -> SetParameter("tau",    6.20547e+02);
//	f_beta_less -> SetParameter("x0",     1000.0);
//	f_beta_less -> SetParameter("xstart", 8.69300e+02);


	// r5:
	f_beta_more -> SetParameter("A",      8.71747e-01);
	f_beta_more -> SetParameter("b",     -1.21383e+00);
	f_beta_more -> SetParameter("b2",     2.86553e+00);
	f_beta_more -> SetParameter("m",      9.92999e-05);
	f_beta_more -> SetParameter("sigma",  2.66178e+02);
	f_beta_more -> SetParameter("tau",    8.05781e+02);
	f_beta_more -> SetParameter("x0",     1000.0);
	f_beta_more -> SetParameter("xstart", 1.08844e+03);

	f_beta_less -> SetParameter("A",      9.35885e-01);
	f_beta_less -> SetParameter("b",     -1.22493e+00);
	f_beta_less -> SetParameter("b2",     2.74850e+00);
	f_beta_less -> SetParameter("m",      9.22041e-05);
	f_beta_less -> SetParameter("sigma",  1.73977e+02);
	f_beta_less -> SetParameter("tau",    7.11168e+02);
	f_beta_less -> SetParameter("x0",     1000.0);
	f_beta_less -> SetParameter("xstart", 1.06046e+03);
	
	
	double thenew_beta_more = f_beta_more->Eval( (double)mono_int);
	double thenew_beta_less = f_beta_less->Eval( (double)mono_int);
	
	R_tp.SetParameter("04_beta", thenew_beta_less);
	R_tm.SetParameter("04_beta", thenew_beta_more);
	R_bp.SetParameter("04_beta", thenew_beta_more);
	R_bm.SetParameter("04_beta", thenew_beta_less);
}

void SetParam_scale(TF1 &R_tp, TF1 &R_tm, TF1 &R_bp, TF1 &R_bm, int mono_int)
{
	TF1 * f_scale = new TF1("f_scale", "[A]*0.5*(1+TMath::Erf((x-[x0])/[sigma]))*exp(-(x-[x1])/[tau])", 0, 5500);
	TF1 * f_scale_more = (TF1*)f_scale->Clone();
	TF1 * f_scale_less = (TF1*)f_scale->Clone();
	
	//
	f_scale_more -> SetParameter("A",       1.93239e+00);
	f_scale_more -> SetParameter("sigma",   1.61255e+02);
	f_scale_more -> SetParameter("tau",     3.43846e+02);
	f_scale_more -> SetParameter("x0",      5.81403e+02);
	f_scale_more -> SetParameter("x1",      1000.0);

	f_scale_less -> SetParameter("A",       5.87957e-01);
	f_scale_less -> SetParameter("sigma",   6.05826e+01);
	f_scale_less -> SetParameter("tau",     3.48971e+02);
	f_scale_less -> SetParameter("x0",      5.25404e+02);
	f_scale_less -> SetParameter("x1",      1000.0);

	double thenew_scale_more = f_scale_more->Eval( (double)mono_int);
	double thenew_scale_less = f_scale_less->Eval( (double)mono_int);
	
	R_tp.SetParameter("02_scale", thenew_scale_less);
	R_tm.SetParameter("02_scale", thenew_scale_more);
	R_bp.SetParameter("02_scale", thenew_scale_more);
	R_bm.SetParameter("02_scale", thenew_scale_less);
}

void SetParam_norm(TF1 &R_tp, TF1 &R_tm, TF1 &R_bp, TF1 &R_bm, int mono_int)
{
	TF1 * f_norm = new TF1("f_norm", "[A]*(1-exp(-(x-[x0])/[tau])) + [m]*x + [b]", 0, 5500);
	TF1 * f_norm_tp = (TF1*)f_norm->Clone();
	TF1 * f_norm_tm = (TF1*)f_norm->Clone();
	TF1 * f_norm_bp = (TF1*)f_norm->Clone();
	TF1 * f_norm_bm = (TF1*)f_norm->Clone();
	
	// ranked from 'top' to 'bottom':
	// bp:
	f_norm_bp -> SetParameter("A",      5.58064e+05);
	f_norm_bp -> SetParameter("b",      4.29724e+05);
	f_norm_bp -> SetParameter("m",     -1.06132e+01);
	f_norm_bp -> SetParameter("tau",    6.96335e+02);
	f_norm_bp -> SetParameter("x0",     1000.0);
	
	// tm:
	f_norm_tm -> SetParameter("A",      5.43613e+05);
	f_norm_tm -> SetParameter("b",      4.14555e+05);
	f_norm_tm -> SetParameter("m",     -9.75495e+00);
	f_norm_tm -> SetParameter("tau",    6.96227e+02);
	f_norm_tm -> SetParameter("x0",     1000.0);
	
	// bm:
	f_norm_bm -> SetParameter("A",      1.69654e+05);
	f_norm_bm -> SetParameter("b",      1.43291e+05);
	f_norm_bm -> SetParameter("m",     -5.63984e+00);
	f_norm_bm -> SetParameter("tau",    6.64862e+02);
	f_norm_bm -> SetParameter("x0",     1000.0);

	// tp:
	f_norm_tp -> SetParameter("A",      1.64365e+05);
	f_norm_tp -> SetParameter("b",      1.37805e+05);
	f_norm_tp -> SetParameter("m",     -4.75169e+00);
	f_norm_tp -> SetParameter("tau",    6.61316e+02);
	f_norm_tp -> SetParameter("x0",     1000.0);
	
	
	double thenew_norm_tp = f_norm_tp->Eval( (double)mono_int);
	double thenew_norm_tm = f_norm_tm->Eval( (double)mono_int);
	double thenew_norm_bp = f_norm_bp->Eval( (double)mono_int);
	double thenew_norm_bm = f_norm_bm->Eval( (double)mono_int);
	if( thenew_norm_tp<0 ) { thenew_norm_tp=0.0; }
	if( thenew_norm_tm<0 ) { thenew_norm_tm=0.0; }
	if( thenew_norm_bp<0 ) { thenew_norm_bp=0.0; }
	if( thenew_norm_bm<0 ) { thenew_norm_bm=0.0; }
	
	
	R_tp.SetParameter("01_norm", thenew_norm_tp);
	R_tm.SetParameter("01_norm", thenew_norm_tm);
	R_bp.SetParameter("01_norm", thenew_norm_bp);
	R_bm.SetParameter("01_norm", thenew_norm_bm);
}


void SetParam_k(TF1 &R_tp, TF1 &R_tm, TF1 &R_bp, TF1 &R_bm, int mono_int)
{
	TF1 * f_k = new TF1("f_k", "0.5*(1.0+TMath::Erf((x-[x0])/[sigma]))*([m]*x+[b])-[b]+[b2]", 0, 5500);
	TF1 * f_k_more = (TF1*)f_k->Clone();
	TF1 * f_k_less = (TF1*)f_k->Clone();
	
	f_k_less -> SetParameter("b",     -4.85784e-01);
	f_k_less -> SetParameter("b2",     3.80946e+00);
	f_k_less -> SetParameter("m",      1.57886e-04);
	f_k_less -> SetParameter("sigma",  1.14973e+03);
	f_k_less -> SetParameter("x0",     2.30e+03);


	f_k_more -> SetParameter("b",     -1.68353e+00);
	f_k_more -> SetParameter("b2",     3.14475e+00);
	f_k_more -> SetParameter("m",      1.98178e-04);
	f_k_more -> SetParameter("sigma",  7.05287e+03);
	f_k_more -> SetParameter("x0",     2.30e+03);
	
	double thenew_k_more = f_k_more->Eval( (double)mono_int);
	double thenew_k_less = f_k_less->Eval( (double)mono_int);
	
	R_tp.SetParameter("08_k", thenew_k_less);
	R_tm.SetParameter("08_k", thenew_k_more);
	R_bp.SetParameter("08_k", thenew_k_more);
	R_bm.SetParameter("08_k", thenew_k_less);

}


// --- // --- // --- // --- // --- // --- // --- // --- // --- // --- // --- // --- //
// --- // --- // --- // --- // --- // --- // --- // --- // --- // --- // --- // --- //
// --- // --- // --- // --- // --- // --- // --- // --- // --- // --- // --- // --- //
plotfuncs::plotfuncs()
{
	n_plot = 500;
	
	string fmoyal_string = "(1.0-[03_alpha]/[00_E0]-[04_beta]/[00_E0]-[06_delta]/([05_gamma]*[07_W])-([05_gamma]*[07_W]))*(1-[09_gfrac])*((1.0/ sqrt(2.0*pi*([11_lres]*[12_gres])*([00_E0]-0.5*[14_dE0]+[13_DgE])) )*exp( -0.5*( -(x-([00_E0]-0.5*[14_dE0]+[13_DgE]) )/ (([11_lres]*[12_gres])*([00_E0]-0.5*[14_dE0]+[13_DgE]) ) )) * exp(-0.5*exp((x-([00_E0]-0.5*[14_dE0]+[13_DgE]) )/ (([11_lres]*[12_gres])*([00_E0]-0.5*[14_dE0]+[13_DgE])) )))";  // the "reversed" moyal function
	
//	string fmoyal_string = "(1.0-[03_alpha]/[00_E0]-[04_beta]-[06_delta]-[05_gamma])*(1-[09_gfrac])*((1.0/ sqrt(2.0*pi*([11_lres]*[10_toeres])*([00_E0])) )*exp( -0.5*( -(x-([00_E0]+[14_dE0]) )/ (([11_lres]*[10_toeres])*([00_E0]) ) )) * exp(-0.5*exp((x-([00_E0]+[14_dE0]) )/ (([11_lres]*[10_toeres])*([00_E0])) )))";  // the "reversed" moyal function.  modified alpha.
	
//	// for landau, this function *is not* normalized -- so 'norm' just describes the total height of the function.  Other params don't affect the height.
//	string flandau_string = "(1.0-[03_alpha]-[04_beta]-[06_delta]-[05_gamma])*(1-[09_gfrac])*TMath::Landau(-1.0*x, -1.0*([00_E0]+[14_dE0]), sqrt(([11_lres]*[10_toeres])*([00_E0])) )";  // built-in (reversed) landau distribution.
//	// TF1 * f = new TF1("[01_norm]*TMath::Landau(-1.0*x, -1.0*( [00_E0] ), sqrt(([11_lres]*[10_toeres])*([00_E0])) )", 0, 5500 );
	
	
	string flandau_string = "(1.0-[03_alpha]/[00_E0]-[04_beta]/[00_E0]-[06_delta]/([05_gamma]*[07_W])-([05_gamma]*[07_W]))*(1-[09_gfrac])*TMath::Landau(-1.0*x, -1.0*([00_E0]-0.5*[14_dE0]), sqrt(([11_lres]*[10_toeres])*([00_E0]-0.5*[14_dE0])) )*(pi/2.0)/sqrt(([11_lres]*[10_toeres])*([00_E0]-0.5*[14_dE0]))";  // built-in (reversed) landau distribution.  It now scales correctly with 'norm', and *probably* has the right overall value.  the pi/2 was a "guess", since it was close to the empirical measured value.
//	string flandau_string = "(1.0-[03_alpha]/[00_E0]-[04_beta]-[06_delta]-[05_gamma])*(1-[09_gfrac])*TMath::Landau(-1.0*x, -1.0*([00_E0]+[14_dE0]), sqrt(([11_lres]*[10_toeres])*([00_E0])) )*(pi/2.0)/sqrt(([11_lres]*[10_toeres])*([00_E0]))";  // built-in (reversed) landau distribution.  It now scales correctly with 'norm', and *probably* has the right overall value.  the pi/2 was a "guess", since it was close to the empirical measured value.  modified alpha.
	
	string f1_string = "(1.0-[03_alpha]/[00_E0]-[04_beta]/[00_E0]-[06_delta]/([05_gamma]*[07_W])-([05_gamma]*[07_W]))*[09_gfrac]*( exp(-1.0*(x-([00_E0]+[13_DgE]+0.5*[14_dE0]))^2/(2.0*[12_gres]*[10_toeres]*([00_E0]+[13_DgE]+0.5*[14_dE0])) ) / ( sqrt(2.0*pi*[12_gres]*[10_toeres]*([00_E0]+[13_DgE]+0.5*[14_dE0])) ) )";  // the primary peak, clifford style.  A gaussian.
//	string f1_string = "(1.0-[03_alpha]/[00_E0]-[04_beta]-[06_delta]-[05_gamma])*[09_gfrac]*( exp(-1.0*(x-([00_E0]+[13_DgE]))^2/(2.0*[12_gres]*[10_toeres]*([00_E0]+[13_DgE])) ) / ( sqrt(2.0*pi*[12_gres]*[10_toeres]*([00_E0]+[13_DgE])) ) )";  // the primary peak, clifford style.  A gaussian.  modified alpha.
	
	string f2_string = "[03_alpha]/[00_E0]*( (1.0-TMath::Erf( (x-[00_E0])/(sqrt(2.0*[10_toeres]*[00_E0])) ) ) / ( 2.0*[00_E0]) )";  // long, flat, low energy tail
//	string f2_string = "[03_alpha]/[00_E0]*( (1.0-TMath::Erf( (x-[00_E0])/(sqrt(2.0*[10_toeres]*[00_E0])) ) ) / ( 2.0*[00_E0]) )";  // long, flat, low energy tail.  modified alpha.

//	string f3_string = "[04_beta]*( (1.0-TMath::Erf( (x-[00_E0]+[10_toeres]*[00_E0]*[08_k])/sqrt(2.0*[10_toeres]*[00_E0]) ))*exp( [08_k]*(x-[00_E0]) + [10_toeres]*[00_E0]*[08_k]*[08_k]/2.0 ) / (2.0*(1-exp(-1.0*[08_k]*[00_E0]) )) )";  // low-energy exponential tail
//	string f3_string = "[04_beta]/[00_E0]*( (1.0-TMath::Erf( (x-[00_E0] )/sqrt(2.0*[10_toeres]*[00_E0]) )) * exp( [08_k]/[00_E0]*(x-[00_E0]) ) / (2.0*(1-exp(-1.0*[08_k]/[00_E0]*[00_E0]) )) )";  // low-energy exponential tail.  now with k/E0.  also now with the modified truncation thingy.  is it even still normalized??  I don't know, but it's horrible.

//	string f3_string = "[04_beta]/[00_E0]*( (1.0-TMath::Erf( (x-[00_E0]+[10_toeres]*[00_E0]*[08_k]/[00_E0] )/sqrt(2.0*[10_toeres]*[00_E0]) )) * exp( [08_k]/[00_E0]*(x-[00_E0]) )*exp( [10_toeres]*[00_E0]*[08_k]/[00_E0]*[08_k]/[00_E0]/2.0 ) / (2.0*(1-exp(-1.0*[08_k]/[00_E0]*[00_E0]) )) )";  // low-energy exponential tail.  now with k/E0.

	string f3_string = "[04_beta]/[00_E0]*( (1.0-TMath::Erf( (x-[00_E0]+[10_toeres]*[08_k] )/sqrt(2.0*[10_toeres]*[00_E0]) )) * exp( [08_k]/[00_E0]*(x-[00_E0]) )*exp( [10_toeres]*[08_k]*[08_k]/[00_E0]/2.0 ) / (2.0*(1-exp(-1.0*[08_k]) )) )";  // low-energy exponential tail.  now with k/E0.  ...simplified.

//	string f3_string = "( (1.0-TMath::Erf( (x-[00_E0]+[10_toeres]*[08_k] )/sqrt(2.0*[10_toeres]*[00_E0]) )) * exp( [08_k]/[00_E0]*(x-[00_E0]) ) * exp( [10_toeres]*[08_k]*[08_k]/[00_E0] ) )";  // a horrible horrible kludge.  never use this..
	
	
	
	string f4_string = "([05_gamma]*[07_W])*(TMath::Erf( (x-[00_E0])/(sqrt(2.0*[10_toeres]*[00_E0])) )/(2.0*[07_W]) - TMath::Erf( (x-[00_E0]-[07_W])/(sqrt(2.0*[10_toeres]*([00_E0]+[07_W]) )) )/(2.0*[07_W]) )";  // shelf

	string f5a_string = "( (x-[00_E0])*( TMath::Erf( (x-[00_E0])/(sqrt(2.0*[10_toeres]*[00_E0]))) - 2.0*TMath::Erf( (x-[00_E0]-[07_W])/(sqrt(2.0*[10_toeres]*([00_E0]+[07_W]) ))) + TMath::Erf( (x-[00_E0]-2.0*[07_W])/(sqrt(2.0*[10_toeres]*([00_E0]+2.0*[07_W]) ))) ) )";
	string f5b_string = "(2.0*[07_W])*( TMath::Erf( (x-[00_E0]-[07_W])/(sqrt(2.0*[10_toeres]*([00_E0]+[07_W]) ))) - TMath::Erf( (x-[00_E0]-2.0*[07_W])/(sqrt(2.0*[10_toeres]*([00_E0]+2.0*[07_W]) ))) )";
	string f5c_string = "(2.0*[10_toeres]*[00_E0])*( (exp(-1.0*(x-[00_E0])^2/(4.0*[10_toeres]*[00_E0])) / ( sqrt(2.0*pi*[10_toeres]*[00_E0]))) -2.0*(exp(-1.0*(x-[00_E0]-[07_W])^2/(4.0*[10_toeres]*([00_E0]+[07_W]) )) / ( sqrt(2.0*pi*[10_toeres]*([00_E0]+[07_W]) ))) + ( exp(-1.0*(x-[00_E0]-2.0*[07_W])^2/(4.0*[10_toeres]*([00_E0]+2.0*[07_W]) )) / ( sqrt(2.0*pi*[10_toeres]*([00_E0]+2.0*[07_W]) )) ) )";
	
	string f5_string = "[06_delta]/([05_gamma]*[07_W])*("+f5a_string+" + "+f5b_string+" + "+f5c_string+")/(2.0*[07_W]*[07_W])";  // triangle
	//
	
	string f_511string1 = "( 195.0*sqrt(2.0/pi) / 17.0 * exp(-(1.0/578.0)*(-308.0 + x)^2) )";// + 
	string f_511string2 = "(80.0 + 1.0/450.0 * (-210.0 + x)^2) * (0.5 - 0.5*TMath::Erf(1.0/30.0*(-334.0 + x)) )";// + 
	string f_511string3 = "1.0/360.0 * (-505.0 + x)^2 * (0.5 - 0.5* TMath::Erf(1.0/30.0*(-505.0 + x)) ) * (0.5 + 0.5* TMath::Erf(1.0/30.0*(-334.0 + x)) )"; 
	string f511_string = "sqrt([02_scale]*[02_scale]) * ("+f_511string1+" + "+f_511string2+" + "+f_511string3+")";
	
	//								          1                    09_gfrac       03_alpha        04_beta/08_k    05_gamma        06_delta         02_scale
	string complete_ff_string = "[01_norm]*("+fmoyal_string+" + "+f1_string+" + "+f2_string+" + "+f3_string+" + "+f4_string+" + "+f5_string+") + "+f511_string;  // landau primary + modified compton toe + 511s.
	
	
	
	// --- // 
	R_total_      = TF1("R",           (complete_ff_string).c_str(),         0.0, 5500.0);
	
	f_landau_     = TF1("f_landau",    ("[01_norm]*"+fmoyal_string).c_str(), 0.0, 5500.0);  // 00_E0 01_norm 06_delta 05_gamma 03_alpha 04_beta 09_gfrac 10_toeres 11_lres 14_dE0 13_DgE
	f1_clifford_  = TF1("f1_clifford", ("[01_norm]*"+f1_string).c_str(),     0.0, 5500.0);  // 00_E0 01_norm 06_delta 05_gamma 03_alpha 04_beta 09_gfrac 10_toeres 12_gres 13_DgE
	
	f2_clifford_  = TF1("f2_clifford", ("[01_norm]*"+f2_string).c_str(),     0.0, 5500.0);  // 00_E0 01_norm 10_toeres 03_alpha
	f3_clifford_  = TF1("f3_clifford", ("[01_norm]*"+f3_string).c_str(),     0.0, 5500.0);  // 00_E0 01_norm 10_toeres 04_beta 08_k
	f4_clifford_  = TF1("f4_clifford", ("[01_norm]*"+f4_string).c_str(),     0.0, 5500.0);  // 00_E0 01_norm 10_toeres 05_gamma 07_W 
	f5_clifford_  = TF1("f5_clifford", ("[01_norm]*"+f5_string).c_str(),     0.0, 5500.0);  // 00_E0 01_norm 10_toeres 06_delta 07_W 05_gamma
	
	f_511_        = TF1("f_511",       (f511_string).c_str(),         0.0, 5500.0);  // 02_scale
	
	// some default parameters so we don't segfault later:
	SetParamsLike_BB1Agree_highenergy(R_total_);
	// Propagate them through...
	propagate_through();
	
	// set up fineness of plots:
	R_total_.SetNpx(n_plot);
	f_landau_.SetNpx(n_plot);
	f1_clifford_.SetNpx(n_plot);
	f2_clifford_.SetNpx(n_plot);
	f3_clifford_.SetNpx(n_plot);
	f4_clifford_.SetNpx(n_plot);
	f5_clifford_.SetNpx(n_plot);
	f_511_.SetNpx(n_plot);

	
	// Set up some colors ....
	R_total_.SetLineColor(kBlack);
	f_landau_.SetLineColor(kGreen);
	f1_clifford_.SetLineColor(kRed);      // gaussian
	f2_clifford_.SetLineColor(kMagenta);  // flat tail
	f3_clifford_.SetLineColor(kOrange);   // exponential tail
	f4_clifford_.SetLineColor(kBlue);     // shelf
	f5_clifford_.SetLineColor(kGray);     // triangle
	f_511_.SetLineColor(kCyan);
	
//	cout << "plotfuncs::plotfuncs() is finished.  f_landau_ etc should be initialized." << endl;
//	cout << "plotfuncs::plotfuncs():  f_landau_.GetParameter(\"00_E0\") = " << f_landau_.GetParameter("00_E0") << endl;
}



// --- // --- // --- // --- // --- // --- // --- // --- // --- // --- // --- // --- //
TF1* make_response_function()
// sigma^2 = lambda*E0
{
	plotfuncs * the_tmp_plotfuncs = new plotfuncs();
	TF1 * R = the_tmp_plotfuncs->R_total();
	return R;
	
}












































