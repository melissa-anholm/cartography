#include "TF1.h"
#include "TMath.h"
#include "/Users/anholm/Packages/MiscLibs/HistExtras.h"
#include "/Users/anholm/Packages/MiscLibs/HistExtras.cpp"


void compare();

void rootscript()
{
	compare();
}


TH1D * make_th1_from_tf1(TF1 * f, string histtype)
{
	TH1D * h = CreateHist( string(f->GetName())+string("_hist"), string("Mapping_Ebeta"), int(kBlack), 1);
	int N_bins = h->GetNbinsX();
//	for
	return h;
}

void compare()
{
	
	string f_511string1 = "( 195.0*sqrt(2.0/pi) / 17.0 * exp(-(1.0/578.0)*(-308.0 + x)^2) )";// + 
	string f_511string2 = "(80.0 + 1.0/450.0 * (-210.0 + x)^2) * (0.5 - 0.5*TMath::Erf(1.0/30.0*(-334.0 + x)) )";// + 
	string f_511string3 = "1.0/360.0 * (-505.0 + x)^2 * (0.5 - 0.5* TMath::Erf(1.0/30.0*(-505.0 + x)) ) * (0.5 + 0.5* TMath::Erf(1.0/30.0*(-334.0 + x)) )"; 
	
	string f511_string = "[02_scale]*("+f_511string1+" + "+f_511string2+" + "+f_511string3+")";
	

	string fmoyal_string = "[01_norm]*( (1.0/ sqrt(2.0*pi*[12_gres]*[00_E0]) )*exp( -0.5*( -(x-[00_E0] )/ ([12_gres]*[00_E0]) )) * exp(-0.5*exp((x-[00_E0] )/ ([12_gres]*[00_E0]) )) )";  // the "reversed" moyal function

	string fmoyal_naked_string = "[01_norm]*( exp( -0.5*( -(x-[00_E0] )/ ([12_gres]*[00_E0]) )) * exp(-0.5*exp((x-[00_E0] )/ ([12_gres]*[00_E0]) )) )";  // the "reversed" moyal function

	string f1_string = "[01_norm]*( exp(-1.0*(x-[00_E0])^2/(2.0*[12_gres]*[00_E0]) ) / ( sqrt(2.0*pi*[12_gres]*[00_E0]) ) )";  // the primary peak, clifford style.  A gaussian.


	TF1 * f_511_   = new TF1("f_511",       (f511_string).c_str(),         -5500.0, 11000.0);  // 02_scale
	TF1 * f_moyal_ = new TF1("f_moyal",     (fmoyal_string).c_str(), -5500.0, 11000.0);  // 00_E0 01_norm 12_gres
	TF1 * f_gauss_ = new TF1("f1_clifford", (f1_string).c_str(),           -5500.0, 11000.0);  // 00_E0 01_norm 12_gres
	
	f_511_   -> SetLineColor(int(kCyan));
	f_moyal_ -> SetLineColor(int(kGreen));
	f_gauss_ -> SetLineColor(int(kRed));
	
	f_511_   -> SetParameter("02_scale", 1.0);
	f_moyal_ -> SetParameter("01_norm",  1.0);
	f_gauss_ -> SetParameter("01_norm",  1.0);
	
	double gres = 8.0;
	double E0   = 1000.0;
	f_moyal_ -> SetParameter("12_gres", gres);
	f_gauss_ -> SetParameter("12_gres", gres);

	f_moyal_ -> SetParameter("00_E0", E0);
	f_gauss_ -> SetParameter("00_E0", E0);
	
	
	double N_511   = f_511_   -> Integral(0, 5500);  // 40349.8890532 , probably only good to ~...889.
	double N_moyal = f_moyal_ -> Integral(0, 5500);
	double N_gauss = f_gauss_ -> Integral(0, 5500);
	
	
	cout << setprecision(12);
	cout << "N_511   = " << N_511   << endl;
	cout << "N_moyal = " << N_moyal << endl;
	cout << "N_gauss = " << N_gauss << endl;
	cout << "N_moyal/N_gauss = " << N_moyal / N_gauss << endl;
	cout << "E0 = " << E0 << "; \tgres = " << gres << endl;
	cout << "(N_moyal/N_gauss)/sqrt(sqrt(gres)) = " << (N_moyal/N_gauss)/sqrt(sqrt(gres)) << endl;
	
//	TCanvas * c = new TCanvas("A Canvas", "A Canvas", 100, 0, 900, 700);
//	c->cd();
//	
//	gPad->Update();	
	
	
	return;
}





























