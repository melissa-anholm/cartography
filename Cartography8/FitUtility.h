#ifndef INCLUDE_FITUTILITY
#define INCLUDE_FITUTILITY 1


#include "TF1.h"



// --- // --- // --- // --- // --- // --- // --- // --- // --- // --- // --- // --- //

// --- // --- // --- // --- // --- // --- // --- // --- // --- // --- // --- // --- //
// --- // --- // --- // --- // --- // --- // --- // --- // --- // --- // --- // --- //


// --- // --- // --- // --- // --- // --- // --- // --- // --- // --- // --- // --- //
void SetParamsLike_BB1Agree_highenergy(TF1 &R);

void SetParam_W(TF1 &R_tp, TF1 &R_tm, TF1 &R_bp, TF1 &R_bm, int mono_int);
void SetParam_gamma(TF1 &R_tp, TF1 &R_tm, TF1 &R_bp, TF1 &R_bm, int mono_int);
void SetParam_toeres(TF1 &R_tp, TF1 &R_tm, TF1 &R_bp, TF1 &R_bm, int mono_int);
void SetParam_E0(TF1 &R_tp, TF1 &R_tm, TF1 &R_bp, TF1 &R_bm, int mono_int);
void SetParam_delta(TF1 &R_tp, TF1 &R_tm, TF1 &R_bp, TF1 &R_bm, int mono_int);
void SetParam_gres(TF1 &R_tp, TF1 &R_tm, TF1 &R_bp, TF1 &R_bm, int mono_int);
void SetParam_toeres_again(TF1 &R_tp, TF1 &R_tm, TF1 &R_bp, TF1 &R_bm, int mono_int);
void SetParam_DgE_tmp(TF1 &R_tp, TF1 &R_tm, TF1 &R_bp, TF1 &R_bm, int mono_int);
void SetParam_dE0(TF1 &R_tp, TF1 &R_tm, TF1 &R_bp, TF1 &R_bm, int mono_int);
void SetParam_lres(TF1 &R_tp, TF1 &R_tm, TF1 &R_bp, TF1 &R_bm, int mono_int);
void SetParam_gfrac(TF1 &R_tp, TF1 &R_tm, TF1 &R_bp, TF1 &R_bm, int mono_int);
void SetParam_alpha_tmp(TF1 &R_tp, TF1 &R_tm, TF1 &R_bp, TF1 &R_bm, int mono_int);
void SetParam_alpha(TF1 &R_tp, TF1 &R_tm, TF1 &R_bp, TF1 &R_bm, int mono_int);
void SetParam_beta(TF1 &R_tp, TF1 &R_tm, TF1 &R_bp, TF1 &R_bm, int mono_int);
void SetParam_scale(TF1 &R_tp, TF1 &R_tm, TF1 &R_bp, TF1 &R_bm, int mono_int);
void SetParam_norm(TF1 &R_tp, TF1 &R_tm, TF1 &R_bp, TF1 &R_bm, int mono_int);
void SetParam_k(TF1 &R_tp, TF1 &R_tm, TF1 &R_bp, TF1 &R_bm, int mono_int);


TF1* make_response_function();

// --- // --- // --- // --- // --- // --- // --- // --- // --- // --- // --- // --- //
// --- // --- // --- // --- // --- // --- // --- // --- // --- // --- // --- // --- //
// --- // --- // --- // --- // --- // --- // --- // --- // --- // --- // --- // --- //
class plotfuncs
{
public:
	plotfuncs();
	void load_parameters(TF1 * R_in)
	{
		TF1 * R_tmp = (TF1*)R_in->Clone();
		R_total_ = *R_tmp;
		propagate_through();
	};
	void propagate_through()
	{
		f_landau_ . SetParameter("00_E0",     R_total_.GetParameter("00_E0"));
		f_landau_ . SetParameter("01_norm",   R_total_.GetParameter("01_norm"));
		f_landau_ . SetParameter("06_delta",  R_total_.GetParameter("06_delta"));
		f_landau_ . SetParameter("05_gamma",  R_total_.GetParameter("05_gamma"));
		f_landau_ . SetParameter("03_alpha",  R_total_.GetParameter("03_alpha"));
		f_landau_ . SetParameter("04_beta",   R_total_.GetParameter("04_beta"));
		f_landau_ . SetParameter("09_gfrac",  R_total_.GetParameter("09_gfrac"));
		f_landau_ . SetParameter("10_toeres", R_total_.GetParameter("10_toeres"));
		f_landau_ . SetParameter("11_lres",   R_total_.GetParameter("11_lres"));
		f_landau_ . SetParameter("12_gres",   R_total_.GetParameter("12_gres"));
		
		f_landau_ . SetParameter("14_dE0",    R_total_.GetParameter("14_dE0"));
		f_landau_ . SetParameter("13_DgE",    R_total_.GetParameter("13_DgE"));
		f_landau_ . SetParameter("07_W",      R_total_.GetParameter("07_W"));
		
		
		// f1 is the gaussian.
		f1_clifford_ . SetParameter("00_E0",     R_total_.GetParameter("00_E0"));
		f1_clifford_ . SetParameter("01_norm",   R_total_.GetParameter("01_norm"));
		f1_clifford_ . SetParameter("11_lres",   R_total_.GetParameter("11_lres"));
		f1_clifford_ . SetParameter("06_delta",  R_total_.GetParameter("06_delta"));
		f1_clifford_ . SetParameter("05_gamma",  R_total_.GetParameter("05_gamma"));
		f1_clifford_ . SetParameter("03_alpha",  R_total_.GetParameter("03_alpha"));
		f1_clifford_ . SetParameter("04_beta",   R_total_.GetParameter("04_beta"));
		f1_clifford_ . SetParameter("09_gfrac",  R_total_.GetParameter("09_gfrac"));
		f1_clifford_ . SetParameter("10_toeres", R_total_.GetParameter("10_toeres"));
		f1_clifford_ . SetParameter("12_gres",   R_total_.GetParameter("12_gres"));
		f1_clifford_ . SetParameter("13_DgE",    R_total_.GetParameter("13_DgE"));
		f1_clifford_ . SetParameter("07_W",      R_total_.GetParameter("07_W"));
		
		f2_clifford_ . SetParameter("00_E0",     R_total_.GetParameter("00_E0"));
		f2_clifford_ . SetParameter("01_norm",   R_total_.GetParameter("01_norm"));
		f2_clifford_ . SetParameter("03_alpha",  R_total_.GetParameter("03_alpha"));
		f2_clifford_ . SetParameter("10_toeres", R_total_.GetParameter("10_toeres"));
	
		f3_clifford_ . SetParameter("00_E0",     R_total_.GetParameter("00_E0"));
		f3_clifford_ . SetParameter("01_norm",   R_total_.GetParameter("01_norm"));
		f3_clifford_ . SetParameter("10_toeres", R_total_.GetParameter("10_toeres"));
		f3_clifford_ . SetParameter("04_beta",   R_total_.GetParameter("04_beta"));
		f3_clifford_ . SetParameter("08_k",      R_total_.GetParameter("08_k"));
	
		f4_clifford_ . SetParameter("00_E0",     R_total_.GetParameter("00_E0"));
		f4_clifford_ . SetParameter("01_norm",   R_total_.GetParameter("01_norm"));
		f4_clifford_ . SetParameter("10_toeres", R_total_.GetParameter("10_toeres"));
		f4_clifford_ . SetParameter("05_gamma",  R_total_.GetParameter("05_gamma"));
		f4_clifford_ . SetParameter("07_W",      R_total_.GetParameter("07_W"));

		f5_clifford_ . SetParameter("00_E0",     R_total_.GetParameter("00_E0"));
		f5_clifford_ . SetParameter("01_norm",   R_total_.GetParameter("01_norm"));
		f5_clifford_ . SetParameter("10_toeres", R_total_.GetParameter("10_toeres"));
		f5_clifford_ . SetParameter("06_delta",  R_total_.GetParameter("06_delta"));
		f5_clifford_ . SetParameter("07_W",      R_total_.GetParameter("07_W"));
		f5_clifford_ . SetParameter("05_gamma",  R_total_.GetParameter("05_gamma"));
	
		f_511_ . SetParameter("02_scale", R_total_.GetParameter("02_scale"));
	};
	
	TF1 * R_total()     { return &R_total_; };
	TF1 * f_landau()    { return &f_landau_; };
	TF1 * f1_clifford() { return &f1_clifford_; };
	TF1 * f2_clifford() { return &f2_clifford_; };
	TF1 * f3_clifford() { return &f3_clifford_; };
	TF1 * f4_clifford() { return &f4_clifford_; };
	TF1 * f5_clifford() { return &f5_clifford_; };
	TF1 * f_511()       { return &f_511_; };
	
private:
	TF1 R_total_;
	TF1 f_landau_;  // or moyal
	TF1 f1_clifford_;
	TF1 f2_clifford_;
	TF1 f3_clifford_;
	TF1 f4_clifford_;
	TF1 f5_clifford_;
	TF1 f_511_;
	
	int n_plot;
};



// --- // --- // --- // --- // --- // --- // --- // --- // --- // --- // --- // --- //

#endif














































